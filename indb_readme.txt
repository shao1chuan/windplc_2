influxdb删除数据：
influx delete -org windb --bucket plc \
--start 1970-01-01T00:00:00Z \
--stop 2050-01-01T00:00:00Z  \
--predicate '_measurement="opcua"' \
--host http://localhost:8086 \
--token 3v3lz9sRWqAzo4zm7w4ujCNRAlsgdPDp8INbgwey6iPOymP9J0hnD02Wb8dznadWMzOwjpkVJ8TfAR9yrnTPEw==

localhost:  3v3lz9sRWqAzo4zm7w4ujCNRAlsgdPDp8INbgwey6iPOymP9J0hnD02Wb8dznadWMzOwjpkVJ8TfAR9yrnTPEw==
plc：VsqErgEYQU_V6L_96TP9wthZVBqpROLkNyqJhxiMA8Rhw8TLX3ArKiZnPLNGmREJ4nsE1CoDekK0DFGRFyE4Ig==
查询数据过去一天：
from(bucket: "plc") |> range(start: -1d) |> filter(fn: (r) => r["_measurement"] == "opcua") |> filter(fn: (r) => r["_field"] == "WindSpeed" or  r["_field"] == "ActivePower" or r["_field"] == "WindDirection" or  r["_field"] == "DataEffectiveFlag")
查询数据过去一小时：
from(bucket: "plc") |> range(start: -1h) |> filter(fn: (r) => r["_measurement"] == "opcua") |> filter(fn: (r) => r["_field"] == "WindSpeed" or  r["_field"] == "ActivePower" or r["_field"] == "WindDirection" or  r["_field"] == "DataEffectiveFlag") 


    from(bucket: "plc")  
    |> range(start: -365d)    
    |> sort(desc: true) 
    |> filter(fn: (r) => r._measurement == "opcua")   
    |> filter(fn: (r) => exists r._value)  
    |> filter(fn: (r) =>  r._value == r._value)
    |> pivot(rowKey:["_time"], columnKey: ["_field"], valueColumn: "_value")
