from nicegui import ui
from fastapi import FastAPI
import plotly.express as px

@ui.page('/')
def test():
        ui.label("this is aaaaaaaaaaaaaaa")

@ui.page('/plotly')
def plotly():

        df = px.data.gapminder()
        df2007 = df.query("year == 2007")
        print(df2007.head())
        fig = px.scatter(df2007,
                         x="gdpPercap",
                         y="lifeExp",
                         color="continent",
                         title="A Plotly Express Figure",
                         size='pop',
                         size_max=60
                         )
        ui.plotly(fig)

        # return (fig.to_html(include_plotlyjs=True, full_html=False, config={'displayModeBar': False}))

def mount_nicegui(app: FastAPI):

    ui.run_with(
        mount_path="/nicegui",
        app=app
    )