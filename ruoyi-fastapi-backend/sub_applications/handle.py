from fastapi import FastAPI
from sub_applications.staticfiles import mount_staticfiles


def handle_sub_applications(app: FastAPI):
    """
    全局处理子应用挂载
    """
    # 挂载静态文件
    mount_staticfiles(app)
    #+++++++++++++++++++++
    from sub_applications.windplc import mount_windplc
    print("挂载windplc")
    mount_windplc(app)
    #+++++++++++++++++++++

    # # +++++++++++++++++++++
    from sub_applications.pywebio import mount_pywebio
    print("挂载pywebio")
    mount_pywebio(app)
    # # +++++++++++++++++++++

    # +++++++++++++++++++++
    from sub_applications.nicegui_ import mount_nicegui
    print("挂载nicegui")
    mount_nicegui(app)
    # +++++++++++++++++++++


