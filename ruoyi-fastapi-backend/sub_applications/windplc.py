from fastapi import FastAPI
from back.fastapi.csvShow import *
from back.fastapi.pitchOnlShow import *
from back.fastapi.torqueCompareShow import *
from back.fastapi.torqueHisShow import *
from back.fastapi.torqueOnlShow import *

from back.fastapi.yawOnlShow import *
from back.fastapi.csvShow import *
from back.fastapi.csv3dShow import csv3dShow

from yawSrc.yawOnl.yawOnl import yawOnl
from yawSrc.yawOnl.initialData import initialData,report
from yawSrc.yawOnl.cleanProcess import cleanProcess
from yawSrc.yawOnl.groupProcess import groupProcess
from yawSrc.yawOnl.smoothProcess import smoothProcess
from yawSrc.yawOnl.surfaceProcess import surfaceProcess
from yawSrc.yawOnl.lineRegress import lineRegress
from yawSrc.yawOnl.lineCompareProcess import lineCompareProcess
from yawSrc.yawOnl.powerCompareProcess import powerCompareProcess
from back.fastapi.csvShow import post_csv



from pitchSrc.pitchOnl.pitchOnl import pitchOnl
from pitchSrc.pitchOnl.initialData import initialData
from pitchSrc.pitchOnl.cleanProcess import cleanProcess
from pitchSrc.pitchOnl.groupProcess import groupProcess
from pitchSrc.pitchOnl.smoothProcess import smoothProcess
from pitchSrc.pitchOnl.surfaceProcess import surfaceProcess
from pitchSrc.pitchOnl.lineRegress import lineRegress
from pitchSrc.pitchOnl.lineCompareProcess import lineCompareProcess
from pitchSrc.tools.toUploadDB import toUploadDBPitchEnable_True
from pitchSrc.tools.toUploadDB import toUploadDBPitchEnable_False

from torqueSrc.torqueExplore.torqueExplore import torqueExplore
from torqueSrc.torqueHis.torqueNewHis import torqueNewHis
from torqueSrc.torqueHis.torqueHis import torqueHis
from torqueSrc.torqueHis.initData import initData
from torqueSrc.torqueHis.cleanProcess import cleanProcess
from torqueSrc.torqueHis.lambdaOptProcess import lambdaOptProcess
from torqueSrc.torqueHis.lambdaOptCompareProcess import lambdaOptCompareProcess
from torqueSrc.torqueOnl.torqueOnl import torqueOnl
from torqueSrc.torqueOnl.initialData import initialData
from torqueSrc.torqueOnl.cleanProcess import cleanProcess
from torqueSrc.torqueOnl.lambdaProcess import lambdaProcess
from torqueSrc.torqueOnl.torqCoefAdjProcess import torqCoefAdjProcess
from torqueSrc.torqueOnl.lambdaCompareProcess import lambdaCompareProcess
from torqueSrc.tools.toFile import toTorqUploadDB
from torqueSrc.tools.toUploadDB import toUploadDBkOptOffsetEnableEnable_True
from torqueSrc.tools.toUploadDB import toUploadDBkOptOffsetEnableEnable_False
from yawSrc.tools.toUploadDB import toUploadDBWindDirEnable_True
from toolsSrc.dbtools.db_select import db_select
from toolsSrc.files.csv_select import csv_select

# http://localhost:9099/windplc

@sub_app.get("/")
def test():
    print('test 开始................')
    print('test 结束................')
    return "test"


def mount_windplc(app: FastAPI):
    from src.main.sub_server import sub_app
    app.mount("/windplc", sub_app)
