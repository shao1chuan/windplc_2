from fastapi import FastAPI
from back.pywebio.pywebioList import pywebioList
from pywebio.output import put_text
from pywebio.platform.fastapi import webio_routes
from back.pywebio.yamlConfig import *
from back.pywebio.upload import *


def mount_pywebio(app: FastAPI):
    pywebioList = [
        yaml_main,
        yaml_yaw,
        yaml_pitch,
        yaml_torque,
        yaw_upload,
        pitch_upload,
        torque_upload,
        csv3dUpload
    ]
    app.mount("/pywebio", FastAPI(routes=webio_routes(pywebioList)))
