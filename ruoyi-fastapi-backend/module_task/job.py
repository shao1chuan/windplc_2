from toolsSrc.files.poolClean import poolClean
from toolsSrc.files.toCsv import saveDatam

# executors = {
#     'default': ThreadPoolExecutor(10)
# }
# aps = BackgroundScheduler()
# poolClean(Config.poolRoot, Config.poolNum)
# saveDatam(Config.poolRoot)
# aps.add_job(poolClean, IntervalTrigger(hours=Config.cleanSpan), id='poolClean',
#             args=[Config.poolRoot, Config.poolNum], max_instances=Config.max_instances)
# aps.add_job(saveDatam, IntervalTrigger(hours=Config.cleanSpan), id='saveDatam', args=[Config.poolRoot], max_instances=Config.max_instances)
# aps.start()
# while True:
#     time.sleep(10)

from datetime import datetime

# 调用方法   :   module_task.job.job_poolClean
# 关键字参数   :  {"path":"../../data/data/plc/pool/","poolNum":10000}
# cron表达式 每小时执行一次 : * 0/59 * * * ?
def job_poolClean(*args, **kwargs):
    print(f"{datetime.now()}执行了 poolClean")
    path = kwargs["path"]
    poolNum = kwargs["poolNum"]
    # print("路径:", path)
    # print("池数量:", poolNum)
    poolClean(path,poolNum)

# 调用方法   :   module_task.job.job_saveDatam
# 关键字参数   :  {"path":"../../data/data/plc/pool/"}
# cron表达式 每小时执行一次 : * 0/59 * * * ?
def job_saveDatam(*args, **kwargs):
    print(f"{datetime.now()}执行了 saveDatam")
    path = kwargs["path"]
    saveDatam(path)