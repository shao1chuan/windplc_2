FROM python:3.11.6
ENV AM_I_IN_A_DOCKER_CONTAINER Yes
COPY ./back/appSrc /app/appSrc
COPY ./back/back /app/back
COPY ./back/common /app/common

COPY ./back/toolsSrc /app/toolsSrc
COPY ./back/yawSrc /app/yawSrc
COPY ./back/pitchSrc /app/pitchSrc
COPY ./back/torqueSrc /app/torqueSrc

COPY ./configData/data /app/data

COPY ./ruoyi-fastapi-backend/config /app/config
COPY ./ruoyi-fastapi-backend/exceptions /app/exceptions
COPY ./ruoyi-fastapi-backend/middlewares /app/middlewares
COPY ./ruoyi-fastapi-backend/module_admin /app/module_admin
COPY ./ruoyi-fastapi-backend/module_task /app/module_task
COPY ./ruoyi-fastapi-backend/sql /app/sql
COPY ./ruoyi-fastapi-backend/config /app/config
COPY ./ruoyi-fastapi-backend/src /app/src
COPY ./ruoyi-fastapi-backend/sub_applications /app/sub_applications
COPY ./ruoyi-fastapi-backend/utils /app/utils

copy ./requirements.txt /app

WORKDIR ./app
RUN pip install -r requirements.txt -i http://mirrors.aliyun.com/pypi/simple --trusted-host mirrors.aliyun.com
EXPOSE 9099
ENV PATH=$PATH:/app
ENV PYTHONPATH /app






