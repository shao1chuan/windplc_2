
import subprocess

def run_robocopy(source, destination):
    try:
        print(f"开始同步：{source} 到 {destination}")
        subprocess.run(["robocopy", source, destination, "/MIR", "/R:5", "/W:5"], check=True)
        print("同步完成。")
    except subprocess.CalledProcessError as e:
        print(f"同步失败：{e}")

def main():
    # 第一次同步
    run_robocopy(r"E:\pycharmproject\windplc_2\configData\initdata", r"E:\pycharmproject\windplc_2\ruoyi-fastapi-backend\data")
    # 第二次同步
    run_robocopy(r"E:\pycharmproject\windplc_2\configData\initdata", r"E:\pycharmproject\windplc_2\back\data")
    print("第二次同步完成")

if __name__ == "__main__":
    main()
