from pitchSrc.tools.getPlotly import get2dFig, get2dFig
from common.config import pitchConfig as Config



if __name__ == '__main__':

    fig = get2dFig(Config.onlLineRegress)
    fig.show()

