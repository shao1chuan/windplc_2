from pitchSrc.tools.getPlotly import get2dFig_com
from common.config import pitchConfig as Config



if __name__ == '__main__':
    fig = get2dFig_com(Config.lineCompareProcess)
    fig.show()