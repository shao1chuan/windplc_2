import pandas as pd
from pitchSrc.tools.smoothDecorator import SmoothDecorator
from common.config import pitchConfig as Config
from toolsSrc.decorator.toCsv import ToCsvDecorator
from pitchSrc.tools.loadFile import toCsv,toAttrCsv
from toolsSrc.status.status import Status
from src.main.sub_server import sub_app
from toolsSrc.util.plcweb import Plcweb

@SmoothDecorator.smooth
@SmoothDecorator.sigma3filter
@ToCsvDecorator(Config.smoothPool)
# @SmoothDecorator.curveSmooth
def onlSmoothProcess(X):
    return X

from datetime import datetime
@sub_app.get("/pitchSmoothProcess")
@Status.updateBA(
    "UPDATE tracking SET status=:status, startTime=:startTime WHERE model=:model and func=:func",
    [{"status": 1, "startTime":datetime.now(), "model": "pitchOnl", "func": "smoothProcess"}],
    "UPDATE tracking SET status=:status, endTime=:endTime WHERE model=:model and func=:func",
    [{"status": 2, "endTime":datetime.now(), "model": "pitchOnl", "func": "smoothProcess"}])
@Plcweb.returnName
def smoothProcess():
    print('pitchOnl smoothProcess 开始.................')
    X = toCsv(Config.onlGroupProcess, Config.onlSmoothProcess, onlSmoothProcess)
    toAttrCsv(Config.onlSmoothProcess, Config.onlSmoothAttr)
    print('pitchOnl smoothProcess 结束................')
    # show3dFig(Config.hisSmoothProcess)
    return X
if __name__ == '__main__':


    smoothProcess()