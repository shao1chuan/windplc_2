from common.config import pitchConfig as Config

from pitchSrc.tools.getPlotly import get3dSurfaceFig

if __name__ == '__main__':
    fig = get3dSurfaceFig(Config.onlSurfaceProcess)
    fig.show()