from common.config import pitchConfig as Config
from pitchSrc.tools.loadFile import toAttrCsv
from toolsSrc.status.status import Status
from datetime import datetime
from toolsSrc.util.plcweb import Plcweb
from toolsSrc.files.toCsv import fromInDbToCsv
from fastapi import  Response
from src.main.sub_server import sub_app

@sub_app.get("/pitchInitialData")
# @Status.updateBA(
#     "UPDATE tracking SET status=:status, startTime=:startTime WHERE model=:model and func=:func",
#     [{"status": 1, "startTime":datetime.now(), "model": "pitchOnl", "func": "initialData"}],
#     "UPDATE tracking SET status=:status, endTime=:endTime WHERE model=:model and func=:func",
#     [{"status": 2, "endTime":datetime.now(), "model": "pitchOnl", "func": "initialData"}])
@Plcweb.returnName
def initialData():
    print('pitchOnl initialData 开始................')
    X = fromInDbToCsv(Config.onlInitialData,
                      [Config.WindSpeed, Config.ActPower, Config.PitchAngle, Config.DataEffectiveFlag])
    toAttrCsv(Config.onlInitialData, Config.onlInitialDataAttr)
    print('pitchOnl initialData 结束................')

    return X


import sweetviz as sv
import pandas as pd
from bs4 import BeautifulSoup
import os
import matplotlib
matplotlib.use('Agg')
@sub_app.get("/pitchReport")
def report():
    # 加载数据
    data_file = Config.onlInitialData
    if not os.path.exists(data_file):
        return "文件不存在，请检查文件路径。"
    df = pd.read_csv(data_file)
    my_report = sv.analyze(df)
    report_file = 'pitchReport.html'
    my_report.show_html(report_file,open_browser=False)
    with open(report_file, 'r', encoding='utf-8') as file:
        report_html = file.read()
    soup = BeautifulSoup(report_html, 'html.parser')
    credits_div = soup.find('div', class_='pos-logo-group')
    if credits_div:
        credits_div.decompose()
    return Response(content=str(soup), media_type="text/html")

if __name__ == '__main__':
    initialData()