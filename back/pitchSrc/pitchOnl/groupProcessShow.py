from common.config import pitchConfig as Config

from pitchSrc.tools.getPlotly import show3dFig
from pitchSrc.tools.loadFile import toCsv,toAttrCsv

if __name__ == '__main__':

    show3dFig(Config.onlGroupProcess)