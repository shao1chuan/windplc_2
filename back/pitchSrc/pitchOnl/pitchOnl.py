from toolsSrc.dbtools.update_upload import update_upload
from toolsSrc.status.status import Status
from pitchSrc.pitchOnl.initialData import initialData
from pitchSrc.pitchOnl.cleanProcess import cleanProcess
from pitchSrc.pitchOnl.groupProcess import groupProcess
from pitchSrc.pitchOnl.smoothProcess import smoothProcess
from pitchSrc.pitchOnl.surfaceProcess import surfaceProcess
from pitchSrc.pitchOnl.lineRegress import lineRegress
from pitchSrc.pitchOnl.lineCompareProcess import lineCompareProcess
from toolsSrc.dbtools.sqltools import Sqltools
from datetime import datetime
from src.main.sub_server import sub_app
from toolsSrc.util.plcweb import Plcweb
from pitchSrc.tools.toUploadDB import toUploadDBPitchEnable_True

@sub_app.get("/pitchOnl")
@Status.updateBA(
    "UPDATE status SET value=:value WHERE name=:name",
    [{"name": "pitch", "value": 2}],
    "UPDATE status SET value=:value WHERE name=:name",
    [{"name": "pitch", "value": 1}])
@Plcweb.returnName
def pitchOnl(*args):
    try:
        initialData()
    except Exception as exp:
        print(f"Exception pitchOnl initialData : [{exp}]")
        Sqltools.excuteSql("UPDATE tracking SET startTime=:startTime ,except1 =:except1 WHERE model=:model and func=:func",
                           [{"startTime": datetime.now(), "model": "pitchOnl", "func": "initialData","except1": str(exp)}])
        return

    try:
        cleanProcess()
    except Exception as exp:
        print(f"Exception pitchOnl cleanProcess : [{exp}]")
        Sqltools.excuteSql("UPDATE tracking SET startTime=:startTime ,except1 =:except1 WHERE model=:model and func=:func",
                           [{"startTime": datetime.now(), "model": "pitchOnl", "func": "cleanProcess","except1": str(exp)}])
        return

    try:
        groupProcess()
    except Exception as exp:
        print(f"Exception pitchOnl groupProcess : [{exp}]")
        Sqltools.excuteSql("UPDATE tracking SET startTime=:startTime ,except1 =:except1 WHERE model=:model and func=:func",
                           [{"startTime": datetime.now(), "model": "pitchOnl", "func": "groupProcess","except1": str(exp)}])
        return

    try:
        smoothProcess()
    except Exception as exp:
        print(f"Exception pitchOnl smoothProcess : [{exp}]")
        Sqltools.excuteSql("UPDATE tracking SET startTime=:startTime ,except1 =:except1 WHERE model=:model and func=:func",
                           [{"startTime": datetime.now(), "model": "pitchOnl", "func": "smoothProcess","except1": str(exp)}])
        return

    try:
        surfaceProcess()
    except Exception as exp:
        print(f"Exception pitchOnl surfaceProcess : [{exp}]")
        Sqltools.excuteSql("UPDATE tracking SET startTime=:startTime ,except1 =:except1 WHERE model=:model and func=:func",
                           [{"startTime": datetime.now(), "model": "pitchOnl", "func": "surfaceProcess","except1": str(exp)}])
        return

    # regressProcess()
    try:
        lineRegress()
    except Exception as exp:
        print(f"Exception pitchOnl lineRegress : [{exp}]")
        Sqltools.excuteSql("UPDATE tracking SET startTime=:startTime ,except1 =:except1 WHERE model=:model and func=:func",
                           [{"startTime": datetime.now(), "model": "pitchOnl", "func": "lineRegress","except1": str(exp)}])
        return


    try:
        lineCompareProcess()
    except Exception as exp:
        print(f"Exception pitchOnl lineCompareProcess : [{exp}]")
        Sqltools.excuteSql("UPDATE tracking SET startTime=:startTime ,except1 =:except1 WHERE model=:model and func=:func",
                           [{"startTime": datetime.now(), "model": "pitchOnl", "func": "lineCompareProcess","except1": str(exp)}])
        return

    # zp!!!  将upload/offsetEnableUploadCsv 中的 pitchOffsetEnable 字段设置成True, 以便上传给plc，优化有效
    try:

        toUploadDBPitchEnable_True()
    except Exception as exp:
        print(f"Exception toPitchOffsetEnableUploadCsv : [{exp}]")
        return


if __name__ == '__main__':
    pitchOnl()
