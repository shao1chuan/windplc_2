from pitchSrc.tools.cleanDecorator import CleanDecorator
from pitchSrc.tools.getPlotly import show3dFig
from common.config import pitchConfig as Config




if __name__ == '__main__':

    show3dFig(Config.onlCleanProcess)