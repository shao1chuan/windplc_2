from pitchSrc.tools.cleanDecorator import CleanDecorator
from common.config import pitchConfig as Config
from pitchSrc.tools.loadFile import toCsv, toAttrCsv
import pandas as pd
from toolsSrc.status.status import Status
from src.main.sub_server import sub_app
from toolsSrc.util.plcweb import Plcweb


# @CleanDecorator.drop_duplicates
@CleanDecorator.insert0   # 如果initData得到空表，插入0
#@CleanDecorator.drop_dataEffectFlag_False # 每次筛选都有可能得到空表，因此，后面都应该insert0
@CleanDecorator.insert0  # 当为空表时，插入一行 0
@CleanDecorator.drop_bigorsmall
@CleanDecorator.insert0
@CleanDecorator.drop_discretePoint
@CleanDecorator.insert0
def onlCleanProcess(X):
    return X

from datetime import datetime

@sub_app.get("/pitchCleanProcess")
@Status.updateBA(
    "UPDATE tracking SET status=:status, startTime=:startTime WHERE model=:model and func=:func",
    [{"status": 1, "startTime":datetime.now(), "model": "pitchOnl", "func": "cleanProcess"}],
    "UPDATE tracking SET status=:status, endTime=:endTime WHERE model=:model and func=:func",
    [{"status": 2, "endTime":datetime.now(), "model": "pitchOnl", "func": "cleanProcess"}])
@Plcweb.returnName
def cleanProcess():
    print('pitchOnl cleanProcess 开始................')
    X = toCsv(Config.onlInitialData, Config.onlCleanProcess, onlCleanProcess)
    toAttrCsv(Config.onlCleanProcess, Config.onlCleanAttr)
    print('pitchOnl cleanProcess 结束................')
    # show3dFig(Config.onlCleanProcess)
    return X

if __name__ == '__main__':

    cleanProcess()