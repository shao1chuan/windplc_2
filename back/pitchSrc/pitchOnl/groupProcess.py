import pandas as pd
from pitchSrc.tools.groupDecorator import GroupDecorator
from common.config import pitchConfig as Config

from pitchSrc.tools.getPlotly import show3dFig
from pitchSrc.tools.loadFile import toCsv,toAttrCsv
from toolsSrc.status.status import Status
from src.main.sub_server import sub_app
from toolsSrc.util.plcweb import Plcweb

@GroupDecorator.formulate
def onlGroupProcess(X):
    return X

from datetime import datetime
@sub_app.get("/pitchGroupProcess")
@Status.updateBA(
    "UPDATE tracking SET status=:status, startTime=:startTime WHERE model=:model and func=:func",
    [{"status": 1, "startTime":datetime.now(), "model": "pitchOnl", "func": "groupProcess"}],
    "UPDATE tracking SET status=:status, endTime=:endTime WHERE model=:model and func=:func",
    [{"status": 2, "endTime":datetime.now(), "model": "pitchOnl", "func": "groupProcess"}])
@Plcweb.returnName
def groupProcess():
    print('pitchOnl groupProcess 开始................')
    X = toCsv(Config.onlCleanProcess, Config.onlGroupProcess, onlGroupProcess)
    toAttrCsv(Config.onlGroupProcess, Config.onlGroupAttr)
    print('pitchOnl groupProcess 完毕................')
    # show3dFig(Config.onlGroupProcess)
    return X
if __name__ == '__main__':

    groupProcess()