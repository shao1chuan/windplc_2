from pitchSrc.tools.loadFile import  toAttrCsv, toLineRegressAttrCsv, toCsv
from common.config import pitchConfig as Config
from toolsSrc.status.status import Status
from pitchSrc.tools.lineRegressDecorator import lineRegressDecorator
from datetime import datetime
import os
import time
from toolsSrc.files.checkFiles import checkCsvFiles_h2
from src.main.sub_server import sub_app
from toolsSrc.util.plcweb import Plcweb

@lineRegressDecorator.toLineRegressCsv # 找到山脊线
@lineRegressDecorator.sigma3filter # 过滤曲线离散点
@lineRegressDecorator.fitLineRegressCsv # 对山脊线点进行曲线拟合
def onlLineRegressProcess(X):
    return X

@sub_app.get("/pitchLineRegress")
@Status.updateBA(
    "UPDATE tracking SET status=:status, startTime=:startTime WHERE model=:model and func=:func",
    [{"status": 1, "startTime":datetime.now(), "model": "pitchOnl", "func": "lineRegress"}],
    "UPDATE tracking SET status=:status, endTime=:endTime WHERE model=:model and func=:func",
    [{"status": 2, "endTime":datetime.now(), "model": "pitchOnl", "func": "lineRegress"}])
@Plcweb.returnName
def lineRegress():
    print('pitchOnl lineRegress 开始.....................')
    print("checkCsvFiles_h2: ", checkCsvFiles_h2([Config.onlLineRegress]))
    X = toCsv(Config.onlSurfaceProcess, Config.onlLineRegress, onlLineRegressProcess)
    # fitLineRegressCsv(Config.onlLineRegress, Config.onlLineRegress)  # 曲线拟合
    toLineRegressAttrCsv(Config.onlLineRegress, Config.onlLineRegressAttr)
    print('pitchOnl lineRegress 结束.....................')
    return X

if __name__ == '__main__':


    lineRegress()