import pandas as pd
from pitchSrc.tools.surfaceDecorator import SurfaceDecorator
from pitchSrc.tools.surfaceLineDecorator import SurfaceLineDecorator

from common.config import pitchConfig as Config
from toolsSrc.decorator.toCsv import ToCsvDecorator
from pitchSrc.tools.loadFile import toCsv, toAttrCsv
import os
from toolsSrc.status.status import Status
from src.main.sub_server import sub_app
from toolsSrc.util.plcweb import Plcweb

###############  曲线拟合 ################
@SurfaceLineDecorator.pointsFit
@SurfaceLineDecorator.insert0
############### end 曲线拟合 #############
# ################  曲面拟合 ###############
# @SurfaceDecorator.insertGround
# @SurfaceDecorator.fit
# @SurfaceDecorator.clean
# ################  end 曲面拟合 ###########
@ToCsvDecorator(Config.surfacePool)
def onlSurfaceProcess(points):
    return points

from datetime import datetime
@sub_app.get("/pitchSurfaceProcess")
@Status.updateBA(
    "UPDATE tracking SET status=:status, startTime=:startTime WHERE model=:model and func=:func",
    [{"status": 1, "startTime":datetime.now(), "model": "pitchOnl", "func": "surfaceProcess"}],
    "UPDATE tracking SET status=:status, endTime=:endTime WHERE model=:model and func=:func",
    [{"status": 2, "endTime":datetime.now(), "model": "pitchOnl", "func": "surfaceProcess"}])
@Plcweb.returnName
def surfaceProcess():
    print('pitchOnl surfaceProcess 开始...............')
    X = toCsv(Config.onlSmoothProcess,  Config.onlSurfaceProcess, onlSurfaceProcess)
    toAttrCsv(Config.onlSurfaceProcess, Config.onlSurfaceAttr)
    print('pitchOnl surfaceProcess 结束................')
    # show3dFig(Config.onlSurfaceProcess)
    return X
if __name__ == '__main__':

    surfaceProcess()