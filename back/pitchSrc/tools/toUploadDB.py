from toolsSrc.dbtools.update_upload import update_upload
from src.main.sub_server import sub_app
from toolsSrc.util.plcweb import Plcweb

@sub_app.get("/toUploadDBPitchEnable_True")
@Plcweb.returnName
def toUploadDBPitchEnable_True():
    list = [{"pitchOffsetEnable": True}]
    update_upload(list)

@sub_app.get("/toUploadDBPitchEnable_False")
@Plcweb.returnName
def toUploadDBPitchEnable_False():
    list = [{"pitchOffsetEnable": False}]
    update_upload(list)