import sys, os
import numpy as np
sys.path.append(os.getcwd())
from common.config import pitchConfig as Config
import pandas as pd



''' 处理数据 保存数据 ##############################################################################  '''
'''
配合 *process.py 使用的函数处理
'''
def toCsv(from1, to2, funtionName):
    X = pd.read_csv(from1)
    X = funtionName(X)
    X = round(X, 2)
    X.to_csv(to2, index=False)
    return X




''' 生成 Attr 文件 ################################################################################# '''
def toAttrCsv(from1, to2):
    X = pd.read_csv(from1)
    speedMin = 0 if X.empty else round(min(X[Config.WindSpeed]), Config.scale_display)
    speedMax = 0 if X.empty else round(max(X[Config.WindSpeed]), Config.scale_display)
    directionMin = 0 if X.empty else round(min(X[Config.PitchAngle]), Config.scale_display)
    directionMax = 0 if X.empty else round(max(X[Config.PitchAngle]), Config.scale_display)
    powerMin = 0 if X.empty else round(min(X[Config.ActPower]), Config.scale_display)
    powerMax = 0 if X.empty else round(max(X[Config.ActPower]), Config.scale_display)

    attr = pd.DataFrame(
        {'数据长度': [len(X)], '最小风速': [speedMin], '最大风速': [speedMax], '最小风向': [directionMin], '最大风向': [directionMax],
         '最小功率': [powerMin], '最大功率': [powerMax]})
    attr.to_csv(to2, index=False)
    # print(from1+'----'+to2+'   toAttrCsv 完毕................')

def toLineRegressAttrCsv(from1, to2):
    X = pd.read_csv(from1)
    speedMin = 0 if X.empty else min(X[Config.WindSpeed])
    speedMax = 0 if X.empty else max(X[Config.WindSpeed])
    directionMin = 0 if X.empty else min(X[Config.PitchAngle])
    directionMax = 0 if X.empty else max(X[Config.PitchAngle])
    attr = pd.DataFrame({'数据长度': [len(X)], '最小风速': [speedMin], '最大风速': [speedMax], '最小风向': [directionMin], '最大风向': [directionMax]})
    attr.to_csv(to2, index=False)
    # print('toAttrCsv 完毕................')

def toLineCompareAttrCsv_Pitch(from1, to2):  # 针对偏航的
    X = pd.read_csv(from1)
    speedMin = 0 if X.empty else min(X[Config.WindSpeed])
    speedMax = 0 if X.empty else max(X[Config.WindSpeed])
    directionMin = 0 if X.empty else min(X[Config.PitchAngle])
    directionMax = 0 if X.empty else max(X[Config.PitchAngle])
    attr = pd.DataFrame(
        {'数据长度': [len(X)], '最小风速': [speedMin], '最大风速': [speedMax], '最小桨距角': [directionMin], '最大桨距角': [directionMax]})
    attr.to_csv(to2, index=False)
    # print('toAttrCsv 完毕................')

if __name__ == '__main__':
    from pitchSrc.pitchOnl.cleanProcess import onlCleanProcess
    X = toCsv(Config.onlInitialData, Config.onlCleanProcess, onlCleanProcess)
