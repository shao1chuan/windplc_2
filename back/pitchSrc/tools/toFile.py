from toolsSrc.files.checkFiles import checkCsvFiles
from common.config import pitchConfig as Config
import pandas as pd
import numpy as np
from toolsSrc.dbtools.getDBData import  getDBData1
from toolsSrc.toUploadCsv import changLimit,getTheorUploadRecord
from toolsSrc.status.status import Status
from toolsSrc.dbtools.sqltools import Sqltools

''' CALL BACK #################################################################################
1. 从数据库data1表中读取当前风速
2. 在 Config.onlLineRegress文件 中查找风速最接近的 pitchAngle
3. 将 {‘PitchOffset’:[pitchAngle]} 写入Config.pitchUpload文件
'''
from datetime import datetime

def getPitchOffset():
    # 1. 检查文件
    if checkCsvFiles([Config.onlLineRegress]) is False:
        return None

    # 2. 读dbData1中 当前风速、当前桨距角
    dbData1 = getDBData1()
    windSpeed = dbData1[Config.dbData1_windSpeedCol]
    actualPitchOffset = dbData1[Config.dbData1_actualPitchOffsetCol]

    # 3.onlLineRegress表中查找与当前风速最接近记录的理论偏航角[ 风速，曲线文件，输出列]
    theorPitchOffset = getTheorUploadRecord(windSpeed, Config.onlLineRegress, Config.PitchAngle)
    if not theorPitchOffset:  # 如果没找到相应的记录，什么都不做
        return None

    # 4. 对偏航角变化速度进行限制（根据通讯协议）
    limit = Config.pitchUploadSpan * Config.pitchOffset_limit
    pitchOffset = changLimit(actualPitchOffset, theorPitchOffset, limit)

    return pitchOffset

@Status.updateBefore('UPDATE tracking SET status=:status WHERE model=:model',[{"status": 0, "model": "pitchUploadCsv"}])
@Status.updateBA(
    "UPDATE tracking SET status=:status, startTime=:startTime WHERE model=:model and func=:func",
    [{"status": 1, "startTime":datetime.now(), "model": "pitchUploadCsv", "func": "toPitchUploadCsv"}],
    "UPDATE tracking SET status=:status, endTime=:endTime WHERE model=:model and func=:func",
    [{"status": 2, "endTime":datetime.now(), "model": "pitchUploadCsv", "func": "toPitchUploadCsv"}])
def toPitchUploadDB():
    pitchOffset = getPitchOffset()
    if pitchOffset is not None:
        Sqltools.excuteSql("UPDATE upload SET pitchOffset=:pitchOffset", [{"pitchOffset": pitchOffset}])


# @Status.updateBA(
#     "UPDATE tracking SET status=:status, startTime=:startTime WHERE model=:model and func=:func",
#     [{"status": 1, "startTime":datetime.now(), "model": "pitchUploadCsv", "func": "toPtichOffsetEnableUploadCsv"}],
#     "UPDATE tracking SET status=:status, endTime=:endTime WHERE model=:model and func=:func",
#     [{"status": 2, "endTime":datetime.now(), "model": "pitchUploadCsv", "func": "toPitchOffsetEnableUploadCsv"}])
# @Status.updateBefore('UPDATE tracking SET status=:status WHERE model=:model',[{"status": 0, "model": "toPtichOffsetEnableUploadCsv"}])
# # @resetPitchUploadDB
# def toPitchEnableUploadDB(value):
#     Sqltools.excuteSql("UPDATE upload SET pitchOffsetEnable=:pitchOffsetEnable", [{"pitchOffsetEnable": value}])
#
#     if False == value:
#         Sqltools.excuteSql("UPDATE upload SET pitchOffset=:pitchOffset", [{"pitchOffset": 0}])
#
#
#
# @Status.updateBA(
#     "UPDATE tracking SET status=:status, startTime=:startTime WHERE model=:model and func=:func",
#     [{"status": 1, "startTime":datetime.now(), "model": "pitchUploadCsv", "func": "setOnlyPitchOffsetEnableTrue"}],
#     "UPDATE tracking SET status=:status, endTime=:endTime WHERE model=:model and func=:func",
#     [{"status": 2, "endTime":datetime.now(), "model": "pitchUploadCsv", "func": "setOnlyPitchOffsetEnableTrue"}])
# @Status.updateBefore('UPDATE tracking SET status=:status WHERE model=:model',[{"status": 0, "model": "setOnlyPitchOffsetEnableTrue"}])
# # 只设置 pitchOffsetEnable 为True，其他都为 False
#
# def setOnlyPitchEnableUploadTrue():
#     Sqltools.excuteSql(
#         "UPDATE upload SET windDirOffsetEnable=:windDirOffsetEnable, pitchOffsetEnable=:pitchOffsetEnable, kOptOffsetEnable=:kOptOffsetEnable",
#         [{"windDirOffsetEnable": False, "pitchOffsetEnable": True, "kOptOffsetEnable": False}])
#     Sqltools.excuteSql("UPDATE upload SET windDirOffset=:windDirOffset, kOptOffset=:kOptOffset",
#                        [{"windDirOffset": 0, "kOptOffset": 1}])




if __name__ == '__main__':
    # toPitchUploadDB()
    # toPitchEnableUploadDB(True)
    setOnlyPitchEnableUploadTrue()