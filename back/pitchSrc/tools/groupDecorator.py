
from common.config import pitchConfig as Config


class GroupDecorator:
    def formulate(p2):
        def wrapper(X):
            # 1 去小数点
            # X[[Config.WindSpeed, Config.PitchAngle]] = round(X[[Config.WindSpeed, Config.PitchAngle]], Config.scale_PitchAngle)
            #X[Config.WindSpeed] = round(X[Config.WindSpeed], Config.scale_WindSpeed)
            #X[Config.PitchAngle] = round(X[Config.PitchAngle], Config.scale_PitchAngle)

            #1 地面划分网格
            X[Config.WindSpeed] = round(X[Config.WindSpeed]/Config.scale_WindSpeed, 0) * Config.scale_WindSpeed
            X[Config.PitchAngle] = round(X[Config.PitchAngle]/Config.scale_PitchAngle, 0) * Config.scale_PitchAngle

            # 2 排序
            X = X.sort_values([Config.PitchAngle, Config.WindSpeed], ascending=[True, True])
            # 3聚合
            X = X.groupby([Config.PitchAngle, Config.WindSpeed], as_index=False)[Config.ActPower]
            X= X.sum() if Config.groupBySum else X.mean()
            #X = X.groupby([Config.PitchAngle, Config.WindSpeed], as_index=False)[Config.ActPower].mean()
            # print(f"点云数据 ：{X},{X.shape}")
            # h = a - b if a > b else a + b

            # print('GroupDecorator. formulate 进行中................')
            return p2(X)
        return wrapper
