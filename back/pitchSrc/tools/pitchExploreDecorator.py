from pitchSrc.pitchHis.initData import initData
from pitchSrc.pitchHis.cleanProcess import cleanProcess
from pitchSrc.pitchHis.groupProcess import groupProcess
import shutil
import os
import numpy as np
from toolsSrc.status.status import Status
from common.config import pitchConfig as Config
import pandas as pd
import time
from toolsSrc.files.hisClean import hisClean
# from pitchSrc.tools.toFile import toPitchEnableUploadDB, setOnlyPitchEnableUploadTrue
from toolsSrc.dbtools.sqltools import Sqltools

'''##########################################################################################
针对 history 文件夹中的数据运行 pitchHis 中的 initData，cleanProcess，groupProcess
'''
# 该函数与groupProcess一致，将scale_WindSpeed->expScale_WindSpeed, scale_PitchAngle->expScale_PitchAngle
def expGroupProcess(X):
    X[Config.WindSpeed] = round(X[Config.WindSpeed] / Config.expScale_WindSpeed, 0) * Config.expScale_WindSpeed
    X[Config.PitchAngle] = round(X[Config.PitchAngle] / Config.expScale_PitchAngle, 0) * Config.expScale_PitchAngle
    X = X.sort_values([Config.PitchAngle, Config.WindSpeed], ascending=[True, True])
    X = X.groupby([Config.PitchAngle, Config.WindSpeed], as_index=False)[Config.ActPower]
    X = X.sum() if Config.groupBySum else X.mean()
    return X

def getGroupDf():
    files = os.listdir(Config.historyDataRoot)  # 获得文件名列表
    if len(files) > 0:
        initData()
        cleanProcess()
        x = pd.read_csv(Config.hisCleanProcess)
        df = expGroupProcess(x)
    else:
        df = pd.DataFrame(columns=[Config.PitchAngle, Config.WindSpeed], index=None)  #  0条记录会有问题么？？？？？？？
    return df

# def getGroupDf():
#     files = os.listdir(Config.historyDataRoot)  # 获得文件名列表
#     if len(files) > 0:
#         initData()
#         cleanProcess()
#         groupProcess()
#         df = pd.read_csv(Config.hisGroupProcess)
#     else:
#         df = pd.DataFrame(columns=[Config.PitchAngle, Config.WindSpeed], index=None)  #  0条记录会有问题么？？？？？？？
#     return df


'''
生成 ptichAngle, windSpeed 范围框
'''
def getPitchWindRectangle(df):
    # 得到范围框，这个范围应该适当缩小一点，否则可能永远也无法主动探索完毕
    pitch_min, pitch_max, wind_min, wind_max = df[Config.PitchAngle].min() + Config.expPitchAngleMargin, df[Config.PitchAngle].max() - Config.expPitchAngleMargin, df[
        Config.WindSpeed].min() + Config.expWindSpeedMargin, df[Config.WindSpeed].max() - Config.expWindSpeedMargin
    # 保证最小范围
    # pitch_min, pitch_max, wind_min, wind_max = pitch_min if pitch_min < Config.expPitchAngleMin else Config.expPitchAngleMin, pitch_max if pitch_max > Config.expPitchAngleMax else Config.expPitchAngleMax, wind_min if wind_min < Config.expWindSpeedMin else Config.expWindSpeedMin, wind_max if wind_max > Config.expWindSpeedMax else Config.expWindSpeedMax
    # 2023.2.2 日改成下面形式，主动探索范围只由config确定，
    pitch_min, pitch_max, wind_min, wind_max = Config.expPitchAngleMin,  Config.expPitchAngleMax,  Config.expWindSpeedMin,  Config.expWindSpeedMax
    return [pitch_min, pitch_max, wind_min, wind_max]

'''
生成确实点的 pitchAngle
'''
# 2023.2.8 之前，修正主动探索过程中，原 pitchHis 数据文件容易对 探索桨距角产生干扰。
# def getLackPitch(df_pitchWind):
#     # 得到ptichAngle, windSpeed范围框，
#     [pitch_min, pitch_max, wind_min, wind_max] = getPitchWindRectangle(df_pitchWind)
#
#     # 找到缺失的pitchAngle
#     windCount_full = round(
#         (wind_max - wind_min) / Config.expScale_WindSpeed) + 1  # 全的windSpeed格点数，例如，每pitchAngle下应该有24个windSpeed格点
#     df_windCount = df_pitchWind.groupby([Config.PitchAngle], as_index=False)[
#         Config.WindSpeed].count()  # Config.WindSpeed列变成了同一桨距角下风速点的数量
#
#     df_lackPitch = df_windCount[
#         df_windCount[Config.WindSpeed] < windCount_full]  # 如果没有缺的怎么办，会返回带 columns 空dataFrame
#     df_lackPitch = df_lackPitch.drop(Config.WindSpeed, axis=1)
#
#     # 找到整体缺失的pitchAngle
#     # pitchAngles = list(map(lambda x: '{:g}'.format(x), df_pitchWind[Config.PitchAngle]))  # 目前有哪些pitchAngle格点，为了匹配成功，用'{:g}'.format() 去掉多余的0
#     # 上面pitchAngles 中会出现 ‘-0’的情况，为了将其变为 ‘0’， 用下面的两条语句替代
#     df_pitchWind_0 = round((df_pitchWind[Config.PitchAngle] + 1e-5) / Config.expScale_PitchAngle,
#                            0) * Config.expScale_PitchAngle  #
#     pitchAngles = list(map(lambda x: '{:g}'.format(x), df_pitchWind_0))
#
#     # 空间中全部的pitchAngle
#     pitchAngles_full = np.arange(pitch_min, pitch_max + 1e-5, Config.expScale_PitchAngle)  # 全的pitchAngle格点
#     pitchAngles_full = np.round((pitchAngles_full + 1e-5) / Config.expScale_PitchAngle,
#                                 0) * Config.expScale_PitchAngle
#     pitchAngles_full = list(map(lambda x: '{:g}'.format(x), pitchAngles_full))
#     # print("pitchAngles_full: ", pitchAngles_full)
#     for pitch in pitchAngles_full:
#         if pitch not in pitchAngles:
#             df_lackPitch = pd.concat([df_lackPitch, pd.DataFrame({Config.PitchAngle: [pitch]})], ignore_index=True)
#
#     # 列名改为与PLC中一致，返回
#     df_lackPitch.columns = [Config.pitchPlcKey]
#
#     return df_lackPitch

# 2023.2.8 修正主动探索过程中，原 pitchHis 数据文件对 探索桨距角的干扰。
def getLackPitch(df_pitchWind):
    # 得到ptichAngle, windSpeed范围框，
    [pitch_min, pitch_max, wind_min, wind_max] = getPitchWindRectangle(df_pitchWind)
    # print("df_pitchWind1: ", df_pitchWind)
    #限定到风速范围
    df_pitchWind = df_pitchWind[df_pitchWind[Config.WindSpeed] >= wind_min-Config.expScale_WindSpeed/2]
    df_pitchWind = df_pitchWind[df_pitchWind[Config.WindSpeed] <= wind_max+Config.expScale_WindSpeed/2]
    # print("df_pitchWind11: ", df_pitchWind)
    # 全的windSpeed格点数，例如，每pitchAngle下应该有24个windSpeed格点
    windCount_full = round(
        (wind_max - wind_min) / Config.expScale_WindSpeed) + 1
    # print("windCount_full: ", windCount_full)
    # 将分散的pitchAngle归并到格点
    df_pitchWind[Config.PitchAngle] = round((df_pitchWind[Config.PitchAngle] + 1e-5) / Config.expScale_PitchAngle,
                           0) * Config.expScale_PitchAngle
    df_pitchWind[Config.PitchAngle] = list(map(lambda x: '{:g}'.format(x), df_pitchWind[Config.PitchAngle]))  # 避免出现 -0 的情况
    # print("df_pitchWind2: ", df_pitchWind)
    # 空间中全部的pitchAngle
    pitchAngles_full = np.arange(pitch_min, pitch_max + 1e-5, Config.expScale_PitchAngle)  # 全的pitchAngle格点
    pitchAngles_full = np.round((pitchAngles_full + 1e-5) / Config.expScale_PitchAngle,
                                0) * Config.expScale_PitchAngle
    pitchAngles_full = list(map(lambda x: '{:g}'.format(x), pitchAngles_full))  # 避免出现 -0 的情况
    # print("pitchAngles_full: ", pitchAngles_full)

    df_lackPitch = pd.DataFrame(columns=[Config.PitchAngle])
    for pitch in pitchAngles_full:
        count = len(df_pitchWind[df_pitchWind[Config.PitchAngle] == pitch])
        # print("pitch,count: ", pitch, count)
        if count < windCount_full: # 每个桨距角允许有5个风速点的空缺
            df_lackPitch = pd.concat([df_lackPitch, pd.DataFrame({Config.PitchAngle: [pitch]})], ignore_index=True)

    # 列名改为与PLC中一致，返回
    df_lackPitch.columns = [Config.pitchPlcKey]

    # print("df_lackPitch: ", df_lackPitch)

    return df_lackPitch



'''##########################################################################################
1. 分批次上传 df_ptichExplore 中的 pitch 数据
2. 每上传一个 pitch，采集一个数据文件，保存到history中
'''
def pitchExploreUpDown(df_pitchExplore):
    length = len(df_pitchExplore)
    for i in range(length):
        pitchExploreUpOne(df_pitchExplore, i)  # 将第i几条记录上传
        pitchExploreDownOne()  # 下载一个数据文件

'''
将 df_pitchExplore中第 i 条数据写入pitchUpload.csv(opcUa上传频率比价高，会马上上传）
'''
def pitchExploreUpOne(df_pitchExplore, i):

    pitchAngle = df_pitchExplore.iloc[i, 0]  # df_pitchExplore中第一行数据（主动探索的一个桨距角）
    Sqltools.excuteSql("UPDATE upload SET pitchOffset=:pitchOffset", [{"pitchOffset": pitchAngle}])
    print("pitchExploreUpOne pitchAngle: ", pitchAngle)
    # df_pitchUploadData = pd.DataFrame({Config.pitchPlcKey: [pitchAngle]})
    # print("pitchExploreUpOne df_pitchUploadData: ", df_pitchUploadData)  # -----------------test-------------
    # df_pitchUploadData.to_csv(Config.pitchUpload)

'''
根据时间戳，在数据池 pool 中取一个新生成的数据文件，拷贝到historyDataRoot文件夹中
'''
def pitchExploreDownOne():
    startTime = time.time()
    if not os.path.exists(Config.historyDataRoot): os.mkdir(Config.historyDataRoot)
    while True:
        files = os.listdir(Config.poolRoot)
        times = list(map(lambda fn: os.path.getmtime(Config.poolRoot + '/' + fn), files))
        if not times: continue  # 如果为空
        maxIndex = np.argmax(times)
        if times[maxIndex] > startTime:
            # shutil.copy(Config.poolRoot + files[maxIndex], Config.historyDataRoot)
            shutil.move(Config.poolRoot + files[maxIndex], Config.historyDataRoot)
            break
        time.sleep(1)  # 等待时间不能小于 pool 采集一个文件的时间， 用在config中设置么？？？？？？？？？？？？？？？？？？？？





