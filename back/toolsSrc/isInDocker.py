import os
def isInDocker():
    SECRET_KEY = os.environ.get('AM_I_IN_A_DOCKER_CONTAINER', False)

    if os.path.exists('/.dockerenv') or SECRET_KEY:
        return True
    else:
        return False

if __name__ == '__main__':
    print(isInDocker())