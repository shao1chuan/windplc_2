import os
from toolsSrc.status.status import Status
from datetime import datetime

@Status.updateBA(
    "UPDATE tracking SET status=:status, startTime=:startTime WHERE model=:model and func=:func",
    [{"status": 1, "startTime":datetime.now(), "model": "opcUa", "func": "poolClean"}],
    "UPDATE tracking SET status=:status, endTime=:endTime WHERE model=:model and func=:func",
    [{"status": 2, "endTime":datetime.now(), "model": "opcUa", "func": "poolClean"}])
def poolClean(path,poolNum):
    print('poolClean 开始.....................')
    files = os.listdir(path)
    # files.remove("init0")
    file_nums = len(files)
    files.sort(key=lambda fn: os.path.getmtime(path + fn))
    if file_nums > poolNum:
        for i in range(file_nums - poolNum):
            if os.path.isfile(path + files[i]):
                os.remove(path + files[i])
    # print(os.listdir(path))
    print('poolClean 结束......................')
if __name__ == '__main__':
    path = "E:\\pycharmproject\\windhr_5\\back\\data\\dataTest\\plc\\pool\\"
    poolClean(path,2)






