from src.main.sub_server import sub_app
from fastapi import Query
import json
import csv

@sub_app.get("/csv_select")
def csv_select(csv_file_path: str = Query(..., description="The path to the CSV file")):
    # 打开CSV文件
    with open(csv_file_path, mode='r', encoding='utf-8') as file:
        # 创建CSV读取器
        reader = csv.DictReader(file)

        # 读取CSV文件中的所有行，并转换为字典列表
        data_list = [row for row in reader]

    # 转换为JSON
    json_result = json.dumps(data_list, ensure_ascii=False)
    return json_result

if __name__ == '__main__':
    # csv_file_path = '../../data/data/yawData/onlLineRegress.csv'  # 替换为您的CSV文件路径
    # json_result = csv_select(csv_file_path)
    # # 打印结果
    # print(json_result)
    import uvicorn
    uvicorn.run(app, host="127.0.0.1", port=8000)
    # http://127.0.0.1:8000/csv_select?csv_file_path=../../data/data/yawData/onlLineRegress.csv
    # http://127.0.0.1:8000/csv_select?csv_file_path=../../data/data/pitchData/onlLineRegress.csv




