from common.config import yawConfig as Config
from toolsSrc.files.loadFiles import loadfile
import pandas as pd
from toolsSrc.en2ch import en2ch,dropColums,isColumnsFull
from toolsSrc.indbtools.indb2df import indb2df
import time
def fromRootToCsv(root,fileName,columsList):
    X = loadfile(root, columsList,None)  # 读取Config().poolRoot目录下的所有数据文件
    if X.empty:
        X = pd.DataFrame([[0 for i in range(len(columsList))]], columns=columsList)
    X = X[columsList]
    X.to_csv(fileName, index=False)


def fromInDbToCsv(fileName, columsList):
    indb_selectSql = Config.indb_selectSql
    X = indb2df(indb_selectSql)
    dropColums(X)
    en2ch(X)
    if X.empty:
        X = pd.DataFrame([[0 for i in range(len(columsList))]], columns=columsList)
    X.to_csv(fileName, index=False)
    return X


def saveDatam(root):
    print('saveDatam 开始......................')
    # 以时间命名文件名
    begintime = time.strftime("%Y.%m.%d.%H.%M.%S", time.localtime(time.time()-3600))
    indb_selectSql1h = Config.indb_selectSql1h
    X = indb2df(indb_selectSql1h)
    if X.empty:
        print('-------没有数据，saveDatam 结束......................')
        return
    print("------------------从influxdb中取得的数据-------------：",X.head(2))
    dropColums(X)
    en2ch(X)
    if isColumnsFull(X):
        endtime = time.strftime("%Y.%m.%d.%H.%M.%S", time.localtime(time.time()))
        fileName = begintime + '-' + endtime + '-' + str(Config.serverID) + '.csv'
        print(root,fileName)
        X.to_csv(root + '/' + fileName, index=False)
        print(f"--------------datam..............",X.head(2))
        print('saveDatam 结束......................')

if __name__ == '__main__':
    columsList = [Config.WindSpeed, Config.ActPower, Config.WindDirection, Config.DataEffectiveFlag]
    fromInDbToCsv("1.csv",columsList)

