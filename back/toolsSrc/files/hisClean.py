import os
def hisClean(path,hisNumLimit):
    print("hisClean 开始.............")
    files = os.listdir(path)
    file_nums = len(files)
    files.sort(key=lambda fn: os.path.getmtime(path + '/' + fn))
    if file_nums > hisNumLimit:
        for i in range(file_nums - hisNumLimit):
            os.remove(path + '/' + files[i])
    print("hisClean 结束..............")
