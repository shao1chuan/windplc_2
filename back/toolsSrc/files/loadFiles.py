import os
import pandas as pd
from toolsSrc.files.checkFiles import checkCsvFiles
from common.config import commonConfig as Config

from datetime import datetime,timedelta

''' READ 原始数据文件 #################################################################################  '''
'''
# 读取一个csv文件的 [风速，功率、风向] 列 
'''
def readcsv(filename, columns):
    csv_data = pd.read_csv(filename, usecols=columns )  # 读取训练数据
    return csv_data

'''
# 遍历并读取目录下的所有数据文件的 [风速，功率、风向] 列,生成一个DataFrame类型变量
'''
# def loadfile(dir, columns):
#     X = pd.DataFrame()
#     if os.path.isfile(dir):  # 如果是文件的话，读取该文件
#         return readcsv(dir, columns)
#     filenames = os.listdir(dir)  # 如果是文件夹的话，遍历文件夹
#     for name in filenames:
#         dirorfile = os.path.join(dir, name)
#         if os.path.isfile(dirorfile) and dirorfile.split(".")[-1] == "csv":
#             x = readcsv(dirorfile, columns)  # 类型：pandas.core.frame.DataFrame
#             X = pd.concat([X, x])
#             # print(len(X))
#             continue
#         if os.path.isdir(dirorfile):
#             # print(f"dir name is :{dirorfile}")
#
#             X = pd.concat([X, loadfile(dirorfile, columns)])
#             continue
#     return X
'''
如果hourSpan==None,读取dir目录下的所以数据，如果hourSpan！=None,读取距现在hourSpan小时以内的文件
'''
def loadfile(dir, columns,hourSpan):
    X = pd.DataFrame()
    if hourSpan == None:
        filenames = os.listdir(dir)  # 如果是文件夹的话，遍历文件夹
    else:
        lists = os.listdir(dir)
        lists.sort(key=lambda fn: os.path.getmtime(dir + '/' + fn), reverse=True)
        loadFileNum = round(hourSpan*3600/Config.datamSpan)
        filenames = lists[:loadFileNum]  # 否则遍历n个文件
    print("-----------------loadfile加载文件数为------------------： ",len(filenames))
    for name in filenames:
        dirorfile = os.path.join(dir, name)
        if os.path.isfile(dirorfile) and dirorfile.split(".")[-1] == "csv":
            x = readcsv(dirorfile, columns)  # 类型：pandas.core.frame.DataFrame
            X = pd.concat([X, x])
            continue
        if os.path.isdir(dirorfile):
            # print(f"dir name is :{dirorfile}")
            X = pd.concat([X, loadfile(dirorfile, columns,hourSpan)])
            continue
    print("-----------------loadfile加载数据长度------------------： ", len(X))
    return X



'''
只有 torqueSrc 调用该函数
# 以独立形式分别读取目录下所有数据文件[风速，功率、风向] 列，以字典{文件名：数据，......}形式返回
'''
def loadfileIndepend(dir,X, columns, hourSpan):
    if hourSpan == None:
        filenames = os.listdir(dir)  # 如果是文件夹的话，遍历文件夹
    else:
        lists = os.listdir(dir)
        lists.sort(key=lambda fn: os.path.getmtime(dir + '/' + fn), reverse=True)
        loadFileNum = round(hourSpan * 3600 / Config.datamSpan)
        # filenames = lists[:int(hourSpan)]  # 否则遍历n个文件
        filenames = lists[:loadFileNum]  # 否则遍历n个文件
    print("-----------------loadfileIndipend加载文件数为------------------： ",len(filenames))
    for name in filenames:
        dirorfile = os.path.join(dir, name)
        if os.path.isfile(dirorfile) and dirorfile.split(".")[-1] == "csv":
            x = readcsv(dirorfile, columns)  # 类型：pandas.core.frame.DataFrame
            X[name] = x
            continue
        if os.path.isdir(dirorfile):
            loadfileIndepend(dirorfile, X, columns, hourSpan)
            continue
    print("-----------------loadfileIndepend加载数据长度------------------： ", len(X))
    return X

# def loadfileIndepend(dir,X, columslist):
#     if os.path.isfile(dir) and dir.split(".")[-1] == "csv":  # 如果是文件，读取该文件
#         filename = os.path.split(dir)
#         x = readcsv(dir, columslist)
#         X[filename] = x
#         return
#     filenames = os.listdir(dir)  # 如果是文件夹，遍历文件夹
#     for name in filenames:
#         dirorfile = os.path.join(dir, name)
#         if os.path.isfile(dirorfile) and dirorfile.split(".")[-1] == "csv":  # 如果是文件
#             # print(f"read file :{dirorfile}")
#             x = readcsv(dirorfile, columslist)  # 类型：pandas.core.frame.DataFrame
#             X[name] = x
#             continue
#         if os.path.isdir(dirorfile):
#             loadfileIndepend(dirorfile, X, columslist)
#             continue
#     return

'''
用于曲线对比：读一个曲线文件，并设定“图例”名称
'''
def readLineCsvSetLegend(filePath, legend):
    if not checkCsvFiles([filePath]):
        return pd.DataFrame()
    X = pd.read_csv(filePath)
    X[Config.legendColName] = legend
    return X

# def loadfile(dir, columns,hourSpan):
#     X = pd.DataFrame()
#     if os.path.isfile(dir):  # 如果是文件的话，读取该文件
#         return readcsv(dir, columns)
#     filenames = os.listdir(dir)  # 如果是文件夹的话，遍历文件夹
#     if hourSpan == None:
#         for name in filenames:
#                 dirorfile = os.path.join(dir, name)
#                 if os.path.isfile(dirorfile) and dirorfile.split(".")[-1] == "csv":
#                     print(dirorfile, columns)
#                     x = readcsv(dirorfile, columns)  # 类型：pandas.core.frame.DataFrame
#                     X = pd.concat([X, x])
#                     continue
#                 if os.path.isdir(dirorfile):
#                     # print(f"dir name is :{dirorfile}")
#                     X = pd.concat([X, loadfile(dirorfile, columns,hourSpan)])
#                     continue
#     else:
#         for name in filenames:
#             dirorfile = os.path.join(dir, name)
#             if os.path.isfile(dirorfile) and dirorfile.split(".")[-1] == "csv":
#                 fileT = datetime.strptime(dirorfile.split("-")[1], "%Y.%m.%d.%H.%M.%S")
#                 nowT_ = datetime.now() - timedelta(minutes=hourSpan)
#                 if (fileT >= nowT_):
#                     x = readcsv(dirorfile, columns)  # 类型：pandas.core.frame.DataFrame
#                     X = pd.concat([X, x])
#                 # print(len(X))
#                 continue
#             if os.path.isdir(dirorfile):
#                 X = pd.concat([X, loadfile(dirorfile, columns, hourSpan)])
#                 continue
#     # print("loaddata 加载数据长度 ", len(X))
#     return X
if __name__ == '__main__':
    dir = "E:\\pycharmproject\\windhr_5\\back\\data\\dataTest\\plc\\pool\\"
    columns = ['风速(m/s)', '功率(KW)', '偏航(deg)']
    loadfile(dir, columns,None)