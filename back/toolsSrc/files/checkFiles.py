import os
import pandas as pd
from torqueSrc.tools.cleanDecorator import CleanDecorator, clean_withTemp

'''
如果列表 list_files 中的文件都存在，并且不为空，返回True，否则返回False
'''
def checkCsvFiles(list_files):
    for file in list_files:
        if not os.path.exists(file):
            print("checkCsvFiles 以下文件还没有生成", file)
            return False
        df = pd.read_csv(file)
        if len(df) < 1:
            print("checkCsvFiles 以下文件为空", file)
            return False
    return True


'''
如果列表 list_files 中的文件都存在，并且记录数>1，返回True，否则返回False
'''
def checkCsvFiles_h2(list_files):
    for file in list_files:
        if not os.path.exists(file):
            # print("checkCsvFiles 以下文件还没有生成", file)
            return False
        df = pd.read_csv(file)
        if len(df) < 2:
            # print("checkCsvFiles 以下文件为空", file)
            return False
    return True


'''
如果 file中 column 列的值为 value 的记录数 大于 minLimitCount 返回 True，否则 返回 False
'''
def checkCleanCount(file, minLimitCount):
    if file is None: return False
    if not os.path.exists(file): return False
    if os.path.getsize(file) <= 0: return False
    df = pd.read_csv(file)
    # print("df: ", df)
    df = clean_withTemp(df)
    count = len(df)  # 1==True
    if count >= minLimitCount:
        return True
    else:
        return False

import numpy as np
if __name__ == "__main__":

    pass