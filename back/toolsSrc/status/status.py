import time
from functools import wraps
from toolsSrc.dbtools.dbtools import DBTools
from sqlalchemy import text
from toolsSrc.dbtools.getDBData import getUrl
from datetime import datetime

class Status:
    '''----------------------  CSV status---------------------------------------------------------'''
    # @staticmethod
    # def writeStatus(name, value):
    #     df = pd.read_csv(Config.status)
    #     condition = df["name"] == name
    #     df.loc[condition, "value"] = value
    #     df.to_csv(Config.status, index=False)
    #
    # @staticmethod
    # def changeStatus(before,after):
    #     def changeStatus_decorator(func):
    #         @wraps(func)
    #         def wrap_function():
    #             Status.writeStatus(func.__name__, before)
    #             ret = func()
    #             Status.writeStatus(func.__name__, after)
    #             return ret
    #         return wrap_function
    #     return changeStatus_decorator

    '''----------------------  DB status---------------------------------------------------------'''
    @staticmethod
    def writeDBStatus(columName, value):
        session = DBTools(getUrl()).connectDB()
        result = session.execute(
                text("UPDATE status SET value=:value WHERE name=:name"), [{"name": columName, "value": value}])
        session.commit()
        session.remove()

    @staticmethod
    def changeDBStatus(name, before, after):
        def changeStatus_decorator(func):
            @wraps(func)
            def wrap_function():
                Status.writeDBStatus(name, before)
                ret = func()
                Status.writeDBStatus(name, after)
                return ret
            return wrap_function
        return changeStatus_decorator

    '''------------------- DB tracking---------------------------------------------------------'''
    # @staticmethod
    # def writeDBTrackingStart(model, func):
    #     startTime = datetime.now()
    #     session = DBTools(getUrl()).connectDB()
    #     result = session.execute(
    #         text("UPDATE tracking SET status=:status, startTime=:startTime WHERE model=:model and func=:func"), [{"status": 1, "startTime":startTime, "model": model, "func": func}])
    #     session.commit()
    #     session.remove()
    #
    # @staticmethod
    # def writeDBTrackingEnd(model, func):
    #     endTime = datetime.now()
    #     session = DBTools(getUrl()).connectDB()
    #     result = session.execute(
    #         text("UPDATE tracking SET status=:status, endTime=:endTime WHERE model=:model and func=:func"), [{"status": 2, "endTime":endTime, "model": model, "func": func}])
    #     session.commit()
    #     session.remove()
    #
    # @staticmethod
    # def changeDBTracking(model):
    #     def changeTracking_decorator(func):
    #         @wraps(func)
    #         def wrap_function(*args, **kwargs):
    #             Status.writeDBTrackingStart(model, func.__name__ )
    #             ret = func(*args, **kwargs)
    #             Status.writeDBTrackingEnd(model, func.__name__ )
    #             return ret
    #         return wrap_function
    #     return changeTracking_decorator


    @staticmethod
    def updateBefore(sql, list):
        def updateBefore_decorator(func):
            @wraps(func)
            def wrap_function(*args, **kwargs):
                session = DBTools(getUrl()).connectDB()
                session.execute(
                    text(sql), list)
                session.commit()
                session.remove()
                ret = func(*args, **kwargs)
                return ret
            return wrap_function
        return updateBefore_decorator

    @staticmethod
    def updateAfter(sql, list):
        def updateAfter_decorator(func):
            @wraps(func)
            def wrap_function(*args, **kwargs):
                ret = func(*args, **kwargs)
                session = DBTools(getUrl()).connectDB()
                session.execute(
                    text(sql), list)
                list[0]["endTime"] = datetime.now()
                session.commit()
                session.remove()
                return ret
            return wrap_function
        return updateAfter_decorator

    # @staticmethod
    # def updateBA(sqlB,listB,sqlA,listA):
    #     def updateBothBA_decorator(func):
    #         @wraps(func)
    #         def wrap_function(*args, **kwargs):
    #             session = DBTools(getUrl()).connectDB()
    #             session.execute(
    #                 text(sqlB), listB)
    #             ret = func(*args, **kwargs)
    #             session.execute(
    #                 text(sqlA), listA)
    #             session.commit()
    #             session.remove()
    #             return ret
    #         return wrap_function
    #     return updateBothBA_decorator

    @staticmethod
    def updateBA(sqlB, listB, sqlA, listA):
        def updateB_A_decorator(func):
            @wraps(func)
            def wrap_function(*args, **kwargs):
                session = DBTools(getUrl()).connectDB()
                listB[0]["startTime"] = datetime.now()
                session.execute(
                    text(sqlB), listB)
                session.commit()
                session.remove()
                ret = func(*args, **kwargs)
                session = DBTools(getUrl()).connectDB()
                listA[0]["endTime"] = datetime.now()
                session.execute(
                    text(sqlA), listA)
                session.commit()
                session.remove()
                return ret

            return wrap_function

        return updateB_A_decorator
if __name__ == '__main__':
    from toolsSrc.status.status import Status
    @Status.updateBA(
        "UPDATE tracking SET status=:status, startTime=:startTime WHERE model=:model and func=:func",
        [{"status": 1, "startTime": datetime.now(), "model": "opcUa", "func": "poolClean"}],
        "UPDATE tracking SET status=:status, endTime=:endTime WHERE model=:model and func=:func",
        [{"status": 2, "endTime": datetime.now(), "model": "opcUa", "func": "poolClean"}])
    def test():
        print('开始................')
        time.sleep(5)
        print('完毕................')
        # show3dFig(Config.onlSurfaceProcess)


    @Status.updateBA(
        "UPDATE tracking SET status=:status, startTime=:startTime WHERE model=:model and func=:func",
        [{"status": 1, "startTime": datetime.now(), "model": "opcUa", "func": "poolClean"}],
        "UPDATE tracking SET status=:status, endTime=:endTime WHERE model=:model and func=:func",
        [{"status": 2, "endTime": datetime.now(), "model": "opcUa", "func": "poolClean"}])
    def test2():
            print('开始................')
            time.sleep(5)
            print('完毕................')
            # show3dFig(Config.onlSurfaceProcess)
    test()
    test2()

