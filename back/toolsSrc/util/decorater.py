import time
class Decorator:
    def toCsv(func,path):
        def wrapper(X):
            # 以时间命名文件名
            endtime = time.strftime("%Y.%m.%d.%H.%M.%S", time.localtime(time.time()))
            fileName = endtime + '.csv'
            X.to_csv(path + '/' + fileName, index=False)
            return func(X)
        return wrapper