# from common.config import yawConfig as Config
import pandas as pd
class Plcweb:
    def returnName(func):
        def wrapper():
            # 调用原函数
            X = func()
            # 使用三元运算符返回相应的结果
            return {'funcName': [func.__name__], 'dataColumns':X.columns.tolist(),'dataLen': [len(X)]} if X is not None else {'funcName': [func.__name__]}
        return wrapper

    def returnD(func):
        def wrapper():
            # 调用原函数
            X = func()
            # 使用三元运算符返回相应的结果
            return {'funcName': [func.__name__], 'dataLen': [len(X)]} if X is not None else {'funcName': [func.__name__]}
        return wrapper


