def dropColums(X):
    cols_to_drop = [col for col in ['result', 'table', '_start', '_stop', '_measurement', 'host', 'id','Quality'] if col in X.columns]
    # 如果存在，则删除这些列
    if cols_to_drop:
        X.drop(cols_to_drop, axis=1, inplace=True)

def en2ch(X):
    EngToCh = {"WindSpeed": "风速(m/s)", "ActivePower": "功率(KW)", "WindDirection": "偏航(deg)",
               "DataEffectiveFlag": "DataEffectiveFlag","GeneratorSpeed": "发电机转速(rpm)","PitchAngle": "桨距角(deg)",
               "TempAmbient": "TempAmbient","GenertorTorq":"GenertorTorq","WindDirectionToNorth":"WindDirectionToNorth",
               "HeartBeatFlagSever":"HeartBeatFlagSever","ActualWindDirOffset":"ActualWindDirOffset","ActualPitchOffset":"ActualPitchOffset",
               "ActualKoptOffset":"ActualKoptOffset","WindDirOffset":"WindDirOffset","PitchOffset":"PitchOffset","KoptOffseet":"KoptOffseet",
               "WindDirOffsetEnable":"WindDirOffsetEnable","PitchOffsetEnable":"PitchOffsetEnable","KoptOffsetEnable":"KoptOffsetEnable",
               "ClientHeartBeatFlag":"ClientHeartBeatFlag"}
    chs = [EngToCh[X.columns[i]] for i in range(len(X.columns))]
    X.columns = chs

def isColumnsFull(X):
    column_exists = '风速(m/s)' in X.columns \
                    and "功率(KW)" in X.columns \
                    and "偏航(deg)" in X.columns \
                    and "DataEffectiveFlag" in X.columns \
                    and "桨距角(deg)" in X.columns \
                    and "发电机转速(rpm)" in X.columns
    return column_exists



