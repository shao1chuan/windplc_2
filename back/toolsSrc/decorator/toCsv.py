
import time
class ToCsvDecorator:
    def __init__(self, path):
        self.path = path
    def __call__(self, fn):
        def wrap(X):
            X = X if len(X) < 600000 else X.sample(600000)  #600000 相当于15M
            X.to_csv(self.path + '/' + time.strftime("%Y.%m.%d.%H.%M.%S", time.localtime(time.time())) + '.csv', index=False)
            result = fn(X)
            return result
        return wrap

# def toCsv(path):
#     def decorator(fn):
#         @wraps(fn)
#         def wrapper(X):
#             endtime = time.strftime("%Y.%m.%d.%H.%M.%S", time.localtime(time.time()))
#             fileName = endtime + '.csv'
#             X.to_csv(path + '/' + fileName, index=False)
#             result = fn(X)
#             return result
#         return wrapper
#     return decorator