import numpy as np
import pandas as pd
from common.config import commonConfig as Config

'''
将输出 after 限定在 [before-toUploadCsv, before+toUploadCsv]范围内，要求参数 toUploadCsv 是正数
'''
def changLimit(before, after, limit):
    if after - before > limit:
        return before + limit
    if after - before < -limit:
        return before - limit
    return after

'''
onlLineRegress表中查找与当前风速最接近记录的理论偏航角
'''
def getTheorUploadRecord(windSpeed, onlLineRegressFile, colName):
    df_onlLineRegress = pd.read_csv(onlLineRegressFile)
    WindSpeed_new = np.array(abs(df_onlLineRegress[Config.WindSpeed] - windSpeed))
    min = np.min(WindSpeed_new)
    indexMin = np.argmin(WindSpeed_new)
    if min > 1:
        return  # 如果最小偏差都很大，不进行后面的上传操作
    theorUpload = df_onlLineRegress.loc[indexMin, colName]
    return theorUpload

if __name__ == "__main__":
    result = changLimit(1, 2, 0.5)
    print(result)