from influxdb_client import InfluxDBClient
from common.config import commonConfig as Config
import pandas as pd
from toolsSrc.isInDocker import isInDocker

def indb2df(sql):
    url = Config.docker_indb_url if isInDocker() else Config.local_indb_url
    client = InfluxDBClient(url=url, token=Config.indb_token, org=Config.indb_org, timeout=1000000)
    query_api = client.query_api()
    print("influxdb url------------", url)

    # 执行查询
    result = query_api.query(query=sql)
    dfs = []  # 用于存储每个 FluxTable 转换后的 DataFrame

    for table in result:
        # 将 FluxRecord 转换为字典列表
        records = [record.values for record in table.records]
        if records:
            # 将字典列表转换为 DataFrame
            df = pd.DataFrame(records)
            # 可以对 df 进行进一步的处理，比如设置时间索引
            if '_time' in df.columns:
                df['_time'] = pd.to_datetime(df['_time'])  # 将时间字符串转换为 datetime 对象
                df.set_index('_time', inplace=True)
            dfs.append(df)

    # 合并所有 DataFrame
    if dfs:
        result_df = pd.concat(dfs, sort=False)
        return result_df
    else:
        print("influxdb 中取到数据为空 No data to concatenate.")
        return pd.DataFrame()

    client.close()

if __name__ == '__main__':
    sql = '''
    from(bucket: "plc")
    |> range(start: -30d)
    |> filter(fn: (r) => r._measurement == "opcua")
    |> filter(fn: (r) =>  r._value == r._value) 
    |> filter(fn: (r) => exists r._value)
    |> pivot(rowKey:["_time"], columnKey: ["_field"], valueColumn: "_value")
    |> sort(desc: true)
    '''
    df = indb2df(sql)
    print(df.columns)
    print(df.head(2))
