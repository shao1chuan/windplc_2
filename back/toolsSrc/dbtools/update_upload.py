from toolsSrc.files.checkFiles import checkCsvFiles
from common.config import pitchConfig as Config
import pandas as pd
import numpy as np
from toolsSrc.dbtools.getDBData import  getDBData1
from toolsSrc.toUploadCsv import changLimit,getTheorUploadRecord
from toolsSrc.status.status import Status
from toolsSrc.dbtools.sqltools import Sqltools


def update_upload(list):
    sql = "UPDATE upload SET "
    for key in list[0].keys():
        l = key +"=:"+key+","
        sql +=l
    sql = sql[:-1]
    # print(sql)
    Sqltools.excuteSql(sql,list)

if __name__ == '__main__':
    list = [{"windDirOffsetEnable": False, "windDirOffset": 0, "pitchOffsetEnable": True, "kOptOffsetEnable": False,
             "kOptOffset": 1}]
    update_upload(list)