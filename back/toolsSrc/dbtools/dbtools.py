from functools import wraps
from sqlalchemy import create_engine
from sqlalchemy import text
from sqlalchemy.orm import Session
import os
from sqlalchemy import pool
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import scoped_session
from common.config import commonConfig as Config

def flyweight(cls):
    _instance = {}

    def _make_arguments_to_key(args, kwds):
        key = args
        if kwds:
            sorted_items = sorted(kwds.items())
            for item in sorted_items:
                key += item
        return key

    @wraps(cls)
    def _flyweight(*args, **kwargs):
        cache_key = f'{cls}_{_make_arguments_to_key(args, kwargs)}'
        if cache_key not in _instance:
            _instance[cache_key] = cls(*args, **kwargs)
        return _instance[cache_key]

    return _flyweight

@flyweight
class DBTools:
    def __init__(self, url):
        self.url = url
        engine = create_engine(
            url,
            max_overflow=10,  # 超过连接池大小外最多创建的连接
            pool_size=100,  # 连接池大小
            pool_timeout=30,  # 池中没有线程最多等待的时间，否则报错
            pool_recycle=-1  # 多久之后对线程池中的线程进行一次连接的回收（重置）
        )
        SessionFactory = sessionmaker(bind=engine)
        self.session  = scoped_session(SessionFactory)
        # self.session = Session(create_engine(self.url))
    def connectDB(self):
        return self.session
        # return Session(create_engine(self.url))

def getUrl():
    SECRET_KEY = os.environ.get('AM_I_IN_A_DOCKER_CONTAINER', False)
    # print("$$$$$$$$$$$$$$$$",SECRET_KEY)
    # if SECRET_KEY:
    #     print('I am running in a Docker container')
    if os.path.exists('/.dockerenv') or SECRET_KEY:
        url = Config.docker_url
    else:
        url = Config.local_url
    return url

if __name__ == '__main__':
    from common.config import commonConfig as Config
    from toolsSrc.isInDocker import isInDocker
    # url = 'mysql+pymysql://root:123456@localhost:3306/eladmin?charset=utf8'
    url = Config.docker_url if isInDocker() else Config.local_url
    columName = "preparePitchExploreData"
    value = 122
    session = DBTools(url).connectDB()
    # result = session.execute(
    #     text("UPDATE status SET value=:value WHERE name=:name"), [{"name": columName, "value": value}])

    result = session.execute(
        text("select * from data1"))
    print(result.all())
    session.commit()
    session.close()
    # session.close()


    # sql ="""select name from df toUploadCsv 2"""

