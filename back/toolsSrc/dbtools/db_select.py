from src.main.sub_server import sub_app
from toolsSrc.dbtools.dbtools import DBTools
from toolsSrc.dbtools.dbtools import getUrl
import json
from sqlalchemy import text

@sub_app.get("/db_select")
def db_select():
    sql_query = "SELECT * FROM upload "
    params = {'param': 'desired_value'}
    session = DBTools(getUrl()).connectDB()
    result = session.execute(text(sql_query), params)
    session.commit()
    # 将结果转换为字典列表
    rows = result.fetchall()
    columns = result.keys()
    result_list = [dict(zip(columns, row)) for row in rows]
    session.close()  # 使用close而不是remove
    # 转换为JSON
    json_result = json.dumps(result_list, ensure_ascii=False)
    return json_result
if __name__ == '__main__':
    json_result = db_select()
    # 打印结果
    print(json_result)