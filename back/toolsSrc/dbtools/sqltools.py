
from toolsSrc.dbtools.dbtools import DBTools
from sqlalchemy import text
from toolsSrc.dbtools.getDBData import getUrl

class Sqltools:
    def excuteSql(txt, list):
        session = DBTools(getUrl()).connectDB()
        result = session.execute(
                text(txt),list)
        session.commit()
        session.remove()
