
from toolsSrc.dbtools.dbtools import DBTools
from sqlalchemy import text
from toolsSrc.dbtools.dbtools import getUrl
def getDBData1():
    url = getUrl()
    session = DBTools(url).connectDB()
    result = session.execute(
        text("select * from data1"))
    session.commit()
    session.close()
    return result.all()[0]  # 返回元组

def getDBStatus():
    url = getUrl()
    session = DBTools(url).connectDB()
    result = session.execute(
        text("select * from status")
    )
    session.commit()
    session.close()
    return result.all()

def sql(sql, list):
    session = DBTools(getUrl()).connectDB()
    result = session.execute(
        text(sql), list)
    session.commit()
    session.remove()
    return result.all()


if __name__ == '__main__':
    # getDBData1()
    # print("getDBData1: ", getDBData1())
    print("getDBStatus: ", getDBStatus())
