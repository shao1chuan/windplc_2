'''
# 当前最优工作线与原始工作线对比
'''
from torqueSrc.tools.loadFile import toLineRegressCsv, toAttrCsv, toLineRegressAttrCsv
import pandas as pd
from common.config import torqueConfig as Config
from toolsSrc.files.loadFiles import readLineCsvSetLegend
from toolsSrc.status.status import Status
from datetime import datetime
from src.main.sub_server import sub_app
from toolsSrc.util.plcweb import Plcweb

@sub_app.get("/torqueHisLambdaOptCompareProcess")
@Status.updateBA(
    "UPDATE tracking SET status=:status, startTime=:startTime WHERE model=:model and func=:func",
    [{"status": 1, "startTime":datetime.now(), "model": "torqueHis", "func": "lambdaOptCompareProcess"}],
    "UPDATE tracking SET status=:status, endTime=:endTime WHERE model=:model and func=:func",
    [{"status": 2, "endTime":datetime.now(), "model": "torqueHis", "func": "lambdaOptCompareProcess"}])
@Plcweb.returnName
def lambdaOptCompareProcess():
    print('torqueHis lineCompareProcess 开始................')
    # 1. 两个曲线文件合并成一个文件，并用图例区分
    X = readLineCsvSetLegend(Config.originLambda, '原始工作曲线')
    X = pd.concat([X, readLineCsvSetLegend(Config.hisLambdaOptProcess, '最优工作曲线')] )
    X = pd.DataFrame(X, columns=[Config.WindSpeed, Config.RotateSpeed, Config.legendColName])
    X.to_csv(Config.lambdaOptCompareProcess, index=False)

    # 2. 生成属性文件
    toLineRegressAttrCsv(Config.lambdaOptCompareProcess, Config.lambdaOptCompareAttr)
    print('torqueHis lineCompareProcess 结束................')
    return X

if __name__ == '__main__':
    lambdaOptCompareProcess()






