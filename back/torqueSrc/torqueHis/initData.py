from common.config import torqueConfig as Config
# from torqueSrc.tools.loadFile import loadfile, toAttrCsv
from torqueSrc.tools.loadFile import toAttrCsv
from toolsSrc.files.loadFiles import loadfile

import pandas as pd
from toolsSrc.status.status import Status
from src.main.sub_server import sub_app
from toolsSrc.util.plcweb import Plcweb

# @CleanDecorator.drop_duplicates
# @CleanDecorator.drop_bigorsmall
#@CleanDecorator.drop_dbscan
def initDataProcess(X):
    return X


def toCsv():
    columsList = [Config.WindSpeed, Config.ActPower, Config.RotateSpeed, Config.TempAmbient, Config.DataEffectiveFlag]
    X = loadfile(Config.historyDataRoot, columsList, None)  # 读取Cofnig().historyDataRoot目录下的所有数据文件
    if X.empty:
        X = pd.DataFrame([[0 for i in range(len(columsList))]], columns=columsList)
    X = X[columsList]
    X.to_csv(Config.hisInitialData, index=False)

from datetime import datetime
@sub_app.get("/torqueHisInitialData")
@Status.updateBA(
    "UPDATE tracking SET status=:status, startTime=:startTime WHERE model=:model and func=:func",
    [{"status": 1, "startTime":datetime.now(), "model": "torqueHis", "func": "initData"}],
    "UPDATE tracking SET status=:status, endTime=:endTime WHERE model=:model and func=:func",
    [{"status": 2, "endTime":datetime.now(), "model": "torqueHis", "func": "initData"}])
@Plcweb.returnName
def initData():
    print("torqueHis initialDataProcess 开始................")
    X = toCsv()
    toAttrCsv(Config.hisInitialData, Config.hisInitialDataAttr)
    print('torqueHis initialDataProcess 结束................')
    # show3dFig(Config.hisInitialData)
    return X
if __name__ == '__main__':

    initData()