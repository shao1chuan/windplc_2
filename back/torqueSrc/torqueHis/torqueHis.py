from toolsSrc.status.status import Status
from torqueSrc.torqueHis.initData import initData
from torqueSrc.torqueHis.cleanProcess import cleanProcess
from torqueSrc.torqueHis.lambdaOptProcess import lambdaOptProcess
from torqueSrc.torqueHis.lambdaOptCompareProcess import lambdaOptCompareProcess
from src.main.sub_server import sub_app
from toolsSrc.util.plcweb import Plcweb

@sub_app.get("/torqueHis")
# @Status.updateBefore('UPDATE tracking SET status=:status WHERE model=:model',[{"status": 0, "model": "torqueHis"}])
@Status.updateBA(
    "UPDATE status SET value=:value WHERE name=:name",
    [{"name": "torque", "value": 3}],
    "UPDATE status SET value=:value WHERE name=:name",
    [{"name": "torque", "value": 1}])

def torqueHis():
    initData()
    cleanProcess()
    lambdaOptProcess()
    lambdaOptCompareProcess()


if __name__ == '__main__':
    torqueHis()
