from toolsSrc.status.status import Status
from torqueSrc.torqueHis.torqueHis import torqueHis
from common.config import torqueConfig as Config
import shutil
import os
from src.main.sub_server import sub_app
from toolsSrc.util.plcweb import Plcweb

'''
针对 data/data/plc/his/torqueHis/new 文件夹中的文件进行的历史优化。
1. 将 data/data/plc/his/torqueHis/new 中的文件拷贝到 .. 文件夹下
2. 重新进行历史优化
'''
@sub_app.get("/torqueNewHis")
# @Status.updateBefore('UPDATE tracking SET status=:status WHERE model=:model',[{"status": 0, "model": "torqueHis"}])
@Status.updateBA(
    "UPDATE status SET value=:value WHERE name=:name",
    [{"name": "torque", "value": 3}],
    "UPDATE status SET value=:value WHERE name=:name",
    [{"name": "torque", "value": 1}])
@Plcweb.returnName
def torqueNewHis():  # 针对 data/data/plc/his/torqueHis/new 文件夹中的文件进行的
    if os.path.exists(Config.historyDataNewRoot):
        for file in os.listdir(Config.historyDataNewRoot):
            shutil.move(os.path.join(Config.historyDataNewRoot, file), Config.historyDataRoot)
    torqueHis()


if __name__ == '__main__':
    torqueNewHis()