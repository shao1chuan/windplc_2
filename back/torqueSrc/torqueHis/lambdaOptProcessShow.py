from torqueSrc.tools.getPlotly import get2dFig, get2dFig, get2dFig_multyLine
from common.config import torqueConfig as Config



if __name__ == '__main__':

    fig = get2dFig_multyLine({'hisLambdaOpt': Config.hisLambdaOptProcess})
    fig.show()
