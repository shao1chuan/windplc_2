from torqueSrc.tools.cleanDecorator import clean_withTemp
from torqueSrc.tools.lambdaOptDecorator import CleanDecorator
from torqueSrc.tools.getPlotly import show3dFig

from common.config import torqueConfig as Config

from torqueSrc.tools.loadFile import fitLineRegressCsv, inverseCurveFitCsv
from torqueSrc.tools.loadFile import toCsv, toLambdaAttrCsv
from toolsSrc.files.loadFiles import loadfileIndepend
from toolsSrc.status.status import Status
from torqueSrc.tools.toolsAboutLambda import lambdaIndividual, maxAveCp_andKey, lambda_dictFilter, aveCpIndividual
from src.main.sub_server import sub_app
from toolsSrc.util.plcweb import Plcweb


# @CleanDecorator.insert0  # 当为空表时，插入一行 0
# @CleanDecorator.drop_dataEffectFlag_False # 每次赛选都有可能得到空表，因此，后面都应该insert0
# @CleanDecorator.insert0  # 当为空表时，插入一行 0
# # @CleanDecorator.drop_duplicates
# # @CleanDecorator.insert0
# @CleanDecorator.drop_bigorsmall
# @CleanDecorator.insert0
# #@CleanDecorator.drop_discretePoint
# # @CleanDecorator.insert0
# def hisCleanProcess(X):
#     return X


from datetime import datetime
def loadfilesToDict():
    X = {}
    columsList = [Config.WindSpeed, Config.ActPower, Config.RotateSpeed, Config.TempAmbient, Config.DataEffectiveFlag]
    loadfileIndepend(Config.historyDataRoot, X, columsList, None)

    return X

def cleanIndividual_withTemp(X):
    # 遍历字典，对value数据点进行clean
    for key in X.keys():
        X[key] = clean_withTemp(X[key])

    # 如果clean后点的数量太少，将其删除
    for key in list(X.keys()):
        if len(X[key]) < Config.validRecordNum_min:
            X.pop(key)

    return X


def toHisLambdaOptProcess(X, maxAveCp, maxAveCpKey):
    # rotateSpeed 切片求 windSpeed平均值
    X[maxAveCpKey][Config.RotateSpeed] = round(X[maxAveCpKey][Config.RotateSpeed] / Config.scale_RotateSpeed,
                                               0) * Config.scale_RotateSpeed  # 将最优文件的转速列保留 scale_RotateSpeed 小数点位数，用于groupby分组
    roWsGroup = X[maxAveCpKey].groupby(Config.RotateSpeed, as_index=False)[Config.WindSpeed]  # 按rotate分组，对windSpeed统计
    roWsGroup = roWsGroup.mean()
    roWsGroup[Config.aveCp] = maxAveCp
    roWsGroup.to_csv(Config.hisLambdaOptProcess)
    # print("roWsGroup: ", roWsGroup)

    # 最小二乘法拟合windSpeed vs. rotateSpeed直线,并且新增一列 aveCp
    inverseCurveFitCsv(Config.hisLambdaOptProcess, Config.hisLambdaOptProcess, maxAveCp)
    toLambdaAttrCsv(Config.hisLambdaOptProcess, Config.hisLambdaOptAttr)

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
@sub_app.get("/torqueHisLambdaOptProcess")
@Status.updateBA(
    "UPDATE tracking SET status=:status, startTime=:startTime WHERE model=:model and func=:func",
    [{"status": 1, "startTime":datetime.now(), "model": "torqueHis", "func": "lambdaOptProcess"}],
    "UPDATE tracking SET status=:status, endTime=:endTime WHERE model=:model and func=:func",
    [{"status": 2, "endTime":datetime.now(), "model": "torqueHis", "func": "lambdaOptProcess"}])
@Plcweb.returnD
def lambdaOptProcess():
    print('torqueHis lanbdaOptProcess 开始..................')
    # 以字典形式读入hisRoot文件夹数据文件{文件名：数据df}
    D = loadfilesToDict()
    # 遍历字典，对value数据点进行clean, 如果clean后点的数量太少，将其删除
    D = cleanIndividual_withTemp(D)

    if D:
        '''
        遍历字典，计算平均风能利用系数 （1）计算风能利用系数平均值（2）重新统计风能利用系数平均值
        '''
        # （1）计算风能利用系数平均值
        lambda_dict = lambdaIndividual(D)
        aveCp_dict = aveCpIndividual(D)

        # lambda_dict, aveCp_dict = lambda_dictFilter(lambda_dict, aveCp_dict)  # 过滤掉离群的 lambda

        # 找到最大平均风能利用系数，返回对应 key
        maxAveCp, maxAveCpKey = maxAveCp_andKey(aveCp_dict)

        toHisLambdaOptProcess(D, maxAveCp, maxAveCpKey)
    print('torqueHis lambdaOptProcess 结束......................')
    return D


if __name__ == '__main__':
    lambdaOptProcess()