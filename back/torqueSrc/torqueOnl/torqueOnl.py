import os

from toolsSrc.dbtools.update_upload import update_upload
from  toolsSrc.status.status import Status
from torqueSrc.torqueOnl.initialData import initialData
from torqueSrc.torqueOnl.cleanProcess import cleanProcess
from torqueSrc.torqueOnl.lambdaProcess import lambdaProcess
from torqueSrc.torqueOnl.lambdaCompareProcess import lambdaCompareProcess
from torqueSrc.torqueOnl.torqCoefAdjProcess import torqCoefAdjProcess
from torqueSrc.tools.toFile import toTorqUploadDB
from datetime import datetime
from toolsSrc.dbtools.sqltools import Sqltools
# from torqueSrc.tools.toFile import toTorqEnableUploadDB
from torqueSrc.tools.toUploadDB import toUploadDBkOptOffsetEnableEnable_True
from src.main.sub_server import sub_app
from toolsSrc.util.plcweb import Plcweb

@sub_app.get("/torqueOnl")
@Status.updateBA(
    "UPDATE status SET value=:value WHERE name=:name",
    [{"name": "torque", "value": 2}],
    "UPDATE status SET value=:value WHERE name=:name",
    [{"name": "torque", "value": 1}])
@Plcweb.returnName
def torqueOnl():
    try:
        initialData()
    except Exception as exp:
        print(f"Exception torqueOnl initialData : [{exp}]")
        Sqltools.excuteSql("UPDATE tracking SET startTime=:startTime ,except1 =:except1 WHERE model=:model and func=:func",
                           [{"startTime": datetime.now(), "model": "torqueOnl", "func": "initialData","except1": str(exp)}])
        return
    try:
        cleanProcess()
    except Exception as exp:
        print(f"Exception torqueOnl cleanProcess : [{exp}]")
        Sqltools.excuteSql("UPDATE tracking SET startTime=:startTime ,except1 =:except1 WHERE model=:model and func=:func",
                           [{"startTime": datetime.now(), "model": "torqueOnl", "func": "cleanProcess","except1": str(exp)}])
        return
    try:
        lambdaProcess()
    except Exception as exp:
        print(f"Exception torqueOnl lambdaProcess : [{exp}]")
        Sqltools.excuteSql("UPDATE tracking SET startTime=:startTime ,except1 =:except1 WHERE model=:model and func=:func",
                           [{"startTime": datetime.now(), "model": "torqueOnl", "func": "lambdaProcess","except1": str(exp)}])
        return

    try:
        torqCoefAdjProcess()
    except Exception as exp:
        print(f"Exception torqueOnl torqCoefAdjProcess : [{exp}]")
        Sqltools.excuteSql("UPDATE tracking SET startTime=:startTime ,except1 =:except1 WHERE model=:model and func=:func",
                           [{"startTime": datetime.now(), "model": "torqueOnl", "func": "torqCoefAdjProcess","except1": str(exp)}])
        return

    try:
        lambdaCompareProcess()
    except Exception as exp:
        print(f"Exception torqueOnl lambdaCompareProcess : [{exp}]")
        Sqltools.excuteSql("UPDATE tracking SET startTime=:startTime ,except1 =:except1 WHERE model=:model and func=:func",
                           [{"startTime": datetime.now(), "model": "torqueOnl", "func": "lambdaCompareProcess","except1": str(exp)}])
        return

    try:
        toTorqUploadDB()
    except Exception as exp:
        print(f"Exception torqueOnl toTorqUploadCsv : [{exp}]")
        Sqltools.excuteSql("UPDATE tracking SET startTime=:startTime ,except1 =:except1 WHERE model=:model and func=:func",
                           [{"startTime": datetime.now(), "model": "torqueOnl", "func": "toTorqUploadCsv","except1": str(exp)}])
        return

    # zp!!!  将upload/offsetEnableUploadCsv 中的 pitchOffsetEnable 字段设置成True, 以便上传给plc，优化有效
    try:
        toUploadDBkOptOffsetEnableEnable_True
    except Exception as exp:
        print(f"Exception toTorqOffsetEnableUploadCsv : [{exp}]")
        return

if __name__ == '__main__':
    torqueOnl()
