from torqueSrc.tools.cleanDecorator import CleanDecorator, clean_withoutTemp
from common.config import torqueConfig as Config
from torqueSrc.tools.loadFile import toCsv, toAttrCsv
from toolsSrc.status.status import Status
from src.main.sub_server import sub_app
from toolsSrc.util.plcweb import Plcweb

# @CleanDecorator.insert0  # 当为空表时，插入一行 0
# @CleanDecorator.drop_dataEffectFlag_False # 每次赛选都有可能得到空表，因此，后面都应该insert0
# @CleanDecorator.insert0  # 当为空表时，插入一行 0
# # @CleanDecorator.drop_duplicates
# # @CleanDecorator.insert0
# @CleanDecorator.drop_bigorsmall
# @CleanDecorator.insert0
# #@CleanDecorator.drop_discretePoint
# # @CleanDecorator.insert0
# def onlCleanProcess(X):
#     return X

from datetime import datetime
@sub_app.get("/torqueOnlCleanProcess")
@Status.updateBA(
    "UPDATE tracking SET status=:status, startTime=:startTime WHERE model=:model and func=:func",
    [{"status": 1, "startTime":datetime.now(), "model": "torqueOnl", "func": "cleanProcess"}],
    "UPDATE tracking SET status=:status, endTime=:endTime WHERE model=:model and func=:func",
    [{"status": 2, "endTime":datetime.now(), "model": "torqueOnl", "func": "cleanProcess"}])
@Plcweb.returnName
def cleanProcess():
    print('torqueOnl cleanProcess 开始................')
    X = toCsv(Config.onlInitialData, Config.onlCleanProcess, clean_withoutTemp)
    toAttrCsv(Config.onlCleanProcess, Config.onlCleanAttr)
    print('torqueOnl cleanProcess 结束................')
    # show3dFig(Config.onlCleanProcess)
    print("++++++++++++++",)
    return X

if __name__ == '__main__':

    cleanProcess()