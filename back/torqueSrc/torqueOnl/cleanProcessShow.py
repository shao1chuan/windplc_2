from torqueSrc.tools.cleanDecorator import CleanDecorator
from torqueSrc.tools.getPlotly import show3dFig

from common.config import torqueConfig as Config

from torqueSrc.tools.loadFile import toCsv, toAttrCsv

import pandas as pd




if __name__ == '__main__':

    show3dFig(Config.onlCleanProcess)