from common.config import torqueConfig as Config
# from torqueSrc.tools.loadFile import loadfile, toAttrCsv
from torqueSrc.tools.loadFile import toAttrCsv
from toolsSrc.files.loadFiles import loadfile
from toolsSrc.status.status import Status
import pandas as pd
from src.main.sub_server import sub_app
from toolsSrc.util.plcweb import Plcweb

# @CleanDecorator.drop_duplicates
# @CleanDecorator.drop_bigorsmall
#@CleanDecorator.drop_dbscan
def hisCleanProcess(X):
    return X


def toCsv():
    columsList = [Config.WindSpeed, Config.ActPower, Config.RotateSpeed, Config.TempAmbient, Config.DataEffectiveFlag]
    # X = loadfile(Config.poolRoot, columsList, None)
    X = loadfile(Config.poolRoot, columsList, Config.torqueOnlHourSpan)
    if X.empty:
        X = pd.DataFrame([[0 for i in range(len(columsList))]], columns=columsList)
    X = X[columsList]
    X.to_csv(Config.onlInitialData, index=False)
    return X

from datetime import datetime
@sub_app.get("/torqueOnlInitialData")
@Status.updateBA(
    "UPDATE tracking SET status=:status, startTime=:startTime WHERE model=:model and func=:func",
    [{"status": 1, "startTime":datetime.now(), "model": "torqueOnl", "func": "initialData"}],
    "UPDATE tracking SET status=:status, endTime=:endTime WHERE model=:model and func=:func",
    [{"status": 2, "endTime":datetime.now(), "model": "torqueOnl", "func": "initialData"}])
@Plcweb.returnName
def initialData():
    print('torqueOnl initialData 开始................')
    X = toCsv()
    toAttrCsv(Config.onlInitialData, Config.onlInitialDataAttr)
    print('torqueOnl initialData 结束................')
    # show3dFig(Config.onlInitialData)
    return X
if __name__ == '__main__':

    initialData()