from torqueSrc.tools.getPlotly import get2dFig, get2dFig, get2dFig_multyLine
from torqueSrc.tools.loadFile import toLineRegressCsv, toAttrCsv, toLineRegressAttrCsv
import pandas as pd
import numpy as np

from common.config import torqueConfig as Config


# def df_read_csv(filename, lineName):
#     X = pd.read_csv(filename)
#     X.columns = [Config.WindSpeed, Config.ActPower, 'lineName']
#     X['lineName'] = lineName
#     return X


if __name__ == '__main__':
    print('onlLambdaProcessShow 进行中................')

    # X = df_read_csv(Config.hisLambdaOptProcess,  'hisLambdaOpt')
    # X = X.append( df_read_csv(Config.onlLambdaProcess,  'onlLambda'))
    #
    #
    # fig = get2dFig_multyLine({'originLmabda':Config.originLambda, 'hisLambdaOpt':Config.hisLambdaOptProcess, 'onlLambda':Config.onlLambdaProcess})

    fig = get2dFig_multyLine({'onlLambdaOpt': Config.onlLambdaProcess})
    fig.show()

    print('onlLambdaProcessShow 结束................')