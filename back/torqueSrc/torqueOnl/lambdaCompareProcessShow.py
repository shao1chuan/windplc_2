from torqueSrc.tools.getPlotly import get2dFig_com

from common.config import torqueConfig as Config



if __name__ == '__main__':
    fig = get2dFig_com(Config.lambdaCompareProcess)
    fig.show()