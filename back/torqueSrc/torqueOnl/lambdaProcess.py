import pandas as pd

from torqueSrc.tools.cleanDecorator import clean_withoutTemp, clean_withTemp
from torqueSrc.tools.lambdaDecorator import LambdaDecorator
from torqueSrc.tools.lambdaOptDecorator import CleanDecorator  # 这里用到了用于历史数据优化的 lambdaOptDecorator ！！！!!!!!
from common.config import torqueConfig as Config
from torqueSrc.tools.loadFile import toCsv, toLineRegressAttrCsv, fitLineRegressCsv, inverseCurveFitCsv
import os
import numpy as np
from torqueSrc.torqueHis.torqueHis import torqueHis
from toolsSrc.files.loadFiles import loadfileIndepend
import shutil
from toolsSrc.status.status import Status
from torqueSrc.tools.toolsAboutLambda import aveCpIndividual
from src.main.sub_server import sub_app
from toolsSrc.util.plcweb import Plcweb

@LambdaDecorator.formulate
def onlLambdaProcess(X):
    return X

# @CleanDecorator.insert0  # 当为空表时，插入一行 0
# @CleanDecorator.drop_dataEffectFlag_False # 每次赛选都有可能得到空表，因此，后面都应该insert0
# @CleanDecorator.insert0  # 当为空表时，插入一行 0
# # @CleanDecorator.drop_duplicates
# # @CleanDecorator.insert0
# @CleanDecorator.drop_bigorsmall
# @CleanDecorator.insert0
# #@CleanDecorator.drop_discretePoint
# # @CleanDecorator.insert0
# def onlCleanProcess(X):
#     return X

from datetime import datetime

# 1.1以字典形式读入hisRoot文件夹数据文件{文件名：数据df}
def loadfilesToDict():
    X = {}
    columsList = [Config.WindSpeed, Config.ActPower, Config.RotateSpeed, Config.TempAmbient, Config.DataEffectiveFlag]
    loadfileIndepend(Config.poolRoot, X, columsList, Config.torqueOnlHourSpan)

    return X

def cleanIndividual_withoutTemp(X):
    # 遍历字典，对value数据点进行clean
    for key in X.keys():
        X[key] = clean_withoutTemp(X[key])
    print("X 3: ", X)

    # 如果clean后点的数量太少，将其删除
    for key in list(X.keys()):
        print("len(X[key]): ", len(X[key]))
        print("Config.validRecordNum_min :", Config.validRecordNum_min)
        if len(X[key]) < Config.validRecordNum_min:
            X.pop(key)
    print("X 4: ", X)

    return X


def maxAveCp_andKey(aveCp):
    # 找到最大平均风能利用系数，返回对应 key
    maxAveCp = 0
    maxAveCpKey = ""
    for key in aveCp.keys():
        if aveCp[key] > maxAveCp:
            maxAveCp = aveCp[key]
            maxAveCpKey = key
    print("     maxAveCp: ", maxAveCp)
    print("     maxAveCpKey: ", maxAveCpKey)
    return maxAveCp, maxAveCpKey

def isMaxAveCpIsGlobal(maxAveCp):
    if os.path.exists(Config.hisLambdaOptProcess):
        df = pd.read_csv(Config.hisLambdaOptProcess)
        if maxAveCp > df[Config.aveCp][0]:
            return True
    return False

def cleanToLambdaProcess(aveCp):
    toCsv(Config.onlCleanProcess, Config.onlLambdaProcess, onlLambdaProcess)
    inverseCurveFitCsv(Config.onlLambdaProcess, Config.onlLambdaProcess,
                       np.array(list(aveCp.values())).mean())  # 曲线拟合
    toLineRegressAttrCsv(Config.onlLambdaProcess, Config.onlLambdaAttr)

@sub_app.get("/torqueOnlLambdaProcess")
@Status.updateBA(
    "UPDATE tracking SET status=:status, startTime=:startTime WHERE model=:model and func=:func",
    [{"status": 1, "startTime":datetime.now(), "model": "torqueOnl", "func": "lambdaProcess"}],
    "UPDATE tracking SET status=:status, endTime=:endTime WHERE model=:model and func=:func",
    [{"status": 2, "endTime":datetime.now(), "model": "torqueOnl", "func": "lambdaProcess"}])
@Plcweb.returnD
def lambdaProcess():
    print('torqueOnl lambdaProcess 开始.................')
    '''
    1.如果onl是多个文件，采用与lambdaOptProcess相同的方法，分别读取每个文件，找到最大平均Cp maxAveCp，
    2.如果maxAveCp > hisLambdaOptProcess.csv中aveCp，将文件拷贝到 "hisData"文件夹中，重新运行 torqueHis\main.py来更新 hisLambdaOptProcess.csv
    3.进行onl优化程序
    注意：这里的1,2 步骤相当于 二次优化 过程.
    '''

    # 以字典形式读入hisRoot文件夹数据文件{文件名：数据df}
    X = loadfilesToDict()
    # 遍历字典，对value数据点进行clean, 如果clean后点的数量太少，将其删除
    X = cleanIndividual_withoutTemp(X)

    # 如果经过数据清理之后一组数都不剩，则什么都不做
    if X:
        # 1.3遍历字典，计算平均风能利用系数
        # 1.3.1 计算法风能利用系数平均值
        aveCp = aveCpIndividual(X)

        # 1.3.2 找到最大平均风能利用系数，返回对应 key
        maxAveCp, maxAveCpKey = maxAveCp_andKey(aveCp)

        # 2.如果maxAveCp > hisLambdaOptProcess.csv中aveCp，将文件拷贝到 "hisDataNew"文件夹中
        if isMaxAveCpIsGlobal(maxAveCp):
            shutil.copy(Config.poolRoot + maxAveCpKey, Config.historyDataNewRoot)
        else:
            # 3. 进行onl优化
            cleanToLambdaProcess(aveCp)

    print('torqueOnl lambdaProcess 结束................')
    # show3dFig(Config.onlGroupProcess)
    return X

# def lambdaProcess():
#     print('torqueOnl lambdaProcess 开始.................')
#     '''
#     1.如果onl是多个文件，采用与lambdaOptProcess相同的方法，分别读取每个文件，找到最大平均Cp maxAveCp，
#     2.如果maxAveCp > hisLambdaOptProcess.csv中aveCp，将文件拷贝到 "hisData"文件夹中，重新运行 torqueHis\main.py来更新 hisLambdaOptProcess.csv
#     3.进行onl优化程序
#     注意：这里的1,2 步骤相当于 二次优化 过程.
#     '''
#     # 1.
#     # 1.1以字典形式读入hisRoot文件夹数据文件{文件名：数据df}
#     X = {}
#     columsList = [Config.WindSpeed, Config.ActPower, Config.RotateSpeed, Config.DataEffectiveFlag]
#     loadfileIndepend(Config.poolRoot, X, columsList, Config.torqueOnlHourSpan)
#     # loadfileIndepend(Config.poolRoot, X, columsList, None)
#
#     # 1.2遍历字典，对value数据点进行clean
#     for key in X.keys():
#         # X[key] = onlCleanProcess(X[key])
#         X[key] = clean(X[key])
#     # print("X1: ", X)
#
#     # 如果clean后点的数量太少，将其删除
#     for key in list(X.keys()):
#         if len(X[key]) < Config.validRecordNum_min: # 1000
#             X.pop(key)
#     # print("X2: ", X)
#
#     # 如果经过数据清理之后一组数都不剩，则什么都不做
#     if X:
#         # 1.3遍历字典，计算平均风能利用系数
#         # 1.3.1 计算法风能利用系数平均值
#         aveCp = {}
#         for key in X.keys():
#             X[key]['Cp'] = X[key][Config.ActPower] / X[key][Config.WindSpeed] ** 3
#             # aveCp[key] = X[key]['Cp'].mean()
#             aveCp[key] = X[key]['Cp'].median()
#         print("     aveCp: ", aveCp)
#
#         # 1.3.2 找到最大平均风能利用系数，返回对应 key
#         maxAveCp = 0
#         maxAveCpKey = ""
#         for key in aveCp.keys():
#             if aveCp[key] > maxAveCp:
#                 maxAveCp = aveCp[key]
#                 maxAveCpKey = key
#         print("     maxAveCp: ", maxAveCp)
#         print("     maxAveCpKey: ", maxAveCpKey)
#
#         # 2.如果maxAveCp > hisLambdaOptProcess.csv中aveCp，将文件拷贝到 "hisData"文件夹中，重新运行 torqueHis\main.py来更新 hisLambdaOptProcess.csv
#         if os.path.exists(Config.hisLambdaOptProcess):
#             df = pd.read_csv(Config.hisLambdaOptProcess)
#             if maxAveCp > df[Config.aveCp][0]:
#                 # os.system(f"copy {Config.poolRoot}/maxAveCpKey {Config.historyDataRoot}".replace('/', '\\') + ' /y')
#                 shutil.copy(Config.poolRoot + maxAveCpKey, Config.historyDataRoot)      #  怎样和下面的同样内容合并
#                 torqueHis()
#                 lambdaProcess()
#             else:
#                 # 3. 进行onl优化
#                 toCsv(Config.onlCleanProcess, Config.onlLambdaProcess, onlLambdaProcess)
#                 inverseCurveFitCsv(Config.onlLambdaProcess, Config.onlLambdaProcess,
#                                    np.array(list(aveCp.values())).mean())  # 曲线拟合
#                 toLineRegressAttrCsv(Config.onlLambdaProcess, Config.onlLambdaAttr)
#         else:
#             # os.system(f"copy {Config.poolRoot}/maxAveCpKey {Config.historyDataRoot}".replace('/', '\\') + ' /y')
#             shutil.copy(Config.poolRoot + maxAveCpKey, Config.historyDataRoot)
#             torqueHis()
#             lambdaProcess()
#
#     print('torqueOnl lambdaProcess 结束................')
#     # show3dFig(Config.onlGroupProcess)

if __name__ == '__main__':
    lambdaProcess()