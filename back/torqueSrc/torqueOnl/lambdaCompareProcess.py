'''
# 当前工作线与 原始工作线 和 当前最优工作线 对比
'''

from torqueSrc.tools.loadFile import toLineRegressCsv, toAttrCsv, toLineRegressAttrCsv
import pandas as pd
import numpy as np
from common.config import torqueConfig as Config
from toolsSrc.files.loadFiles import readLineCsvSetLegend
from toolsSrc.status.status import Status
import os
from toolsSrc.files.checkFiles import checkCsvFiles
from src.main.sub_server import sub_app
from toolsSrc.util.plcweb import Plcweb
'''
对比三条曲线
'''
from datetime import datetime
@sub_app.get("/torqueOnlLambdaCompareProcess")
@Status.updateBA(
    "UPDATE tracking SET status=:status, startTime=:startTime WHERE model=:model and func=:func",
    [{"status": 1, "startTime":datetime.now(), "model": "torqueOnl", "func": "lambdaCompareProcess"}],
    "UPDATE tracking SET status=:status, endTime=:endTime WHERE model=:model and func=:func",
    [{"status": 2, "endTime":datetime.now(), "model": "torqueOnl", "func": "lambdaCompareProcess"}])
@Plcweb.returnName
def lambdaCompareProcess():
    print('torqueOnl lineCompareProcess 开始................')

    if checkCsvFiles([Config.originLambda, Config.hisLambdaOptProcess, Config.onlLambdaProcess]) is False:
        return

    # 1. 三个曲线文件合并成一个文件，并用图例区分
    # adjustRate = pd.read_csv(Config.torqCoefAdjProcess)[Config.AdjustRate][0]
    X = readLineCsvSetLegend(Config.originLambda, '原始工作曲线')
    X = pd.concat([X, readLineCsvSetLegend(Config.hisLambdaOptProcess, '最优工作曲线')])
    # X = pd.concat([X, readLineCsvSetLegend(Config.onlLambdaProcess, '当前工作曲线: 调整率'+str(adjustRate)[:5])])
    X = pd.concat([X, readLineCsvSetLegend(Config.onlLambdaProcess, '当前工作曲线')])
    X = pd.DataFrame(X, columns=[Config.WindSpeed, Config.RotateSpeed, Config.legendColName])
    X.to_csv(Config.lambdaCompareProcess, index=False)

    # 2. 生成属性文件
    toLineRegressAttrCsv(Config.lambdaCompareProcess, Config.lambdaCompareAttr)
    print('torqueOnl lineCompareProcess 结束................')
    return X
if __name__ == '__main__':
    lambdaCompareProcess()








