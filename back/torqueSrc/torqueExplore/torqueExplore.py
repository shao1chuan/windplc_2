
from toolsSrc.status.status import Status
from common.config import torqueConfig as Config
from toolsSrc.files.hisClean import hisClean
# from torqueSrc.tools.toFile import setOnlyTorqEnableUploadTrue, toTorqEnableUploadDB
from torqueSrc.tools.torqExploreDecorator import prepareTorqueExploreData, torqueExploreUpDown
from datetime import datetime
from toolsSrc.dbtools.update_upload import update_upload
from toolsSrc.dbtools.sqltools import Sqltools
from src.main.sub_server import sub_app
from toolsSrc.util.plcweb import Plcweb
'''########################################################################################
1. 准备主动探索数据 df_torqueExplore
2. 主动探索（多次，直到数据全部采集全）
'''
@sub_app.get("/torqueExplore")
@Status.updateBA(
    "UPDATE tracking SET status=:status, startTime=:startTime WHERE model=:model and func=:func",
    [{"status": 1, "startTime":datetime.now(), "model": "torqueExplore", "func": "torqueExplore"}],
    "UPDATE tracking SET status=:status, endTime=:endTime WHERE model=:model and func=:func",
    [{"status": 2, "endTime":datetime.now(), "model": "torqueExplore", "func": "torqueExplore"}])
@Status.updateBA(
    "UPDATE status SET value=:value WHERE name=:name",
    [{"name": "torque", "value": 4}],
    "UPDATE status SET value=:value WHERE name=:name",
    [{"name": "torque", "value": 1}])
@Plcweb.returnName
def torqueExplore(*args):
    try:
        print("torqueExploring......")

        # 主动探索开始，设置只有 torqueUploadCsv 上传有效
        # setOnlyTorqEnableUploadTrue()

        list = [{"windDirOffsetEnable": False, "windDirOffset":0, "pitchOffsetEnable": False, "pitchOffset":0, "kOptOffsetEnable": True}]
        update_upload(list)

        # 1. 准备数据
        df_torqueExplore = prepareTorqueExploreData()
        # print("df_torqueExplore: ", df_torqueExplore)
        # 2. 收集数据
        while len(df_torqueExplore) > 0:
            torqueExploreUpDown(df_torqueExplore)
            df_torqueExplore = prepareTorqueExploreData()
        # 3. 如果数据过多要进行清理
        hisClean(Config.historyDataRoot, Config.hisNumLimit) #??????????????????????????

        # 主动探索结束后，设置所有上传有效标志为 False
        # toTorqEnableUploadDB(False)
        list = [{"kOptOffsetEnable": False, "kOptOffset": 1}]
        update_upload(list)
        print("finish torqueExplore")

    except Exception as exp:
        print(f"Exception pitchExplore : [{exp}]")
        Sqltools.excuteSql("UPDATE tracking SET startTime=:startTime ,except1 =:except1 WHERE model=:model and func=:func",
                           [{"startTime": datetime.now(), "model": "torqueExplore", "func": "torqueExplore","except1": str(exp)}])

if __name__ == '__main__':
    torqueExplore()
    # list = [{"kOptOffsetEnable": False, "kOptOffset": 1}]
    # update_upload(list)
