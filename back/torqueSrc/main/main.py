from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.triggers.interval import IntervalTrigger
from apscheduler.executors.pool import ThreadPoolExecutor
from torqueSrc.torqueOnl.torqueOnl import torqueOnl
from torqueSrc.torqueHis.torqueHis import torqueHis
from torqueSrc.torqueHis.torqueNewHis import torqueNewHis
from common.config import torqueConfig as Config

from torqueSrc.torqueExplore.torqueExplore import torqueExplore

import time

def torqueHisRun(*args):
    torqueHis()

def torqueOnlRun():
    print('torquerun 开始...............')

    # torqueHis()

    # 进行在线优化
    executors = {
        'default': ThreadPoolExecutor(10)
    }
    main = BackgroundScheduler()
    main.add_job(torqueOnl, IntervalTrigger(hours=Config.torqueOnlHourSpan), id='torqueOnl', max_instances=Config.max_instances)

    main.add_job(torqueNewHis, IntervalTrigger(hours=Config.torqueNewHisHourSpan), id='torqueNewHis', max_instances=Config.max_instances)

    main.start()

    # 防止主程序退出
    while True:
        time.sleep(1000)
    print('torquerun 结束...................')

'''#############################################################################################
MAIN
1. 主动探索
2. 历史优化
3. 在线优化（线程池）
'''
if __name__ == "__main__":
    torqueOnlRun()