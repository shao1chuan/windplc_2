
from common.config import torqueConfig as Config
import numpy as np
from pynverse import inversefunc
import pandas as pd



'''
两个lambda同一风速时的转速比值，要求lambda曲线上的点是等间隔的
'''
def ratioOfLambda(X_optLambda, X_curLambda):
    # # 取两条曲线中 windSpeed_min中较大者，windSpeed_max中较小者，用来求这段的rotateSpeed差值平均值
    # windSpeed_min = np.max([X_optLambda[Config.WindSpeed].min(), X_curLambda[Config.WindSpeed].min()])
    # windSpeed_max = np.min([X_optLambda[Config.WindSpeed].max(), X_curLambda[Config.WindSpeed].max()])
    # X_optLambda = X_optLambda[
    #     (X_optLambda[Config.WindSpeed] >= windSpeed_min) & (X_optLambda[Config.WindSpeed] <= windSpeed_max)]
    # X_curLambda = X_curLambda[
    #     (X_curLambda[Config.WindSpeed] >= windSpeed_min) & (X_curLambda[Config.WindSpeed] <= windSpeed_max)]
    # # print("X_optLambda: ", X_optLambda)
    # # print("X_curLambda: ", X_curLambda)

    # 2022.6.8 找到了转矩系数调整与转速变化之间3次方关系，
    opt_rotateSpeed_mean = X_optLambda[Config.RotateSpeed].mean()
    cur_rotateSpeed_mean = X_curLambda[Config.RotateSpeed].mean()

    return cur_rotateSpeed_mean / opt_rotateSpeed_mean



'''
对 rotateSpeed vs. windSpeed 的曲线拟合。
方法为： 曲线拟合之后求其反函数输出。即：将第一列作为x轴，第二列作为y轴，曲线拟合后，求反函数，再输出。
'''
def lambda_dictFit(lambda_dict):
    dict = {}
    for key in lambda_dict.keys():
        f1 = np.polyfit(lambda_dict[key][Config.RotateSpeed], lambda_dict[key][Config.WindSpeed], 1)  # 返回多项式系数
        func = np.poly1d(f1)  # 返回多项式表达式（多项式函数）
        # print("func:  ", func)
        invFunc = inversefunc(func)  # 反函数

        #生成windSpeed等间隔数据点,
        windSpeeds = np.arange(Config.windSpeedFilter_min, Config.windSpeedFilter_max, Config.scale_WindSpeed)
        # 得到windSpeeds 对应的rotateSpeeds
        RotateSpeeds_fit = invFunc(windSpeeds)

        # 过滤掉拟合后 rotateSpeed 不在[Config.RotateSpeedFiler_min, Config.rotateSpeedFilter_max] 范围内的点
        df2 = pd.DataFrame({Config.WindSpeed: windSpeeds, Config.RotateSpeed: RotateSpeeds_fit})
        df2[Config.WindSpeed] = round(df2[Config.WindSpeed], Config.scale_display)
        df2[Config.RotateSpeed] = round(df2[Config.RotateSpeed], Config.scale_display)
        dict[key] = df2
    return dict

'''
X: dict
输出：经过fit之后的lambda字典
'''
def lambdaIndividual(X):
    lambda_dict = {}
    for key in X.keys():
        X[key][Config.RotateSpeed] = round(X[key][Config.RotateSpeed] / Config.scale_RotateSpeed, 0) * Config.scale_RotateSpeed# 将最优文件的转速列保留 scale_RotateSpeed 小数点位数，用于groupby分组
        lambda_dict[key] = X[key].groupby(Config.RotateSpeed, as_index=False)[Config.WindSpeed].mean()# 按rotate分组，对windSpeed统计
    lambda_dict = lambda_dictFit(lambda_dict)

    return lambda_dict


def maxAveCp_andKey(aveCp_dict):
    # 找到最大平均风能利用系数，返回对应 key
    maxAveCp = 0
    maxAveCpKey = ""
    for key in aveCp_dict.keys():
        if aveCp_dict[key] > maxAveCp:
            maxAveCp = aveCp_dict[key]
            maxAveCpKey = key
    print("     maxAveCp: ", maxAveCp)
    print("     maxAveCpKey: ", maxAveCpKey)
    return maxAveCp, maxAveCpKey

'''
删除过于偏离的lambda
'''
def lambda_dictFilter(lambda_dict, aveCp_dict):

    while True:
        maxAveCp, maxAveCpKey = maxAveCp_andKey(aveCp_dict)
        ratio_optVsAllLambda = []
        for key in lambda_dict.keys():
            ratio = ratioOfLambda(lambda_dict[key], lambda_dict[maxAveCpKey])  # 计算最优lambda 与所有 lambda 的比值
            ratio_optVsAllLambda.append(ratio)
        # print("ratio_optVsAllLambda: ", ratio_optVsAllLambda)

        if all(np.array(ratio_optVsAllLambda) >= 1) or all(np.array(ratio_optVsAllLambda) <= 1):  # 如果最优lambda是最大 或 最小 的
            ratioFiltered = list(filter(lambda item: abs(item-1) < Config.lambdaRatioFilter, ratio_optVsAllLambda))  # 多虑掉离 最优lambda 比较远的
            if len(ratioFiltered) < 2:  # 如果就剩下自己一个元素，说明没有离它太近的，应该将该 最优lambda 删除
                lambda_dict.pop(maxAveCpKey, None)
                aveCp_dict.pop(maxAveCpKey, None)
            else:
                break
        else:
            break

    return lambda_dict, aveCp_dict


def aveCpIndividual(X):
    # 计算风能利用系数平均值，折算成25摄氏度空气密度
    aveCp = {}
    for key in X.keys():  # 相当于文件名
        X[key]['Cp'] = X[key][Config.ActPower] / X[key][Config.WindSpeed] ** 3
        # aveCp[key] = X[key]['Cp'].mean()

        if Config.isRhoConvert == True:  # 空气密度折算 开关
            if Config.convertBytemp1_orRho2 == 1:  # 如果TempAmbient项采集的是温度，将功率则算成 0 摄氏度密度情况
                X[key]['Cp'] = X[key]['Cp'] * 273.15 / ( X[key][Config.TempAmbient] + 273.15)
            else:  # 如果TempAmbient项采集的是空气密度
                X[key]['Cp'] = X[key]['Cp'] / X[key][Config.TempAmbient]

        aveCp[key] = X[key]['Cp'].median()
    print("aveCp: ", aveCp)

    return aveCp





