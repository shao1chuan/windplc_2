from toolsSrc.files.checkFiles import checkCsvFiles
from common.config import torqueConfig as Config
import pandas as pd
from toolsSrc.status.status import Status
from toolsSrc.dbtools.getDBData import getDBData1
from toolsSrc.dbtools.sqltools import Sqltools
from src.main.sub_server import sub_app
from toolsSrc.util.plcweb import Plcweb
''' 用于 PLC 通讯 ################################################################### '''

# 对UploadTorqCoef做一定的限定
def Limit_UploadTorqCoef(UploadTorqCoef):
    if UploadTorqCoef > (1+Config.torqCoef_AdjLimit):
        return (1+Config.torqCoef_AdjLimit)
    elif UploadTorqCoef < (1-Config.torqCoef_AdjLimit):
        return (1-Config.torqCoef_AdjLimit)
    return UploadTorqCoef

'''
转矩优化之后，将转矩系数调整值 copy to torqueUpload.csv
'''
from datetime import datetime

def getKOptOffset():
    print('torqueOnl toTorqUploadCsv 开始................')
    if checkCsvFiles([Config.torqCoefAdjProcess]) is False:
        return None

    # 1. 从torqCoefAdjProcess.csv中读转矩系数调整率
    df = pd.read_csv(Config.torqCoefAdjProcess)
    torqueCoefAdj_rate = df[Config.AdjustRate][0]

    # 2. 读dbData1中 当前转矩系数
    dbData1 = getDBData1()
    actualKoptOffset = dbData1[Config.dbData1_actualKoptOffsetCol]
    # print('     ', 'actualKoptOffset ', actualKoptOffset)

    UploadTorqCoef = actualKoptOffset * torqueCoefAdj_rate
    # print('     ', 'torqueCoefAdj_rate ', torqueCoefAdj_rate)

    UploadTorqCoef = Limit_UploadTorqCoef(UploadTorqCoef)
    return UploadTorqCoef

@sub_app.get("/toTorqUploadDB")
@Status.updateBefore('UPDATE tracking SET status=:status WHERE model=:model',[{"status": 0, "model": "torqUploadCsv"}])
@Status.updateBA(
    "UPDATE tracking SET status=:status, startTime=:startTime WHERE model=:model and func=:func",
    [{"status": 1, "startTime":datetime.now(), "model": "torqUploadCsv", "func": "toTorqUploadCsv"}],
    "UPDATE tracking SET status=:status, endTime=:endTime WHERE model=:model and func=:func",
    [{"status": 2, "endTime":datetime.now(), "model": "torqUploadCsv", "func": "toTorqUploadCsv"}])
@Plcweb.returnName
def toTorqUploadDB():
    kOptOffset = getKOptOffset()
    # print('     ', Config.torquePlcKey, kOptOffset)
    if kOptOffset is not None:
        Sqltools.excuteSql("UPDATE upload SET kOptOffset=:kOptOffset", [{"kOptOffset": kOptOffset}])

    print('torqueOnl toTorqUploadCsv 结束......................')



# @Status.updateBA(
#     "UPDATE tracking SET status=:status, startTime=:startTime WHERE model=:model and func=:func",
#     [{"status": 1, "startTime":datetime.now(), "model": "torqueUploadCsv", "func": "toTorqOffsetEnableUploadCsv"}],
#     "UPDATE tracking SET status=:status, endTime=:endTime WHERE model=:model and func=:func",
#     [{"status": 2, "endTime":datetime.now(), "model": "torqueUploadCsv", "func": "toTorqOffsetEnableUploadCsv"}])
# @Status.updateBefore('UPDATE tracking SET status=:status WHERE model=:model',[{"status": 0, "model": "toTorqOffsetEnableUploadCsv"}])
# def toTorqEnableUploadDB(value):
#     Sqltools.excuteSql("UPDATE upload SET kOptOffsetEnable=:kOptOffsetEnable", [{"kOptOffsetEnable": value}])
#
#     if False == value:
#         Sqltools.excuteSql("UPDATE upload SET kOptOffset=:kOptOffset", [{"kOptOffset": 1}])
#
#
#
# @Status.updateBA(
#     "UPDATE tracking SET status=:status, startTime=:startTime WHERE model=:model and func=:func",
#     [{"status": 1, "startTime":datetime.now(), "model": "torqueUploadCsv", "func": "setOnlyTorqOffsetEnableTrue"}],
#     "UPDATE tracking SET status=:status, endTime=:endTime WHERE model=:model and func=:func",
#     [{"status": 2, "endTime":datetime.now(), "model": "torqueUploadCsv", "func": "setOnlyTorqOffsetEnableTrue"}])
# @Status.updateBefore('UPDATE tracking SET status=:status WHERE model=:model',[{"status": 0, "model": "setOnlyTorqOffsetEnableTrue"}])
# def setOnlyTorqEnableUploadTrue():
#     Sqltools.excuteSql(
#         "UPDATE upload SET windDirOffsetEnable=:windDirOffsetEnable, pitchOffsetEnable=:pitchOffsetEnable, kOptOffsetEnable=:kOptOffsetEnable",
#         [{"windDirOffsetEnable": False, "pitchOffsetEnable": False, "kOptOffsetEnable": True}])
#     Sqltools.excuteSql("UPDATE upload SET windDirOffset=:windDirOffset, pitchOffset=:pitchOffset",
#                        [{"windDirOffset": 0, "pitchOffset": 0}])



if __name__ == '__main__':
    # toTorqUploadDB()
    # toTorqEnableUploadDB(False)
    pass