
from common.config import torqueConfig as Config

import numpy as np
import pandas as pd

def fun(x):
    # 找组内 power 最大值
    max = np.max(x[Config.ActPower])
    # print("rotateSpeed, max: ", x[Config.RotateSpeed].mean(), max)

    # 沿着山脊线切 delta_ActPower 厚度的薄层
    # x = x[x[Config.ActPower] > max-Config.delta_ActPower]

    # 返回 windSpeed vs. rotateSpeed 工作线上的一个点坐标
    res = pd.DataFrame([{Config.RotateSpeed: x[Config.RotateSpeed].mean(), Config.WindSpeed: x[Config.WindSpeed].mean()}])

    return res

class LambdaDecorator:

    def formulate(p2):
        def wrapper(X):

            #1 rotate speed 切片
            X[Config.RotateSpeed] = round(X[Config.RotateSpeed]/Config.scale_RotateSpeed, 0) * Config.scale_RotateSpeed

            # 2 排序
            X = X.sort_values([Config.RotateSpeed], ascending=[True])

            # 3 找片内最大功率,得到（rotateSpeed, windSpeed, power)点集的山脊线，并沿着山脊线切 delta_ActPower 厚度的薄层
            X = X.groupby([Config.RotateSpeed], as_index=False).apply(lambda x: fun(x))
            # print("X: ", X)

            # print('GroupDecorator. formulate 进行中................')
            return p2(X)
        return wrapper
