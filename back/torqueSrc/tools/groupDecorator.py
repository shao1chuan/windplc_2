
from common.config import torqueConfig as Config


class GroupDecorator:
    def formulate(p2):
        def wrapper(X):
            # 1 去小数点
            # X[[Config.WindSpeed, Config.RotateSpeed]] = round(X[[Config.WindSpeed, Config.RotateSpeed]], Config.scale_RotateSpeed)
            #X[Config.WindSpeed] = round(X[Config.WindSpeed], Config.scale_WindSpeed)
            #X[Config.RotateSpeed] = round(X[Config.RotateSpeed], Config.scale_RotateSpeed)

            #1 地面划分网格
            X[Config.WindSpeed] = round(X[Config.WindSpeed]/Config.scale_WindSpeed, 0) * Config.scale_WindSpeed
            X[Config.RotateSpeed] = round(X[Config.RotateSpeed]/Config.scale_RotateSpeed, 0) * Config.scale_RotateSpeed

            # 2 排序
            X = X.sort_values([Config.RotateSpeed, Config.WindSpeed], ascending=[True, True])
            # 3聚合
            X = X.groupby([Config.RotateSpeed, Config.WindSpeed], as_index=False)[Config.ActPower]
            X= X.sum() if Config.groupBySum else X.mean()
            #X = X.groupby([Config.RotateSpeed, Config.WindSpeed], as_index=False)[Config.ActPower].mean()
            # print(f"点云数据 ：{X},{X.shape}")
            # h = a - b if a > b else a + b

            print('GroupDecorator. formulate 进行中................')
            return p2(X)
        return wrapper
