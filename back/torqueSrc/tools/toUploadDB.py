from toolsSrc.dbtools.update_upload import update_upload
from src.main.sub_server import sub_app
from toolsSrc.util.plcweb import Plcweb
@sub_app.get("/toUploadDBkOptOffsetEnableEnable_True")
@Plcweb.returnName
def toUploadDBkOptOffsetEnableEnable_True():
    list = [{"kOptOffsetEnable": True}]
    update_upload(list)

@sub_app.get("/toUploadDBkOptOffsetEnableEnable_False")
@Plcweb.returnName
def toUploadDBkOptOffsetEnableEnable_False():
    list = [{"kOptOffsetEnable": False}]
    update_upload(list)