import pandas as pd
from common.config import torqueConfig as Config

import plotly.express as px
import plotly.graph_objects as go
from plotly.subplots import make_subplots
import numpy as np

def fetch3dFig(X, x, y, z, size, color):
    if len(X) > Config.displayMaxPoints:
        X = X.sample(Config.displayMaxPoints)
    X = X.iloc[:, 0:4]
    fig = px.scatter_3d(X, x=x, y=y, z=z, size=size, size_max=Config.size_max, opacity=Config.opacity, color=color)
    fig.update_layout(margin=dict(l=0, r=0, b=0, t=0),height=Config.scatter_3d_height)
    return fig


def show3dFig(fileName):
    X = pd.read_csv(fileName)
    x = Config.WindSpeed
    y = Config.RotateSpeed
    z = Config.ActPower
    size = Config.WindSpeed
    color = Config.ActPower
    fig = fetch3dFig(X, x, y, z, size, color)
    fig.show()


def get3dFig(fileName):
    X = pd.read_csv(fileName)
    x = Config.WindSpeed
    y = Config.RotateSpeed
    z = Config.ActPower
    size = Config.WindSpeed
    color = Config.ActPower
    fig = fetch3dFig(X, x, y, z, size, color)
    return fig


# def get2dFig(fileName):
#     X = pd.read_csv(fileName)
#     points_maxPower = pd.DataFrame(columns=[Config.WindSpeed, Config.RotateSpeed])  # WindSpeed切片->最高点 DataFrame
#     WNAC_WindSpeed = X[Config.WindSpeed].drop_duplicates()
#     for i in WNAC_WindSpeed:
#         if i < 3.5:
#             continue
#         df = X[X[Config.WindSpeed] == i]  # X中，WindSpeed == i 切片的记录
#
#         index_maxPower = df[Config.ActPower].nlargest(n=1).index[0]
#         points_maxPower = points_maxPower.append(df.loc[index_maxPower, :])
#
#     fig = px.line(points_maxPower, x=Config.RotateSpeed, y=Config.WindSpeed, title='偏航误差曲线', range_x=[-Config.misalignment, Config.misalignment])
#     return fig


def get2dSubFig(left, right):
    X = pd.read_csv(left)
    Y = pd.read_csv(right)
    points_maxPower = pd.DataFrame(columns=[Config.WindSpeed, Config.RotateSpeed])  # WindSpeed切片->最高点 DataFrame
    WNAC_WindSpeed = X[Config.WindSpeed].drop_duplicates()
    for i in WNAC_WindSpeed:
        if i < 3.5:
            continue
        df = X[X[Config.WindSpeed] == i]  # X中，WindSpeed == i 切片的记录

        index_maxPower = df[Config.ActPower].nlargest(n=1).index[0]
        points_maxPower = points_maxPower.append(df.loc[index_maxPower, :])

    fig = px.line(points_maxPower, x=Config.RotateSpeed, y=Config.WindSpeed, title='偏航误差曲线',
                  range_x=[-Config.misalignment, Config.misalignment])
    fig = make_subplots(
        rows=2, cols=2,
        specs=[[{'type': 'surface'}, {'type': 'surface'}], [{'type': 'surface'}, {'type': 'surface'}]])
    fig.add_trace(
        px.line(points_maxPower, x=Config.RotateSpeed, y=Config.WindSpeed, title='偏航误差曲线',
                range_x=[-Config.misalignment, Config.misalignment])
        , row=1, col=1)
    fig.add_trace(
        px.line(points_maxPower, x=Config.RotateSpeed, y=Config.WindSpeed, title='偏航误差曲线',
                range_x=[-Config.misalignment, Config.misalignment])
        , row=1, col=2)
    fig.add_trace(
        px.line(points_maxPower, x=Config.RotateSpeed, y=Config.WindSpeed, title='偏航误差曲线',
                range_x=[-Config.misalignment, Config.misalignment])
        , row=2, col=1)
    fig.add_trace(
        px.line(points_maxPower, x=Config.RotateSpeed, y=Config.WindSpeed, title='偏航误差曲线',
                range_x=[-Config.misalignment, Config.misalignment])
        , row=2, col=2)
    return fig


def get3dSurfaceFig(fileName):
    X = pd.read_csv(fileName)
    x, y, z = X[Config.WindSpeed].values.tolist(), X[Config.RotateSpeed].values.tolist(), X[
        Config.ActPower].values.tolist()
    fig = go.Figure(data=[go.Mesh3d(x=x,
                                    y=y,
                                    z=z,
                                    opacity=0.7,
                                    color='rgba(100,22,200,0.9)'
                                    )])
    fig.update_layout(scene=dict(
        xaxis_title=Config.WindSpeed,
        yaxis_title=Config.RotateSpeed,
        zaxis_title=Config.ActPower),
        # width=700,
        margin=dict(l=0, r=0, b=0, t=0),height=Config.scatter_3d_height)
    return fig

def get3dSurfacePlotFig(fileName):
    X = pd.read_csv(fileName)
    z = X.values
    sh_0, sh_1 = z.shape
    x, y = np.linspace(-Config.misalignment, Config.misalignment, sh_0), np.linspace(1, Config.windSpeedFilter_max, sh_1)
    fig = go.Figure(data=[go.Surface(z=z, x=x, y=y)])
    fig.update_traces(contours_z=dict(show=True, usecolormap=True,
                                      highlightcolor="limegreen", project_z=True))
    fig.update_layout(height=Config.scatter_3d_height,
                      margin=dict(l=0, r=0, b=0, t=0))
    return fig

def get3dSubSurfaceFig(left, right):
    X = pd.read_csv(left)
    x, y, z = X[Config.WindSpeed].values.tolist(), X[Config.RotateSpeed].values.tolist(), X[
        Config.ActPower].values.tolist()
    Y = pd.read_csv(right)
    x1, y1, z1 = Y[Config.WindSpeed].values.tolist(), Y[Config.RotateSpeed].values.tolist(), Y[
        Config.ActPower].values.tolist()

    fig = make_subplots(
        rows=1, cols=2,
        specs=[[{'type': 'surface'}, {'type': 'surface'}]])
    fig.add_trace(
        go.Mesh3d(x=x,
                  y=y,
                  z=z,
                  opacity=0.7,
                  color='rgba(100,22,200,0.9)',
                  name='历史数据'
                  ),
        row=1, col=1)
    fig.add_trace(
        go.Mesh3d(x=x1,
                  y=y1,
                  z=z1,
                  opacity=0.7,
                  color='rgba(10,2,1,0.9)',
                  name='更新数据'
                  ),
        row=1, col=2)

    fig.update_layout(scene=dict(
        xaxis_title=Config.WindSpeed,
        yaxis_title=Config.RotateSpeed,
        zaxis_title=Config.ActPower),
        width=700,
        margin=dict(r=0, b=0, l=0, t=0))
    return fig


def get2dFig(fileName):
    X = pd.read_csv(fileName)
    fig = px.line(X, x=Config.RotateSpeed, y=Config.WindSpeed, title='偏航误差曲线',
                  range_x=[Config.rotateSpeedFilter_min, Config.rotateSpeedFilter_max])
    return fig

#  通用的绘制 2d 曲线方法，主要体现在可以画多条曲线，文件格式为 （x列，y列，分类列）
def get2dFig_com(fileName):
    X = pd.read_csv(fileName)
    columns = X.columns
    if len(columns)>2:
        fig = px.line(X, x=columns[0], y=columns[1], color=columns[2], title='',
                  range_x=[Config.windSpeedFilter_min, Config.windSpeedFilter_max])
    else:
        fig = px.line(X, x=columns[0], y=columns[1], title='',
                  range_x=[Config.windSpeedFilter_min, Config.windSpeedFilter_max])
    return fig

#  在一个坐标系中绘制多条 2d 曲线方法，参数形式： {lineName1:file1, lineName2:file2, ......] 其中file文件必须都为2列，且每一列物理量相同，列名以file1列名为准
def get2dFig_multyLine(lines):
    X = pd.DataFrame()
    for key in lines.keys():
        x = pd.read_csv(lines[key])
        x['lineName'] = key
        X = pd.concat([X, x])
    columns = X.columns
    if len(columns)>2:
        fig = px.line(X, x=columns[0], y=columns[1], color=columns[2], title='',
                  # range_x=[-Config.misalignment, Config.misalignment]
                      )
    else:
        fig = px.line(X, x=columns[0], y=columns[1], title='',
                  # range_x=[-Config.misalignment, Config.misalignment]
                      )
    return fig

####################################################
def get2dFig_power(fileName):
    X = pd.read_csv(fileName)
    fig = px.line(X, x=Config.WindSpeed, y=Config.ActPower, title='偏航误差曲线',
                  )
    return fig



if __name__ == '__main__':
    show3dFig(Config.hisCleanProcess)
