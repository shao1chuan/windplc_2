
import pandas as pd
import time
import numpy as np
from common.config import torqueConfig as Config




class FusionDecorator:

    # 在装饰器函数里传入参数
    def fusion(fun1):
        def wrapper(data1, pointsCount1, data2, pointsCount2):

            recoverRatioOfPoints = Config.attenuationRatio * pointsCount1  / data1.shape[0] / (pointsCount2 / data2.shape[0])  # 根据两个数据文件点数定权重，应该把历史曲面中的记录扩充多少比例

            fraction = recoverRatioOfPoints - int(recoverRatioOfPoints)  # 上面扩比例的小数部分

            data_fusion = pd.DataFrame(columns=[Config.WindSpeed, Config.RotateSpeed, Config.ActPower])  # 未来融合后的dataframe

            # 历史数据点扩充比例的整数部分
            for i in range(int(recoverRatioOfPoints)):
                data_fusion = data_fusion.append(data1)

            # 历史数据点扩充比例的小数部分
            rand = np.random.random(data1.shape[0])
            rand = rand < fraction
            data1['isReserve'] = rand
            data_fusion = data_fusion.append(data1[data1['isReserve'] == True].drop(['isReserve'], axis=1))

            # online 数据点
            data_fusion = data_fusion.append(data2)

            return fun1(data_fusion)
        return wrapper

    def fusion2(fun2):
        def wrapper(hisData, onlData):
            hisData.loc[:, Config.ActPower] = hisData.loc[:, Config.ActPower]*Config.fusionHisCoff
            onlData.loc[:, Config.ActPower] = onlData.loc[:, Config.ActPower]*Config.fusionOnlCoff

            data_fusion = hisData
            data_fusion = data_fusion.append(onlData)

            return fun2(data_fusion)
        return wrapper



