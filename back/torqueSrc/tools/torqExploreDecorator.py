
from toolsSrc.status.status import Status
import shutil
import os
import time
import numpy as np
import pandas as pd
from common.config import torqueConfig as Config
from toolsSrc.files.checkFiles import checkCleanCount
from toolsSrc.dbtools.sqltools import Sqltools
from toolsSrc.dbtools.getDBData import sql


'''#########################################################################################
判断 his 文件夹中的数据文件个数是否够用，如果不够，生成需要主动探索数据表 df_torqueExplore
'''
@Status.updateBA(
    "UPDATE status SET value=:value WHERE name=:name",
    [{"name": "torque", "value": 4}],
    "UPDATE status SET value=:value WHERE name=:name",
    [{"name": "torque", "value": 1}])
def prepareTorqueExploreData():
    df_torqueExplore = pd.DataFrame(columns=[Config.torquePlcKey], index=None)
    if not os.path.exists(Config.historyDataRoot): os.mkdir(Config.historyDataRoot) # 如果没有历史文件夹，生成一个
    hisFiles = os.listdir(Config.historyDataRoot)
    # print("hisFiles: ", hisFiles)

    # 如果 his 数据不够，生成 Explore 数据
    if len(hisFiles) < Config.hisFiles_min:
        # adjRatio:转矩系数调整率(0,~)，表示 1*adjRatio; torqCoef_AdjLimit:（-1，~)，表示转矩系数限定在[1-torqCoef_AdjLimit, 1+torqCoef_AdjLimit]范围
        adjRatio_list = [1 + ratio for ratio in
                         np.linspace(-Config.torqCoef_AdjLimit, Config.torqCoef_AdjLimit, Config.hisFiles_min)]
        # print("adjRatio_list: ", adjRatio_list)
        df_torqueExplore = pd.DataFrame({Config.torquePlcKey: adjRatio_list})  # 'KoptOffset' 与PLC字段一致
        # print("df_TorqueExplore: ", df_torqueExplore)

    return df_torqueExplore


'''##########################################################################################
将 df_torqueExplore中第 i 条数据写入torqueUpload.csv(opcUa上传频率比价高，会马上上传）
'''
def torqueExploreUpOne(df_torqueExplore, i):
    torque = df_torqueExplore.iloc[i, 0]  # df_torqueExplore中第一行数据（主动探索的一个转矩系数）
    Sqltools.excuteSql("UPDATE upload SET kOptOffset=:kOptOffset", [{"kOptOffset": torque}])
    print("torqueExploreUpOne torque: ", torque)
    # df_torqueUploadData = pd.DataFrame({Config.torquePlcKey: [torque]})
    # df_torqueUploadData.to_csv(Config.torqueUpload)

'''
根据时间戳，在数据池 pool 中取一个新生成的数据文件，拷贝到 historyDataRoot 文件夹中
这个函数和 pitchExploreDownOne() 是一样的，要不要公用一个？？？？？？？？
'''
def torqueExploreDownOne():
    startTime = time.time()
    if not os.path.exists(Config.historyDataRoot): os.mkdir(Config.historyDataRoot)  # 如果torqueHis文件夹不存在，建立
    while True:
        time.sleep(1)
        files = os.listdir(Config.poolRoot)  # pool中 所有文件名
        times = list(map(lambda fn: os.path.getmtime(Config.poolRoot + '/' + fn), files))  # pool中 所有文件建立时间
        if not times: continue  # 如果pool为空什么也不做
        maxIndex = np.argmax(times)  # 最新文件在列表中的索引
        if times[maxIndex] > startTime:
            sql("SELECT * FROM upload ", [{}])  # 为了防止8小时数据库断线，这里进行了一个无用的操作数据库操作
            file = Config.poolRoot+'/'+files[maxIndex]
            if not os.path.isfile(file) or not file.split(".")[-1] == "csv": continue
            if checkCleanCount(file, Config.validRecordNum_min ) == True:
                # os.system( Config.copyCommand + ' '+ Config.poolRoot + '/' + files[maxIndex] + ' ' + Config.historyDataRoot)
                # shutil.copy(Config.poolRoot+ files[maxIndex], Config.historyDataRoot)
                shutil.move(Config.poolRoot + files[maxIndex], Config.historyDataRoot)
                break
            else:
                os.unlink(Config.poolRoot + files[maxIndex])

'''
1. 分批次上传 df_torqueExplore 中的 torque 数据
2. 每上传一个 torque，采集一个数据文件，保存到history中
'''
def torqueExploreUpDown(df_torqueExplore):
    length = len(df_torqueExplore)
    for i in range(length):
        torqueExploreUpOne(df_torqueExplore,i)  # 将第i几条记录上传
        torqueExploreDownOne()  # 下载一个数据文件

if __name__ == '__main__':
    torqueExploreDownOne()