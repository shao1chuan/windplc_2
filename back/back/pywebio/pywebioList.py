from back.pywebio.yamlConfig import yaml_main,yaml_yaw,yaml_pitch,yaml_torque
from back.pywebio.upload import yaw_upload,pitch_upload,torque_upload,csv3dUpload
from pywebio import *
pywebioList = [
        yaml_main,
        yaml_yaw,
        yaml_pitch,
        yaml_torque,
        yaw_upload,
        pitch_upload,
        torque_upload,
        csv3dUpload
       ]
if __name__ == '__main__':
    # start_server(list, port=8000, debug=False, host='::')
    start_server(pywebioList, port=9999, debug=False,cdn=False)
# https://blog.csdn.net/qq_42761569/article/details/123352838