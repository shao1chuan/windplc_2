from pywebio import *
from pywebio.input import *
from pywebio.output import *
from pywebio.session import *
from pywebio import *
from common.config import dataRoot
def yaw_upload():
    filename = "../" + dataRoot + 'yawData/hisSurfaceProcess.csv'
    content = open(filename, 'rb').read()
    put_button('下载文件', lambda: download('hisSurfaceProcess.csv', content))

    f = input.file_upload("上传文件")
    # Config = open('../../data/yawData/' + f['filename'], 'wb').write(f['content'])
    Config = open(filename, 'wb').write(f['content'])
    if (Config):
        put_text("上传yaw的hisSurfaceProcess文件成功")

def pitch_upload():
    filename = "../" + dataRoot + 'pitchData/hisSurfaceProcess.csv'
    content = open(filename, 'rb').read()
    put_button('下载文件', lambda: download('hisSurfaceProcess.csv', content))

    f = input.file_upload("上传文件")
    Config = open(filename, 'wb').write(f['content'])
    if (Config):
        put_text("上传pitch的hisSurfaceProcess文件成功")

def torque_upload():
    filename = "../" + dataRoot + 'torqueData/hisLambdaOptProcess.csv'
    content = open(filename, 'rb').read()
    put_button('下载文件', lambda: download('hisLambdaOptProcess.csv', content))

    f = input.file_upload("上传文件")
    Config = open(filename, 'wb').write(f['content'])
    if (Config):
        put_text("上传torque的hisLambdaOptProcess文件成功")

def csv3dUpload():
    f = input.file_upload("上传文件",placeholder="选择文件",max_size=16000000)  #16000000 相当于15M
    print(f)
    Config = open("../" + dataRoot + 'csv3dShow/csv3dShow.csv', 'wb').write(f['content'])
    if (Config):
        put_text("上传csv3dShow文件成功")



if __name__ == '__main__':
    start_server(csv3dUpload, port=9999, debug=False)