from src.main.sub_server import sub_app
import hone
from fastapi import  HTTPException
from common.config import commonConfig as Config
from common.config import dataRoot
from typing import Optional
from pydantic import BaseModel
import pandas as pd
class Item(BaseModel):
    path: str
    fileName: str
    # 可以使用None让这个属性变为可选的。

# @sub_app.get("/")
# def test():
#     print("发起fastApi请求------------------------------------",222)
#     return "发起fastApi请求------------------------------------"
optional_arguments = {
        "delimiters": [" ", "_", ","]
    }
Hone = hone.Hone(**optional_arguments)
@sub_app.post("/csv")
async def post_csv(item:Item):
    rootpath = "../"
    print("发起fastpostCsv请求------------------------------------",item.fileName)
    file = rootpath+dataRoot+item.path+'/'+item.fileName + '.csv'
    print("-------------------",file)
    schema = Hone.get_schema(file)  # nested JSON schema for input.csv
    keys = reversed(schema.keys())
    values = reversed(schema.values())
    schema2 = {}
    for key, value in zip(keys, values):
        schema2[key] = value
    schema = schema2
    # print("schema : ", schema)
    columns = []
    for key, value in zip(schema.keys(), schema.values()):
        columns.append({"title": key, "dataIndex": value})
    data = Hone.convert(file, schema=schema)  # final structure, nested according to schema
    return [columns, data]

@sub_app.get("/csv")
async def get_csv():
    schema = Hone.get_schema(Config.status)  # nested JSON schema for input.csv
    keys = reversed(schema.keys())
    values = reversed(schema.values())
    schema2 = {}
    for key, value in zip(keys, values):
        schema2[key] = value
    schema = schema2
    # print("schema : ", schema)
    columns = []
    for key, value in zip(schema.keys(), schema.values()):
        columns.append({"title": key, "dataIndex": value})
    data = Hone.convert(Config.status, schema=schema)  # final structure, nested according to schema
    # print(data, columns)
    return [columns, data]

# fileName: 'onlLineRegress',
# path: 'yawData',
# http://127.0.0.1:9099/windplc/csv/yawData?fileName=onlLineRegress
@sub_app.get("/csv/{path}")
async def get_csv(path: str, fileName: Optional[str] = None):
    rootpath = "../"
    file = f"{rootpath}{dataRoot}{path}/{fileName}.csv"
    try:
        # 读取 CSV 文件到 DataFrame
        df = pd.read_csv(file)
        # 将 DataFrame 转换为 JSON，设置 force_ascii=False 以显示中文字符
        json_data = df.to_json(orient='records', force_ascii=False)
        print("-------------------", file)
        return json_data
    except Exception as e:
        print(e)
        raise HTTPException(status_code=400, detail=str(e))