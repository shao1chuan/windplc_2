import plotly.express as px
from common.config import pitchConfig as Config
import plotly.graph_objects as go
import pandas as pd
from plotly.subplots import make_subplots
from src.main.sub_server import sub_app
from fastapi.responses import HTMLResponse
import os
@sub_app.get("/pitch_onlInitialData",response_class=HTMLResponse)
def pitch_onlInitialData():
    print("Config.onlInitialData: ", Config.onlInitialData)
    X = pd.read_csv(Config.onlInitialData)
    X.dropna(inplace=True)
    X = X if len(X) < Config.displayMaxPoints else X.sample(Config.displayMaxPoints)
    fig = px.scatter_3d(X,
                        x=Config.WindSpeed, y=Config.PitchAngle, z=Config.ActPower,
                        size=Config.WindSpeed,
                        size_max=Config.size_max,
                        opacity=Config.opacity,
                        color=Config.ActPower)
    fig.update_layout(margin=dict(l=0, r=0, b=0, t=0), height=Config.scatter_3d_height)
    return(fig.to_html(include_plotlyjs= True, full_html=False,config={'displayModeBar':False}))

@sub_app.get("/pitch_onlCleanProcess",response_class=HTMLResponse)
def pitch_onlCleanProcess():
    print("Config.onlClearProcess: ", Config.onlCleanProcess)
    X = pd.read_csv(Config.onlCleanProcess)
    X.dropna(inplace=True)
    X = X if len(X) < Config.displayMaxPoints else X.sample(Config.displayMaxPoints)
    fig = px.scatter_3d(X,
                        x=Config.WindSpeed, y=Config.PitchAngle, z=Config.ActPower,
                        size=Config.WindSpeed,
                        size_max=Config.size_max,
                        opacity=Config.opacity,
                        color=Config.ActPower)
    fig.update_layout(margin=dict(l=0, r=0, b=0, t=0), height=Config.scatter_3d_height)
    return (fig.to_html(include_plotlyjs=True, full_html=False, config={'displayModeBar': False}))


@sub_app.get("/pitch_onlGroupProcess",response_class=HTMLResponse)
def pitch_onlGroupProcess():
    X = pd.read_csv(Config.onlGroupProcess)
    X.dropna(inplace=True)
    X = X if len(X) < Config.displayMaxPoints else X.sample(Config.displayMaxPoints)
    fig = px.scatter_3d(X,
                        x=Config.WindSpeed, y=Config.PitchAngle, z=Config.ActPower,
                        size=Config.WindSpeed,
                        size_max=Config.size_max,
                        opacity=Config.opacity,
                        color=Config.ActPower)
    fig.update_layout(margin=dict(l=0, r=0, b=0, t=0), height=Config.scatter_3d_height)
    return(fig.to_html(include_plotlyjs= True, full_html=False,config={'displayModeBar':False}))

@sub_app.get("/pitch_onlSmoothProcess",response_class=HTMLResponse)
def pitch_onlSmoothProcess():
    X = pd.read_csv(Config.onlSmoothProcess)
    X.dropna(inplace=True)
    X = X if len(X) < Config.displayMaxPoints else X.sample(Config.displayMaxPoints)
    fig = px.scatter_3d(X,
                        x=Config.WindSpeed, y=Config.PitchAngle, z=Config.ActPower,
                        size=Config.WindSpeed,
                        size_max=Config.size_max,
                        opacity=Config.opacity,
                        color=Config.ActPower)
    fig.update_layout(margin=dict(l=0, r=0, b=0, t=0), height=Config.scatter_3d_height)
    return(fig.to_html(include_plotlyjs= True, full_html=False,config={'displayModeBar':False}))

@sub_app.get("/pitch_onlSurfaceProcess",response_class=HTMLResponse)
def pitch_onlSurfaceProcess():
    X = pd.read_csv(Config.onlSurfaceProcess)
    X.dropna(inplace=True)
    X = X if len(X) < Config.displayMaxPoints else X.sample(Config.displayMaxPoints)

    fig = px.scatter_3d(X,
                        x=Config.WindSpeed, y=Config.PitchAngle, z=Config.ActPower,
                        size=Config.WindSpeed,
                        size_max=Config.size_max,
                        opacity=Config.opacity,
                        color=Config.ActPower)
    fig.update_layout(margin=dict(l=0, r=0, b=0, t=0), height=Config.scatter_3d_height)
    return(fig.to_html(include_plotlyjs= True, full_html=False,config={'displayModeBar':False}))

@sub_app.get("/pitch_onlSurfaceAxes",response_class=HTMLResponse)
def pitch_onlSurfaceAxes():
    X = pd.read_csv(Config.onlSurfaceProcess)
    X.dropna(inplace=True)
    X = X if len(X) < Config.displayMaxPoints else X.sample(Config.displayMaxPoints)
    x, y, z = X[Config.WindSpeed].values.tolist(), X[Config.PitchAngle].values.tolist(), X[
        Config.ActPower].values.tolist()
    fig = go.Figure(data=[go.Mesh3d(x=x,
                                    y=y,
                                    z=z,
                                    opacity=0.7,
                                    color='rgba(100,22,200,0.9)'
                                    )])
    fig.update_layout(scene=dict(
        xaxis_title=Config.WindSpeed,
        yaxis_title=Config.PitchAngle,
        zaxis_title=Config.ActPower),
        # width=700,
        margin=dict(l=0, r=0, b=0, t=0), height=Config.scatter_3d_height)
    return(fig.to_html(include_plotlyjs= True, full_html=False,config={'displayModeBar':False}))

@sub_app.get("/pitch_onlSubSurfaceAxes",response_class=HTMLResponse)
def pitch_onlSubSurfaceAxes():
    X = pd.read_csv(Config.onlSurfaceProcess)
    X.dropna(inplace=True)
    X = X if len(X) < Config.displayMaxPoints else X.sample(Config.displayMaxPoints)
    x, y, z = X[Config.WindSpeed].values.tolist(), X[Config.PitchAngle].values.tolist(), X[
        Config.ActPower].values.tolist()
    Y = pd.read_csv(Config.onlSurfaceProcess)
    x1, y1, z1 = Y[Config.WindSpeed].values.tolist(), Y[Config.PitchAngle].values.tolist(), Y[
        Config.ActPower].values.tolist()
    fig = make_subplots(
        rows=1, cols=2,
        specs=[[{'type': 'surface'}, {'type': 'surface'}]])
    fig.add_trace(
        go.Mesh3d(x=x,
                  y=y,
                  z=z,
                  opacity=0.7,
                  color='rgba(100,22,200,0.9)',
                  name='历史数据'
                  ),
        row=1, col=1)
    fig.add_trace(
        go.Mesh3d(x=x1,
                  y=y1,
                  z=z1,
                  opacity=0.7,
                  color='rgba(10,2,1,0.9)',
                  name='更新数据'
                  ),
        row=1, col=2)

    fig.update_layout(scene=dict(
        xaxis_title=Config.WindSpeed,
        yaxis_title=Config.PitchAngle,
        zaxis_title=Config.ActPower),
        width=700,
        margin=dict(r=0, b=0, l=0, t=0))
    return(fig.to_html(include_plotlyjs= True, full_html=False,config={'displayModeBar':False}))

@sub_app.get("/pitch_onlLineRegress",response_class=HTMLResponse)
def pitch_onlLineRegress():
    X = pd.read_csv(Config.onlLineRegress)
    X.dropna(inplace=True)
    X = X if len(X) < Config.displayMaxPoints else X.sample(Config.displayMaxPoints)
    fig = px.line(X, x=Config.PitchAngle, y=Config.WindSpeed, title='偏航误差曲线',
                  range_x=[-Config.pitchAngleFilter_max, Config.pitchAngleFilter_max])
    fig.update_layout(title='偏航误差识别', xaxis_title='风向偏差',yaxis_title='风速[m/s]')
    return(fig.to_html(include_plotlyjs= True, full_html=False,config={'displayModeBar':False}))

@sub_app.get("/pitch_lineCompareProcess",response_class=HTMLResponse)
def pitch_lineCompareProcess():
    X = pd.read_csv(Config.lineCompareProcess)
    X.dropna(inplace=True)
    X = X if len(X) < Config.displayMaxPoints else X.sample(Config.displayMaxPoints)
    columns = X.columns
    if len(columns) > 2:
        fig = px.line(X, x=columns[0], y=columns[1], color=columns[2], title='',
                      range_x=[-Config.pitchAngleFilter_max, Config.pitchAngleFilter_max])
    else:
        fig = px.line(X, x=columns[0], y=columns[1], title='',
                      range_x=[-Config.pitchAngleFilter_max, Config.pitchAngleFilter_max])
    return(fig.to_html(include_plotlyjs= True, full_html=False,config={'displayModeBar':False}))

