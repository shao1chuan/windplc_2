import plotly.express as px
from common.config import yawConfig as Config
import plotly.graph_objects as go
import pandas as pd
from src.main.sub_server import sub_app
from fastapi.responses import HTMLResponse
import numpy as np

@sub_app.get("/yaw_onlInitialData",response_class=HTMLResponse)
def yaw_onlInitialData():
    X = pd.read_csv(Config.onlInitialData)
    X.dropna(inplace=True)
    X = X if len(X) < Config.displayMaxPoints else X.sample(Config.displayMaxPoints)
    fig = px.scatter_3d(X,
                        x=Config.WindSpeed, y=Config.WindDirection, z=Config.ActPower,
                        size=Config.WindSpeed,
                        size_max=Config.size_max,
                        opacity=Config.opacity,
                        color=Config.ActPower)
    fig.update_layout(margin=dict(l=0, r=0, b=0, t=0), height=Config.scatter_3d_height)
    return(fig.to_html(include_plotlyjs= True, full_html=False,config={'displayModeBar':False}))

@sub_app.get("/yaw_onlCleanProcess", response_class=HTMLResponse)
def yaw_onlCleanProcess():
    X = pd.read_csv(Config.onlCleanProcess)
    X.dropna(inplace=True)
    X = X if len(X) < Config.displayMaxPoints else X.sample(Config.displayMaxPoints)
    fig = px.scatter_3d(X,
                        x=Config.WindSpeed, y=Config.WindDirection, z=Config.ActPower,
                        size=Config.WindSpeed,
                        size_max=Config.size_max,
                        opacity=Config.opacity,
                        color=Config.ActPower)
    fig.update_layout(margin=dict(l=0, r=0, b=0, t=0), height=Config.scatter_3d_height)
    return(fig.to_html(include_plotlyjs= True, full_html=False,config={'displayModeBar':False}))

@sub_app.get("/yaw_onlGroupProcess",response_class=HTMLResponse)
def yaw_onlGroupProcess():
    X = pd.read_csv(Config.onlGroupProcess)
    X.dropna(inplace=True)
    X = X if len(X) < Config.displayMaxPoints else X.sample(Config.displayMaxPoints)
    fig = px.scatter_3d(X,
                        x=Config.WindSpeed, y=Config.WindDirection, z=Config.ActPower,
                        size=Config.WindSpeed,
                        size_max=Config.size_max,
                        opacity=Config.opacity,
                        color=Config.ActPower)
    fig.update_layout(margin=dict(l=0, r=0, b=0, t=0), height=Config.scatter_3d_height)
    return(fig.to_html(include_plotlyjs= True, full_html=False,config={'displayModeBar':False}))
@sub_app.get("/yaw_onlSmoothProcess",response_class=HTMLResponse)
def yaw_onlSmoothProcess():
    X = pd.read_csv(Config.onlSmoothProcess)
    X.dropna(inplace=True)
    X = X if len(X) < Config.displayMaxPoints else X.sample(Config.displayMaxPoints)
    fig = px.scatter_3d(X,
                        x=Config.WindSpeed, y=Config.WindDirection, z=Config.ActPower,
                        size=Config.WindSpeed,
                        size_max=Config.size_max,
                        opacity=Config.opacity,
                        color=Config.ActPower)
    fig.update_layout(margin=dict(l=0, r=0, b=0, t=0), height=Config.scatter_3d_height)
    return(fig.to_html(include_plotlyjs= True, full_html=False,config={'displayModeBar':False}))

# @sub_app.get("/yaw_onlFusionProcess",response_class=HTMLResponse)
# def yaw_onlFusionProcess():
#     X = pd.read_csv(Config.onlFusionProcess)
#     X = X if len(X) < Config.displayMaxPoints else X.sample(Config.displayMaxPoints)
#     fig = px.scatter_3d(X,
#                         x=Config.WindSpeed, y=Config.WindDirection, z=Config.ActPower,
#                         size=Config.WindSpeed,
#                         size_max=Config.size_max,
#                         opacity=Config.opacity,
#                         color=Config.ActPower)
#     fig.update_layout(margin=dict(l=0, r=0, b=0, t=0), height=Config.scatter_3d_height)
#     return(fig.to_html(include_plotlyjs= True, full_html=False,config={'displayModeBar':False}))

@sub_app.get("/yaw_onlFusionProcess",response_class=HTMLResponse)
def yaw_onlSmoothSumProcess():
    X = pd.read_csv(Config.onlSmoothSumProcess)
    X.dropna(inplace=True)
    X = X if len(X) < Config.displayMaxPoints else X.sample(Config.displayMaxPoints)
    fig = px.scatter_3d(X,
                        x=Config.WindSpeed, y=Config.WindDirection, z=Config.ActPower,
                        size=Config.WindSpeed,
                        size_max=Config.size_max,
                        opacity=Config.opacity,
                        color=Config.ActPower)
    fig.update_layout(margin=dict(l=0, r=0, b=0, t=0), height=Config.scatter_3d_height)
    return(fig.to_html(include_plotlyjs= True, full_html=False,config={'displayModeBar':False}))

@sub_app.get("/yaw_onlSurfaceProcess",response_class=HTMLResponse)
def yaw_onlSurfaceProcess():
    X = pd.read_csv(Config.onlSurfaceProcess)
    X.dropna(inplace=True)
    X = X if len(X) < Config.displayMaxPoints else X.sample(Config.displayMaxPoints)
    fig = px.scatter_3d(X,
                    x=Config.WindSpeed, y=Config.WindDirection, z=Config.ActPower,
                    size=Config.WindSpeed,
                    size_max=Config.size_max,
                    opacity=Config.opacity,
                    color=Config.ActPower)
    fig.update_layout(margin=dict(l=0, r=0, b=0, t=0), height=Config.scatter_3d_height)

    return(fig.to_html(include_plotlyjs= True, full_html=False,config={'displayModeBar':False}))

@sub_app.get("/yaw_onlSurfaceAxes",response_class=HTMLResponse)
def yaw_onlSurfaceAxes():
    X = pd.read_csv(Config.onlSurfaceProcess)
    X.dropna(inplace=True)
    X = X if len(X) < Config.displayMaxPoints else X.sample(Config.displayMaxPoints)
    x, y, z = X[Config.WindSpeed].values.tolist(), X[Config.WindDirection].values.tolist(), X[
        Config.ActPower].values.tolist()
    fig = go.Figure(data=[go.Mesh3d(x=x,
                                    y=y,
                                    z=z,
                                    opacity=0.7,
                                    color='rgba(100,22,200,0.9)'
                                    )])
    fig.update_layout(scene=dict(
        xaxis_title=Config.WindSpeed,
        yaxis_title=Config.WindDirection,
        zaxis_title=Config.ActPower),
        # width=700,
        margin=dict(l=0, r=0, b=0, t=0), height=Config.scatter_3d_height)
    return(fig.to_html(include_plotlyjs= True, full_html=False,config={'displayModeBar':False}))

@sub_app.get("/yaw_onlSurfacePlotProcess",response_class=HTMLResponse)
def yaw_onlSurfacePlotProcess():
    X = pd.read_csv(Config.onlSurfaceProcess)
    X.dropna(inplace=True)
    X = X if len(X) < Config.displayMaxPoints else X.sample(Config.displayMaxPoints)
    z = X.values
    sh_0, sh_1 = z.shape
    x, y = np.linspace(-Config.misalignment, Config.misalignment, sh_0), np.linspace(1, Config.windSpeedFilter_max,
                                                                                     sh_1)
    fig = go.Figure(data=[go.Surface(z=z, x=x, y=y)])
    fig.update_traces(contours_z=dict(show=True, usecolormap=True,
                                      highlightcolor="limegreen", project_z=True))
    fig.update_layout(height=Config.scatter_3d_height,
                      margin=dict(l=0, r=0, b=0, t=0))
    return(fig.to_html(include_plotlyjs= True, full_html=False,config={'displayModeBar':False}))

@sub_app.get("/yaw_onlLineRegress",response_class=HTMLResponse)
def yaw_onlLineRegress():
    X = pd.read_csv(Config.onlLineRegress)
    X.dropna(inplace=True)
    # X = X if len(X) < Config.displayMaxPoints else X.sample(Config.displayMaxPoints)
    fig = px.line(X, x=Config.WindDirection, y=Config.WindSpeed, title='偏航误差曲线',
                  range_x=[-Config.misalignment, Config.misalignment])
    fig.update_layout(title='偏航误差识别', xaxis_title='风向偏差',yaxis_title='风速[m/s]')
    return(fig.to_html(include_plotlyjs= True, full_html=False,config={'displayModeBar':False}))

@sub_app.get("/yaw_lineCompareProcess",response_class=HTMLResponse)
def yaw_lineCompareProcess():
    X = pd.read_csv(Config.lineCompareProcess)
    X.dropna(inplace=True)
    columns = X.columns
    if len(columns) > 2:
        fig = px.line(X, x=columns[0], y=columns[1], color=columns[2], title='',
                      range_x=[-Config.misalignment, Config.misalignment])
    else:
        fig = px.line(X, x=columns[0], y=columns[1], title='',
                      range_x=[-Config.misalignment, Config.misalignment])
    return(fig.to_html(include_plotlyjs= True, full_html=False,config={'displayModeBar':False}))

@sub_app.get("/yaw_powerCompareProcess",response_class=HTMLResponse)
def yaw_powerCompareProcess():
    X = pd.read_csv(Config.powerCompareProcess)
    X.dropna(inplace=True)
    columns = X.columns
    if len(columns) > 2:
        fig = px.line(X, x=columns[0], y=columns[1], color=columns[2], title='',
                      range_x=[0, Config.misalignment])
    else:
        fig = px.line(X, x=columns[0], y=columns[1], title='',
                      range_x=[0, Config.misalignment])
    return(fig.to_html(include_plotlyjs= True, full_html=False,config={'displayModeBar':False}))

