import plotly.express as px
from pywebio.output import *
from common.config import torqueConfig as Config
import pandas as pd
from src.main.sub_server import sub_app
from fastapi.responses import HTMLResponse

@sub_app.get("/torque_hisInitialData",response_class=HTMLResponse)
def torque_hisInitialData():
    # with use_scope("table_scope", clear=True):
    #     put_table(pd.read_csv(Config.hisInitialDataAttr, header=None).values.tolist())
    #     return(tableStyle)

    X = pd.read_csv(Config.hisInitialData)
    X = X if len(X) < Config.displayMaxPoints else X.sample(Config.displayMaxPoints)
    fig = px.scatter_3d(X,
                        x=Config.WindSpeed, y=Config.RotateSpeed, z=Config.ActPower,
                        size=Config.WindSpeed,
                        size_max=Config.size_max,
                        opacity=Config.opacity,
                        color=Config.ActPower)
    fig.update_layout(margin=dict(l=0, r=0, b=0, t=0), height=Config.scatter_3d_height)
    return(fig.to_html(include_plotlyjs= True, full_html=False,config={'displayModeBar':False}))

@sub_app.get("/torque_hisCleanProcess", response_class=HTMLResponse)
def torque_hisCleanProcess():
    # with use_scope("table_scope", clear=True):
    #     put_table(pd.read_csv(Config.hisCleanAttr, header=None).values.tolist())
    #     return(tableStyle)

    X = pd.read_csv(Config.hisCleanProcess)
    X = X if len(X) < Config.displayMaxPoints else X.sample(Config.displayMaxPoints)
    fig = px.scatter_3d(X,
                        x=Config.WindSpeed, y=Config.RotateSpeed, z=Config.ActPower,
                        size=Config.WindSpeed,
                        size_max=Config.size_max,
                        opacity=Config.opacity,
                        color=Config.ActPower)
    fig.update_layout(margin=dict(l=0, r=0, b=0, t=0), height=Config.scatter_3d_height)
    return(fig.to_html(include_plotlyjs= True, full_html=False,config={'displayModeBar':False}))
@sub_app.get("/torque_hisLambdaOptProcess",response_class=HTMLResponse)
def torque_hisLambdaOptProcess():
    # with use_scope("table_scope", clear=True):
    #     put_table(pd.read_csv(Config.hisLambdaOptAttr, header=None).values.tolist())
    #     return(tableStyle)

    lines = {'hisLambdaOpt': Config.hisLambdaOptProcess}
    X = pd.DataFrame()
    for key in lines.keys():
        x = pd.read_csv(lines[key])
        x['lineName'] = key
        X = X.append(x)


    columns = X.columns
    if len(columns) > 2:
        fig = px.line(X, x=columns[0], y=columns[1], color=columns[2], title='',
                      # range_x=[-Config.misalignment, Config.misalignment]
                      )
    else:
        fig = px.line(X, x=columns[0], y=columns[1], title='',
                      # range_x=[-Config.misalignment, Config.misalignment]
                      )
    fig.update_layout(margin=dict(l=0, r=0, b=0, t=0), height=Config.scatter_3d_height)
    return(fig.to_html(include_plotlyjs= True, full_html=False,config={'displayModeBar':False}))
