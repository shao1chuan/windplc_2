import plotly.express as px
from pywebio.output import *
from  common.config import yawConfig as Config
import plotly.graph_objects as go
import pandas as pd
import plotly.offline as pltoff
from plotly.subplots import make_subplots
from pywebio.input import *
from pywebio.output import *
from pywebio.session import *
from pywebio import *
from common.config import dataRoot
from src.main.sub_server import sub_app
from fastapi.responses import HTMLResponse

@sub_app.get("/csv3dShow",response_class=HTMLResponse)
def csv3dShow():
    X = pd.read_csv("../" + dataRoot + 'csv3dShow/csv3dShow.csv')
    X = X if len(X) < Config.displayMaxPoints else X.sample(Config.displayMaxPoints)
    fig = px.scatter_3d(X,
                        x=X.columns.values[0], y=X.columns.values[1], z=Config.ActPower,
                        size=Config.WindSpeed,
                        size_max=Config.size_max,
                        opacity=Config.opacity,
                        color=Config.ActPower)
    fig.update_layout(margin=dict(l=0, r=0, b=0, t=0), height=Config.scatter_3d_height)
    return(fig.to_html(include_plotlyjs= True, full_html=False,config={'displayModeBar':False}))




