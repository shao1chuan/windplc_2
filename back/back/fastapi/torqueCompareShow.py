import plotly.express as px
from common.config import torqueConfig as Config
import pandas as pd
from src.main.sub_server import sub_app
from fastapi.responses import HTMLResponse

@sub_app.get("/torque_lambdaOptCompareProcess",response_class=HTMLResponse)
def torque_lambdaOptCompareProcess():
    X=pd.read_csv(Config.lambdaOptCompareProcess)
    columns = X.columns
    if len(columns) > 2:
        fig = px.line(X, x=columns[0], y=columns[1], color=columns[2], title='',
                      range_x=[Config.windSpeedFilter_min, Config.windSpeedFilter_max])
    else:
        fig = px.line(X, x=columns[0], y=columns[1], title='',
                      range_x=[Config.windSpeedFilter_min, Config.windSpeedFilter_max])
    fig.update_layout(margin=dict(l=0, r=0, b=0, t=0), height=Config.scatter_3d_height)
    return(fig.to_html(include_plotlyjs= True, full_html=False,config={'displayModeBar':False}))
@sub_app.get("/torque_lambdaCompareProcess",response_class=HTMLResponse)
def torque_lambdaCompareProcess():
    X = pd.read_csv(Config.lambdaCompareProcess)
    columns = X.columns
    if len(columns) > 2:
        fig = px.line(X, x=columns[0], y=columns[1], color=columns[2], title='',
                      range_x=[Config.windSpeedFilter_min, Config.windSpeedFilter_max])
    else:
        fig = px.line(X, x=columns[0], y=columns[1], title='',
                      range_x=[Config.windSpeedFilter_min, Config.windSpeedFilter_max])
    fig.update_layout(margin=dict(l=0, r=0, b=0, t=0), height=Config.scatter_3d_height)
    return(fig.to_html(include_plotlyjs= True, full_html=False,config={'displayModeBar':False}))