import uuid
import hashlib
import socket
def get_id():

    id =  socket.gethostname()
    sha1 = hashlib.sha1()
    data = str(id) + 'id' + str(id)
    sha1.update(data.encode('utf-8'))
    sha1_data = sha1.hexdigest()
    # import subprocess
    # sha1_data = subprocess.check_output("lsblk --nodeps -no serial /app/data/share", shell=True)
    # print(sha1_data)
    return sha1_data

def get_mac():
    # id=uuid.UUID(int = uuid.getnode()).hex[-12:]
    id ="id".join([uuid.uuid1().hex[-12:][i: i + 2] for i in range(0, 11, 2)])

    sha1 = hashlib.sha1()
    data = str(id) + 'id' + str(id)
    sha1.update(data.encode('utf-8'))
    sha1_data = sha1.hexdigest()

    return sha1_data
def product_key(id):
    sha1 = hashlib.sha1()
    data = str(id) + 'key' + str(id)
    sha1.update(data.encode('utf-8'))
    sha1_data = sha1.hexdigest()

    return sha1_data

if __name__ == '__main__':
    id = get_id()
    print(id)
    # key =  product_key(id)
    # key2 = product_key(id)
    # print(key,key2)