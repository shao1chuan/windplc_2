from common.config import commonConfig as Config
from appSrc.decryption.tools import product_key, get_id
import os
import time
def checkId():
    if os.path.exists(Config.id_file):
        correct_id = get_id()
        with open(Config.id_file, 'r') as f:
            id = f.read()
        if id == correct_id:
            return True
    return False

    # return True if id == get_id() else False


def checkKey():
    if os.path.exists(Config.key_file):
        id = get_id()
        key = None
        with open(Config.key_file, 'r') as f:
            key = f.read()
        correct_key = product_key(id)
        if key == correct_key:
            return True
    return False

def product_idfile():
    id = get_id()
    with open(Config.id_file, 'w') as f:
        f.write(id)

def decryption(*args):  # PyWebIO application function
    print("等待密钥中。。。。")
    permit = checkKey()
    while not permit:
        print("没有通过，继续等待等待密钥中。。。。")
        product_idfile()
        time.sleep(2)
        permit = checkKey() and checkId()
    print("密钥审核通过了！")

# start_server(main, port=8002, debug=True)
if __name__ == '__main__':
    decryption()
    print("hahaha 我通过了！")
