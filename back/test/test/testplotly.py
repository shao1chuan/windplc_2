from pywebio import *
import plotly.express as px
from pywebio.output import *
from common.config import yawConfig as Config

import plotly
import pandas as pd
import plotly.offline as pltoff


def a1():
    X = pd.read_csv(Config.onlSurfaceProcess)
    X = X if len(X) < Config.displayMaxPoints else X.sample(Config.displayMaxPoints)
    fig = px.scatter_3d(X,
                        x=Config.WindSpeed, y=Config.WindDirection, z=Config.ActPower,
                        size=Config.WindSpeed,
                        size_max=Config.size_max,
                        opacity=Config.opacity,
                        color=Config.ActPower)
    fig.update_layout(margin=dict(l=0, r=0, b=0, t=0), height=Config.scatter_3d_height)
    a1 = fig.to_html(include_plotlyjs="require", full_html=False, config={'displayModeBar': False})
    print(type(a1))
    file = open("a1.txt", "w")
    file.write(a1)
    put_html(a1).send()
def a2():
    X = pd.read_csv(Config.onlSurfaceProcess)
    X = X if len(X) < Config.displayMaxPoints else X.sample(Config.displayMaxPoints)
    fig = px.scatter_3d(X,
                        x=Config.WindSpeed, y=Config.WindDirection, z=Config.ActPower,
                        size=Config.WindSpeed,
                        size_max=Config.size_max,
                        opacity=Config.opacity,
                        color=Config.ActPower)
    fig.update_layout(margin=dict(l=0, r=0, b=0, t=0), height=Config.scatter_3d_height)
    # pltoff.plot(fig,filename="a.html", auto_open=False)
    # fig.write_html(file = "a.html", include_plotlyjs=True, full_html=True,config={'displayModeBar':False})
    aPlot = plotly.offline.plot(fig,
                                config={"displayModeBar": False},
                                show_link=False,
                                include_plotlyjs="require",
                                output_type='div')
    # <script src="https://cdn.plot.ly/plotly-latest.min.js">
    html_string = '''
    <html>
    <head>
   
    </head>
    <body>
      aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
      ''' + aPlot + '''
    </body>
</html>'''
    put_html(html_string).send()

def a3():
    X = pd.read_csv(Config.onlSurfaceProcess)
    X = X if len(X) < Config.displayMaxPoints else X.sample(Config.displayMaxPoints)
    fig = px.scatter_3d(X,
                        x=Config.WindSpeed, y=Config.WindDirection, z=Config.ActPower,
                        size=Config.WindSpeed,
                        size_max=Config.size_max,
                        opacity=Config.opacity,
                        color=Config.ActPower)
    fig.update_layout(margin=dict(l=0, r=0, b=0, t=0), height=Config.scatter_3d_height)
    pltoff.plot(fig,filename= "a3.html", auto_open=False)
    f = open("a3.html", encoding="utf-8")
    html_string = f.read()
    f.close()
    put_html(str(html_string))


if __name__ == '__main__':
    start_server([a1,a2,a3], port=9999, debug=False,auto_open_webbrowser=False,cdn=False)