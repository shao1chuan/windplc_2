
import plotly.express as px
import plotly
import plotly.offline as pltoff
from common.config import yawConfig as Config
from uvicorn import run
import hone
from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware
import pandas as pd
from fastapi.responses import HTMLResponse
app = FastAPI()
app.add_middleware(         # 添加中间件
    CORSMiddleware,         # CORS中间件类
    allow_origins=["*"],  # 允许起源
    allow_credentials=True, # 允许凭据
    allow_methods=["*"],    # 允许方法
    allow_headers=["*"],    # 允许头部
)
optional_arguments = {
        "delimiters": [" ", "_", ","]
    }
Hone = hone.Hone(**optional_arguments)

@app.get("/")
def test():
    print("发起fastApi请求------------------------------------",222)
    return "发起fastApi请求------------------------------------"
@app.get("/a1",response_class=HTMLResponse)
def a1():
    X = pd.read_csv(Config.onlSurfaceProcess)
    X = X if len(X) < Config.displayMaxPoints else X.sample(Config.displayMaxPoints)
    fig = px.scatter_3d(X,
                        x=Config.WindSpeed, y=Config.WindDirection, z=Config.ActPower,
                        size=Config.WindSpeed,
                        size_max=Config.size_max,
                        opacity=Config.opacity,
                        color=Config.ActPower)
    fig.update_layout(margin=dict(l=0, r=0, b=0, t=0), height=Config.scatter_3d_height)
    return fig.to_html(include_plotlyjs= True, full_html=False, config={'displayModeBar': False})
@app.get("/a2",response_class=HTMLResponse)
def a2():
    X = pd.read_csv(Config.onlSurfaceProcess)
    X = X if len(X) < Config.displayMaxPoints else X.sample(Config.displayMaxPoints)
    fig = px.scatter_3d(X,
                        x=Config.WindSpeed, y=Config.WindDirection, z=Config.ActPower,
                        size=Config.WindSpeed,
                        size_max=Config.size_max,
                        opacity=Config.opacity,
                        color=Config.ActPower)
    fig.update_layout(margin=dict(l=0, r=0, b=0, t=0), height=Config.scatter_3d_height)
    # pltoff.plot(fig,filename="a.html", auto_open=False)
    # fig.write_html(file = "a.html", include_plotlyjs=True, full_html=True,config={'displayModeBar':False})
    aPlot = plotly.offline.plot(fig,
                                config={"displayModeBar": False},
                                show_link=False,
                                include_plotlyjs="require",
                                output_type='div')
    # <script src="https://cdn.plot.ly/plotly-latest.min.js">
    html_string = '''
    <html>
    <head>
   
    </head>
    <body>
      aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
      ''' + aPlot + '''
    </body>
</html>'''
    return(html_string)

@app.get("/a3",response_class=HTMLResponse)
def a3():
    X = pd.read_csv(Config.onlSurfaceProcess)
    X = X if len(X) < Config.displayMaxPoints else X.sample(Config.displayMaxPoints)
    fig = px.scatter_3d(X,
                        x=Config.WindSpeed, y=Config.WindDirection, z=Config.ActPower,
                        size=Config.WindSpeed,
                        size_max=Config.size_max,
                        opacity=Config.opacity,
                        color=Config.ActPower)
    fig.update_layout(margin=dict(l=0, r=0, b=0, t=0), height=Config.scatter_3d_height)
    pltoff.plot(fig,filename= "a3.html", auto_open=False)
    f = open("a3.html", encoding="utf-8")
    html_string = f.read()
    f.close()
    return(html_string)


if __name__=="__main__":
    run(app, port=9999)