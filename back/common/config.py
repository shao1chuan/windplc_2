import yaml, os
from easydict import EasyDict

mainConfigFile = os.path.join(os.path.dirname(__file__), '../data/mainConfig.yaml').replace("\\", "/")
mainConfigFile = open(mainConfigFile,"r",encoding="utf_8").read()
mainConfig = EasyDict(yaml.safe_load(mainConfigFile))
if mainConfig.data_test_demo == 0:
    config,dataRoot = mainConfig.config,mainConfig.dataRoot
elif mainConfig.data_test_demo == 1:
    config, dataRoot = mainConfig.configTest, mainConfig.dataRootTest
elif mainConfig.data_test_demo == 2:
    config, dataRoot = mainConfig.configDemo, mainConfig.dataRootDemo

print("--------开始------------", config, dataRoot)
commonConfigFile = os.path.join(os.path.dirname(__file__), config + 'commonConfig.yaml').replace("\\", "/")
yawConfigFile = os.path.join(os.path.dirname(__file__), config + 'yawConfig.yaml').replace("\\", "/")
torqueConfigFile = os.path.join(os.path.dirname(__file__), config + 'torqueConfig.yaml').replace("\\", "/")
pitchConfigFile = os.path.join(os.path.dirname(__file__), config + 'pitchConfig.yaml').replace("\\", "/")

print(commonConfigFile)

commonConfigFile = open(commonConfigFile,"r",encoding="utf_8").read()
yawConfigFile = open(yawConfigFile,"r",encoding="utf_8").read()
torqueConfigFile = open(torqueConfigFile,"r",encoding="utf_8").read()
pitchConfigFile = open(pitchConfigFile,"r",encoding="utf_8").read()



commonConfig = EasyDict(yaml.safe_load(commonConfigFile))
yawConfig = EasyDict(yaml.safe_load("{}{}".format(commonConfigFile,yawConfigFile)))
torqueConfig = EasyDict(yaml.safe_load("{}{}".format(commonConfigFile,torqueConfigFile)))
pitchConfig = EasyDict(yaml.safe_load("{}{}".format(commonConfigFile,pitchConfigFile)))


appConfigFile = os.path.join(os.path.dirname(__file__), config + 'appConfig.yaml').replace("\\", "/")
appConfigFile = open(appConfigFile,"r",encoding="utf_8").read()
appConfig = EasyDict(yaml.safe_load("{}{}".format(commonConfigFile,appConfigFile)))








