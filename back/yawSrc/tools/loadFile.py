import sys, os
import numpy as np
sys.path.append(os.getcwd())
from common.config import yawConfig as Config
import pandas as pd


''' 处理数据 保存数据 ##############################################################################  '''
'''
配合 *process.py 使用的函数处理 （与torque 一样，是否应该提成公共的方法？？？？？？？？？？？？？？）
'''
def toCsv(from1, to2, funtionName):
    X = pd.read_csv(from1)
    X = funtionName(X)
    X = round(X, 2)
    X.to_csv(to2, index=False)
    # print('toCsv 完毕................')
    return X

'''
得到曲面文件 from1 的山脊线
'''
def toLineRegressCsv(from1, to2):
    X = pd.read_csv(from1)
    points_maxPower = pd.DataFrame(columns=[Config.WindSpeed, Config.WindDirection],
                                   dtype=float)  # WindSpeed切片->最高点 DataFrame
    WNAC_WindSpeed = X[Config.WindSpeed].drop_duplicates()
    for i in WNAC_WindSpeed:
        if i < 3.5:
            continue
        df = X[X[Config.WindSpeed] == i]  # X中，WindSpeed == i 切片的记录
        index_maxPower = df[Config.ActPower].nlargest(n=1).index[0]
        # points_maxPower = points_maxPower.append(df.loc[index_maxPower, :])
        s = df.loc[index_maxPower, :]
        s1 = s.to_frame()
        s2 = pd.DataFrame(s1.values.T, columns=s1.index)
        points_maxPower = pd.concat([points_maxPower, s2], ignore_index=True)
    points_maxPower = points_maxPower[[Config.WindSpeed, Config.WindDirection]]
    points_maxPower[Config.WindSpeed] = round(points_maxPower[Config.WindSpeed], Config.scale_display)
    points_maxPower[Config.WindDirection] = round(points_maxPower[Config.WindDirection], Config.scale_display)
#######################################################
    # points_maxPower = points_maxPower.sort_values(by=Config.WindDirection, ascending=False)
    points_maxPower.to_csv(to2, index=False)
    # print('toLineRegressCsv 完毕...............')

'''
对曲线文件 from1 进行最小二乘法拟合
'''

def fitLineRegressCsv(from1, to2):
    X = pd.read_csv(from1)
    f1 = np.polyfit(X[Config.WindSpeed], X[Config.WindDirection], 4)  # 返回多项式系数
    func = np.poly1d(f1)  # 返回多项式表达式（多项式函数）
    windDirection_fit = func(X[Config.WindSpeed])
    # X[Config.WindSpeed]=round(X[Config.WindSpeed], Config.scale_display)
    # windDirection_fit=round(windDirection_fit, Config.scale_display)
    df2 = pd.DataFrame({Config.WindSpeed: X[Config.WindSpeed], Config.WindDirection: windDirection_fit})
    df2[Config.WindSpeed] = round(df2[Config.WindSpeed], Config.scale_display)
    df2[Config.WindDirection] = round(df2[Config.WindDirection], Config.scale_display)
    df2.to_csv(to2, index=False)
    return df2
    # print('fitLineRegressCsv 完毕.................')


''' 生成 Attr 文件 ################################################################################# '''
def toAttrCsv(from1, to2):
    X = pd.read_csv(from1)
    speedMin = 0 if X.empty else round(min(X[Config.WindSpeed]), Config.scale_display)
    speedMax = 0 if X.empty else round(max(X[Config.WindSpeed]), Config.scale_display)
    directionMin = 0 if X.empty else round(min(X[Config.WindDirection]), Config.scale_display)
    directionMax = 0 if X.empty else round(max(X[Config.WindDirection]), Config.scale_display)
    powerMin = 0 if X.empty else round(min(X[Config.ActPower]), Config.scale_display)
    powerMax = 0 if X.empty else round(max(X[Config.ActPower]), Config.scale_display)
    attr = pd.DataFrame(
        {'数据长度': [len(X)], '最小风速': [speedMin], '最大风速': [speedMax], '最小风向': [directionMin], '最大风向': [directionMax],
         '最小功率': [powerMin], '最大功率': [powerMax]})
    attr.to_csv(to2, index=False)
    # print(from1+'----'+to2+'   toAttrCsv 完毕................')

def toLineRegressAttrCsv(from1, to2):
    X = pd.read_csv(from1)
    speedMin = 0 if X.empty else min(X[Config.WindSpeed])
    speedMax = 0 if X.empty else max(X[Config.WindSpeed])
    directionMin = 0 if X.empty else min(X[Config.WindDirection])
    directionMax = 0 if X.empty else max(X[Config.WindDirection])
    attr = pd.DataFrame({'数据长度': [len(X)], '最小风速': [speedMin], '最大风速': [speedMax], '最小风向': [directionMin], '最大风向': [directionMax]})
    attr.to_csv(to2, index=False)
    # print('toAttrCsv 完毕................')

def toPowerCompressAttrCsv(from1, to2):  # 用 toPowerCompareAttrCsv 函数名更合理
    X = pd.read_csv(from1)
    speedMin = 0 if X.empty else min(X[Config.WindSpeed])
    speedMax = 0 if X.empty else max(X[Config.WindSpeed])
    powerMin = 0 if X.empty else min(X[Config.ActPower])
    powerMax = 0 if X.empty else max(X[Config.ActPower])
    # attr = pd.DataFrame({'数据长度': [len(X)], '最小风速': [speedMin], '最大风速': [speedMax]})
    attr = pd.DataFrame(
        {'数据长度': [len(X)], '最小风速': [speedMin], '最大风速': [speedMax], '最小功率': [powerMin], '最大功率': [str(powerMax)[:7]]})
    attr.to_csv(to2, index=False)
    # print('toAttrCsv 完毕................')

def toLineCompareAttrCsv_Yaw(from1, to2):  # 针对偏航的
    X = pd.read_csv(from1)
    speedMin = 0 if X.empty else min(X[Config.WindSpeed])
    speedMax = 0 if X.empty else max(X[Config.WindSpeed])
    directionMin = 0 if X.empty else min(X[Config.WindDirection])
    directionMax = 0 if X.empty else max(X[Config.WindDirection])
    # attr = pd.DataFrame({'数据长度': [len(X)], '最小风速': [speedMin], '最大风速': [speedMax]})
    attr = pd.DataFrame(
        {'数据长度': [len(X)], '最小风速': [speedMin], '最大风速': [speedMax], '最小风向': [directionMin], '最大风向': [directionMax]})
    attr.to_csv(to2, index=False)
    # print('toAttrCsv 完毕................')




