import numpy as np
import pandas as pd
from common.config import yawConfig as Config


class lineRegressDecorator:
    def sigma3filter(fun):
        def wrapper(X):
            xx = pd.DataFrame(columns=X.columns)
            while True:
                print("start sigma3filter len(x): ", len(X))
                if X.empty:
                    break

                if len(X[Config.WindSpeed]) < 5: break  # 曲线点数量过少不处理

                # 将切片进行二次曲线拟合
                f1 = np.polyfit(X[Config.WindSpeed].tolist(), X[Config.WindDirection].tolist(), 4)  # 返回多项式系数

                func = np.poly1d(f1)  # 返回多项式表达式（多项式函数）

                windDirection_fit = func(X[Config.WindSpeed])
                diff = X[Config.WindDirection] - windDirection_fit
                sigma = np.std(diff)

                num0 = len(X)
                X = X[abs(X[Config.WindDirection]-windDirection_fit) <= 3*sigma]
                num1 = len(X)

                if num0-num1<1:
                    break

            return fun(X)
        return wrapper


    '''
    得到曲面文件 from1 的山脊线
    '''
    def toLineRegressCsv(fun):
        def wrapper(X):
            # points_maxPower = pd.DataFrame(columns=[Config.WindSpeed, Config.PitchAngle], dtype=float)  # WindSpeed切片->最高点 DataFrame
            points_maxPower = pd.DataFrame(columns=[Config.WindSpeed, Config.WindDirection])
            WNAC_WindSpeed = X[Config.WindSpeed].drop_duplicates()
            for i in WNAC_WindSpeed:
                if i < 3.5:
                    continue
                df = X[X[Config.WindSpeed] == i]  # X中，WindSpeed == i 切片的记录
                index_maxPower = df[Config.ActPower].nlargest(n=1).index[0]
                s = df.loc[index_maxPower, :]
                s1 = s.to_frame()
                s2 = pd.DataFrame(s1.values.T, columns=s1.index)
                points_maxPower = pd.concat([points_maxPower, s2], ignore_index=True)

            points_maxPower = points_maxPower[[Config.WindSpeed, Config.WindDirection]]
            points_maxPower[Config.WindSpeed] = round(points_maxPower[Config.WindSpeed], Config.scale_display)
            points_maxPower[Config.WindDirection] = round(points_maxPower[Config.WindDirection], Config.scale_display)

            points_maxPower = points_maxPower.sort_values([Config.WindSpeed, Config.WindDirection], ascending=[True, True])
            # print('toLineRegressCsv 完毕...............')
            print("points_maxPower: ", points_maxPower)
            return fun(points_maxPower)
        return wrapper


    '''
    对曲线文件 from1 进行最小二乘法拟合
    '''
    def fitLineRegressCsv(fun):
        def wrapper(X):
            if len(X) > 3: # 至少有4个点才进行曲线拟合
                f1 = np.polyfit(X[Config.WindSpeed], X[Config.WindDirection], 4)  # 返回多项式系数
                func = np.poly1d(f1)  # 返回多项式表达式（多项式函数）
                windDirection_fit = func(X[Config.WindSpeed])
                # X[Config.WindSpeed]=round(X[Config.WindSpeed], Config.scale_display)
                # PitchAngle_fit=round(PitchAngle_fit, Config.scale_display)
                df2 = pd.DataFrame({Config.WindSpeed: X[Config.WindSpeed], Config.WindDirection: windDirection_fit})
                df2[Config.WindSpeed] = round(df2[Config.WindSpeed], Config.scale_display)
                df2[Config.WindDirection] = round(df2[Config.WindDirection], Config.scale_display)
            else:
                df2 = X

            print('fitLineRegressCsv 完毕.................')

            return fun(df2)
        return wrapper

    def insert0(fun1):
        def wrapper(x):
            if x.empty:
                zeroLine = pd.DataFrame([[0.0 for i in range(len(x.columns))]], columns = x.columns)
                x = pd.concat([x, zeroLine],ignore_index=True)
            # print("after insert0 x: ", x)
            return fun1(x)
        return wrapper
