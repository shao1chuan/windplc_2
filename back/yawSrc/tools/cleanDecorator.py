from common.config import yawConfig as Config
import numpy as np
import pandas as pd

class CleanDecorator:
    def drop_false(fun1):
        def wrapper(x):
            x = x[(x[Config.WindSpeed] <= 13) & (x[Config.ActPower] <= 3500)
                  & (x[Config.ActPower] > 0) ]
            return fun1(x)
        return wrapper
    # 在装饰器函数里传入参数
    def drop_duplicates(fun1):
        def wrapper(x):
            #x = x.drop_duplicates()  # 删除重复行
            x = x.fillna(value=0)  # 对缺失值进行填充 ,用实数0填充na
            # print('After drop_duplicates: ', len(x))
            return fun1(x)
        return wrapper

    def drop_bigorsmall(fun1): #过滤不合法数据
        def wrapper(x):
            x = x[(x[Config.WindSpeed] >= Config.windSpeedFilter_min) & (x[Config.WindSpeed] <= Config.windSpeedFilter_max) & (x[Config.ActPower] > Config.minActPower) & (
                        x[Config.WindDirection] <= Config.misalignment) & (x[Config.WindDirection] >= -Config.misalignment)]
            # print('After drop_bigorsmall: x', x)
            return fun1(x)
        return wrapper

    def drop_discretePoint(fun1): # 过滤离散点
        def wrapper(x):
            #将WindSpeed 轴划分小段，每段长度 Config.scale_WindSpeed
            x_result = pd.DataFrame()
            min_windSpeed = min(x[Config.WindSpeed])
            max_windSpeed = max(x[Config.WindSpeed])

            for speed in np.arange(min_windSpeed, max_windSpeed+1e-5, Config.scale_WindSpeed): # 1e-5 为了解决 min_windSpeed = max_windSpeed 的情况
                points_slice = x[(x[Config.WindSpeed]>=speed) & (x[Config.WindSpeed]<speed+Config.scale_WindSpeed)]  # 统计切 speed 轴 切片中的点
                mean = np.mean(points_slice[Config.ActPower])
                sigma = np.std(points_slice[Config.ActPower])  # 计算均值和标准差
                data2 = points_slice[np.fabs(points_slice[Config.ActPower]-mean)<=3*sigma] # 保留偏离均值 < 3*sigma 的点
                x_result = pd.concat([x_result, data2])

            # print('After drop_discretePoint x: ', x_result)
            return fun1(x_result)
        return wrapper

    # def drop_false(fun1):
    #     def wrapper(x):
    #         x = x[x[Config.DataEffectiveFlag] == 'True']
    #         return fun1(x)
    #     return wrapper

    def drop_dataEffectFlag_False(fun1):
        def wrapper(x):
            # print(x.head())
            # x = x[x[Config.DataEffectiveFlag] == True]
            return fun1(x)
        return wrapper

    def insert0(fun1):
        def wrapper(x):
            if x.empty:
                zeroLine = pd.DataFrame([[0.0 for i in range(len(x.columns))]], columns = x.columns)
                x = pd.concat([x, zeroLine],ignore_index=True)
            # print("after insert0 x: ", x)
            return fun1(x)
        return wrapper







