# from sklearn.cluster import DBSCAN
# from sklearn.preprocessing import MinMaxScaler, StandardScaler
import pandas as pd
import time
import numpy as np
from common.config import yawConfig as Config

'''
如果某个格点只有一个值，即 A，B 文件中有一个在该位置为空，该值不变。
如果某个格点有两个值，按照 config.fusionHisCoff 和  Config.fusionOnlCoff 比例进行融合。
'''
def applyFusionOnePoint(x, hisCoff, onlCoff):
    if(len(x)>1):
        # x[Config.ActPower] = (x.iloc[0][Config.ActPower]*Config.fusionHisCoff + x.iloc[1][Config.ActPower]*Config.fusionOnlCoff) / (Config.fusionHisCoff + Config.fusionOnlCoff)
        x[Config.ActPower] = (x.iloc[0][Config.ActPower] * hisCoff + x.iloc[-1][Config.ActPower] * onlCoff) / (hisCoff + onlCoff)
    return x

class FusionDecorator:
    # 在装饰器函数里传入参数
    def fusion2(fun2):
        def wrapper(hisData, onlData, hisCoff, onlCoff):
            # 修改前
            # hisData.loc[:, Config.ActPower] = hisData.loc[:, Config.ActPower]*Config.fusionHisCoff
            # onlData.loc[:, Config.ActPower] = onlData.loc[:, Config.ActPower]*Config.fusionOnlCoff
            # data_fusion = hisData
            # data_fusion = pd.concat([data_fusion, onlData])
            # return fun2(data_fusion)

            # 修改后
            #排序
            hisData = hisData.sort_values([Config.WindDirection, Config.WindSpeed], ascending=[True, True])
            onlData = onlData.sort_values([Config.WindDirection, Config.WindSpeed], ascending=[True, True])
            # groupby
            hisData = hisData.groupby([Config.WindDirection, Config.WindSpeed], as_index=False).mean()
            onlData = onlData.groupby([Config.WindDirection, Config.WindSpeed], as_index=False).mean()
            # his, onl 分别网格化
            hisData[Config.WindSpeed] = round(hisData[Config.WindSpeed] / Config.scale_WindSpeed, 0) * Config.scale_WindSpeed
            hisData[Config.WindDirection] = round(hisData[Config.WindDirection] / Config.scale_WindDirection, 0) * Config.scale_WindDirection
            onlData[Config.WindSpeed] = round(onlData[Config.WindSpeed] / Config.scale_WindSpeed, 0) * Config.scale_WindSpeed
            onlData[Config.WindDirection] = round(onlData[Config.WindDirection] / Config.scale_WindDirection, 0) * Config.scale_WindDirection
            # 融合
            his_onl_fusion = pd.concat([hisData, onlData])
            his_onl_fusion = his_onl_fusion.groupby([Config.WindDirection, Config.WindSpeed],as_index=False).apply(applyFusionOnePoint, hisCoff, onlCoff)

            return fun2(his_onl_fusion)
        return wrapper



