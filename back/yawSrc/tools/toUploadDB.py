from toolsSrc.dbtools.update_upload import update_upload
from src.main.sub_server import sub_app
from toolsSrc.util.plcweb import Plcweb

@sub_app.get("/toUploadDBWindDirEnable_True")
@Plcweb.returnName
def toUploadDBWindDirEnable_True():
    list = [{"windDirOffsetEnable": True}]
    update_upload(list)

@sub_app.get("/toUploadDBWindDirEnable_False")
@Plcweb.returnName
def toUploadDBWindDirEnable_False():
    list = [{"windDirOffsetEnable": False}]
    update_upload(list)