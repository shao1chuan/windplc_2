# -*- coding: utf-8 -*-

import numpy as np
import pandas as pd
from common.config import yawConfig as Config
import time



class SurfaceLineDecorator:
    def pointsFit(fun):
        def wrapper(X):
            X = round(X, 2)

            if X.empty:
                return fun(X)
            # 将WindSpeed 轴划分小段，每段长度 Config.scale_WindSpeed
            min_windSpeed = min(X[Config.WindSpeed])
            max_windSpeed = max(X[Config.WindSpeed])

            df_Points_fit = pd.DataFrame(columns=[Config.WindSpeed, Config.WindDirection, Config.ActPower])
            df_peakPoints = pd.DataFrame(columns=[Config.WindSpeed, Config.WindDirection, Config.ActPower])

            for speed in np.arange(min_windSpeed, max_windSpeed, Config.scale_WindSpeed):

                points_slice = X[(X[Config.WindSpeed] >= speed) & (
                        X[Config.WindSpeed] < speed + Config.scale_WindSpeed)]  # 统计切 speed 轴 切片中的点

                if len(points_slice) < 10: continue  # 切片内点数量过少不处理

                # 将切片进行二次曲线拟合

                f1 = np.polyfit(points_slice[Config.WindDirection].tolist(), points_slice[Config.ActPower].tolist(),
                                2)  # 返回多项式系数
                if f1[0] < 0:  # 只需要开口向下的二次曲线
                    func = np.poly1d(f1)  # 返回多项式表达式（多项式函数）

                    # 计算切片曲面拟合最高点坐标
                    # min_direction = min(points_slice[Config.WindDirection])
                    # max_direction = max(points_slice[Config.WindDirection])
                    directions = np.arange(-Config.misalignment, Config.misalignment, Config.scale_WindDirection)
                    power_fit = func(directions)
                    index = np.argmax(power_fit)
                    peak_direction = directions[index]
                    peak_power = power_fit[index]
                    # print("最高点坐标位置（风速，风向，功率）：", speed, peak_direction, peak_power)

                    # 将最高点加入到df_Points_fit中
                    df = pd.DataFrame({Config.WindSpeed: [speed for i in range(len(directions))], Config.WindDirection: directions, Config.ActPower: power_fit},
                                      columns=[Config.WindSpeed, Config.WindDirection, Config.ActPower])
                    df_peak = pd.DataFrame({Config.WindSpeed: [speed], Config.WindDirection:[peak_direction], Config.ActPower: [0.0]}, columns=[Config.WindSpeed, Config.WindDirection, Config.ActPower])
                    df_Points_fit = pd.concat([df_Points_fit, df], ignore_index=True)
                    df_peakPoints = pd.concat([df_peakPoints, df_peak], ignore_index=True)

                    # 显示拟合曲线，测试用
                    # plt.scatter(directions, power_fit)
                    # plt.show()
            # print("df_Points_fit: ", df_Points_fit)
            df_Points_fit = pd.concat([df_Points_fit, df_peakPoints])
            return fun(df_Points_fit)

        return wrapper

    def insert0(fun1):
        def wrapper(x):
            if x.empty:
                zeroLine = pd.DataFrame([[0.0 for i in range(len(x.columns))]], columns=x.columns)
                x = pd.concat([x, zeroLine], ignore_index=True)
            # print("after insert0 x: ", x)
            return fun1(x)

        return wrapper


