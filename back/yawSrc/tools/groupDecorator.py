
from common.config import yawConfig as Config


class GroupDecorator:
    def formulate(p2):
        def wrapper(X):
            # 1 去小数点
            # X[[Config.WindSpeed, Config.WindDirection]] = round(X[[Config.WindSpeed, Config.WindDirection]], Config.scale_WindDirection)
            #X[Config.WindSpeed] = round(X[Config.WindSpeed], Config.scale_WindSpeed)
            #X[Config.WindDirection] = round(X[Config.WindDirection], Config.scale_WindDirection)

            #1 地面划分网格
            X[Config.WindSpeed] = round(X[Config.WindSpeed]/Config.scale_WindSpeed, 0) * Config.scale_WindSpeed
            X[Config.WindDirection] = round(X[Config.WindDirection]/Config.scale_WindDirection, 0) * Config.scale_WindDirection

            # 2 排序
            X = X.sort_values([Config.WindDirection, Config.WindSpeed], ascending=[True, True])
            # 3聚合
            X = X.groupby([Config.WindDirection, Config.WindSpeed], as_index=False)[Config.ActPower]
            X= X.sum() if Config.groupBySum else X.mean()
            #X = X.groupby([Config.WindDirection, Config.WindSpeed], as_index=False)[Config.ActPower].mean()
            # print(f"点云数据 ：{X},{X.shape}")
            # h = a - b if a > b else a + b

            # print('GroupDecorator. formulate 进行中................')
            return p2(X)
        return wrapper
