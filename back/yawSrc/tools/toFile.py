import pandas as pd
import numpy as np
from toolsSrc.files.checkFiles import checkCsvFiles
from common.config import yawConfig as Config
from toolsSrc.dbtools.getDBData import getDBData1
from toolsSrc.toUploadCsv import changLimit, getTheorUploadRecord
from toolsSrc.status.status import Status
from toolsSrc.dbtools.sqltools import Sqltools

''' CALL BACK #######################################################
1. 从数据库data1表中读取当前风速
2. 在 Config.onlLineRegress文件 中查找风速最接近的 windDirection
3. 将 {‘WindDirOffset’:[windDirection]} 写入Config.yawUpload文件
'''
from datetime import datetime

def getWindDirOffset():
    # 1. 检查文件
    if checkCsvFiles([Config.onlLineRegress]) is False:
        return None

    # 2. 读dbData1中 当前风速、当前偏航角
    dbData1 = getDBData1()
    windSpeed = dbData1[Config.dbData1_windSpeedCol]
    actualWindDirOffset = dbData1[Config.dbData1_actualWindDirOffsetCol]

    # 3.onlLineRegress表中查找与当前风速最接近记录的理论偏航角[ 风速，曲线文件，输出列]
    theorWindDirOffset = getTheorUploadRecord(windSpeed, Config.onlLineRegress, Config.WindDirection)
    if not theorWindDirOffset:  # 如果没找到相应记录，什么都不做
        return None

    # 4. 对偏航角变化速度进行限制（根据通讯协议）
    limit = Config.yawUploadSpan * Config.windDirOffset_limit
    windDirOffset = changLimit(actualWindDirOffset, theorWindDirOffset, limit)

    return windDirOffset

@Status.updateBA(
    "UPDATE tracking SET status=:status, startTime=:startTime WHERE model=:model and func=:func",
    [{"status": 1, "startTime":datetime.now(), "model": "yawUploadCsv", "func": "toYawUploadCsv"}],
    "UPDATE tracking SET status=:status, endTime=:endTime WHERE model=:model and func=:func",
    [{"status": 2, "endTime":datetime.now(), "model": "yawUploadCsv", "func": "toYawUploadCsv"}])
@Status.updateBefore('UPDATE tracking SET status=:status WHERE model=:model',[{"status": 0, "model": "yawUploadCsv"}])
def toYawUploadDB():
    windDirOffset = getWindDirOffset()
    if windDirOffset is not None:
        Sqltools.excuteSql("UPDATE upload SET windDirOffset=:windDirOffset", [{"windDirOffset": windDirOffset}])



@Status.updateBA(
    "UPDATE tracking SET status=:status, startTime=:startTime WHERE model=:model and func=:func",
    [{"status": 1, "startTime":datetime.now(), "model": "yawUploadCsv", "func": "toYawOffsetEnableUploadCsv"}],
    "UPDATE tracking SET status=:status, endTime=:endTime WHERE model=:model and func=:func",
    [{"status": 2, "endTime":datetime.now(), "model": "yawUploadCsv", "func": "toYawOffsetEnableUploadCsv"}])
@Status.updateBefore('UPDATE tracking SET status=:status WHERE model=:model',[{"status": 0, "model": "toYawOffsetEnableUploadCsv"}])
# @resetYawUploadDB
def toYawEnableUploadDB(value):
    Sqltools.excuteSql("UPDATE upload SET windDirOffsetEnable=:windDirOffsetEnable", [{"windDirOffsetEnable": value}])

    if False == value:
        Sqltools.excuteSql("UPDATE upload SET windDirOffset=:windDirOffset", [{"windDirOffset": 0}])



@Status.updateBA(
    "UPDATE tracking SET status=:status, startTime=:startTime WHERE model=:model and func=:func",
    [{"status": 1, "startTime":datetime.now(), "model": "yawUploadCsv", "func": "setOnlyYawOffsetEnableTrue"}],
    "UPDATE tracking SET status=:status, endTime=:endTime WHERE model=:model and func=:func",
    [{"status": 2, "endTime":datetime.now(), "model": "yawUploadCsv", "func": "setOnlyYawOffsetEnableTrue"}])
@Status.updateBefore('UPDATE tracking SET status=:status WHERE model=:model',[{"status": 0, "model": "setOnlyYawOffsetEnableTrue"}])

def setOnlyYawEnableUploadTrue():
    Sqltools.excuteSql("UPDATE upload SET windDirOffsetEnable=:windDirOffsetEnable, pitchOffsetEnable=:pitchOffsetEnable, kOptOffsetEnable=:kOptOffsetEnable", [{"windDirOffsetEnable": True, "pitchOffsetEnable": False, "kOptOffsetEnable": False}])
    Sqltools.excuteSql("UPDATE upload SET pitchOffset=:pitchOffset, kOptOffset=:kOptOffset", [{"pitchOffset": 0, "kOptOffset": 1}])





if __name__ == "__main__":
    # toYawUploadDB()
    # toYawEnableUploadDB(True)
    setOnlyYawEnableUploadTrue()
