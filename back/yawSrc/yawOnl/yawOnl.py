from yawSrc.yawOnl.initialData import initialData
from yawSrc.yawOnl.cleanProcess import cleanProcess
from yawSrc.yawOnl.groupProcess import groupProcess
from yawSrc.yawOnl.smoothProcess import smoothProcess
from yawSrc.tools.getPlotly import  get2dFig
from yawSrc.yawOnl.surfaceProcess import surfaceProcess
from common.config import yawConfig as Config
from yawSrc.yawOnl.lineRegress import lineRegress
from yawSrc.tools.getPlotly import show3dFig
from toolsSrc.status.status import Status
from toolsSrc.dbtools.sqltools import Sqltools
from yawSrc.tools.toFile import toYawEnableUploadDB
from datetime import datetime
from yawSrc.yawOnl.lineRegress import lineRegress
from yawSrc.yawOnl.lineCompareProcess import lineCompareProcess
from yawSrc.yawOnl.powerCompareProcess import powerCompareProcess
from yawSrc.tools.toUploadDB import toUploadDBWindDirEnable_True
from src.main.sub_server import sub_app
from toolsSrc.util.plcweb import Plcweb

@sub_app.get("/yawOnl")
@Status.updateBA(
    "UPDATE status SET value=:value WHERE name=:name",
    [{"name": "yaw", "value": 2}],
    "UPDATE status SET value=:value WHERE name=:name",
    [{"name": "yaw", "value": 1}])

def yawOnl():

    try:
        initialData()
    except Exception as exp:
        print(f"Exception initialData : [{exp}]")
        Sqltools.excuteSql("UPDATE tracking SET startTime=:startTime ,except1 =:except1 WHERE model=:model and func=:func",
                           [{"startTime": datetime.now(), "model": "yawOnl", "func": "initialData","except1": str(exp)}])
        return

    try:
        cleanProcess()
    except Exception as exp:
        print(f"Exception cleanProcess : [{exp}]")
        Sqltools.excuteSql("UPDATE tracking SET startTime=:startTime ,except1 =:except1 WHERE model=:model and func=:func",
                           [{"startTime": datetime.now(), "model": "yawOnl", "func": "cleanProcess","except1": str(exp)}])
        return


    try:
        groupProcess()
    except Exception as exp:
        print(f"Exception groupProcess : [{exp}]")
        Sqltools.excuteSql("UPDATE tracking SET startTime=:startTime ,except1 =:except1 WHERE model=:model and func=:func",
                           [{"startTime": datetime.now(), "model": "yawOnl", "func": "groupProcess","except1": str(exp)}])
        return

    try:
        smoothProcess()
    except Exception as exp:
        print(f"Exception smoothProcess : [{exp}]")
        Sqltools.excuteSql("UPDATE tracking SET startTime=:startTime ,except1 =:except1 WHERE model=:model and func=:func",
                           [{"startTime": datetime.now(), "model": "yawOnl", "func": "smoothProcess","except1": str(exp)}])
        return
    try:
        surfaceProcess()
    except Exception as exp:
        print(f"Exception surfaceProcess : [{exp}]")
        Sqltools.excuteSql("UPDATE tracking SET startTime=:startTime ,except1 =:except1 WHERE model=:model and func=:func",
                           [{"startTime": datetime.now(), "model": "yawOnl", "func": "surfaceProcess","except1": str(exp)}])
        return
    try:
        lineRegress()
    except Exception as exp:
        print(f"Exception lineRegress : [{exp}]")
        Sqltools.excuteSql("UPDATE tracking SET startTime=:startTime ,except1 =:except1 WHERE model=:model and func=:func",
                           [{"startTime": datetime.now(), "model": "yawOnl", "func": "lineRegress","except1": str(exp)}])
        return

    try:
        lineCompareProcess()
    except Exception as exp:
        print(f"Exception lineCompareProcess : [{exp}]")
        Sqltools.excuteSql("UPDATE tracking SET startTime=:startTime ,except1 =:except1 WHERE model=:model and func=:func",
                           [{"startTime": datetime.now(), "model": "yawOnl", "func": "lineCompareProcess","except1": str(exp)}])
        return

    try:
        powerCompareProcess()
    except Exception as exp:
        print(f"Exception powerCompareProcess : [{exp}]")
        Sqltools.excuteSql("UPDATE tracking SET startTime=:startTime ,except1 =:except1 WHERE model=:model and func=:func",
                           [{"startTime": datetime.now(), "model": "yawOnl", "func": "powerCompareProcess","except1": str(exp)}])
        return

    # zp!!!  将upload/offsetEnableUploadCsv 中的 yawOffsetEnable 字段设置成True, 以便上传给plc，优化有效
    try:
        toUploadDBWindDirEnable_True()
    except Exception as exp:
        print(f"Exception toYawOffsetEnableUploadCsv : [{exp}]")
        return
    return "yawOnl finish"

if __name__ == '__main__':
    yawOnl()
    show3dFig(Config.onlSmoothProcess)
    show3dFig(Config.onlSurfaceProcess)
    get2dFig(Config.onlLineRegress).show()
