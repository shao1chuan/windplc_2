import pandas as pd
from yawSrc.tools.groupDecorator import GroupDecorator
from common.config import yawConfig as Config

from yawSrc.tools.getPlotly import show3dFig
from yawSrc.tools.loadFile import toCsv,toAttrCsv
from toolsSrc.status.status import Status
from src.main.sub_server import sub_app
from toolsSrc.util.plcweb import Plcweb
from datetime import datetime
@GroupDecorator.formulate
def onlGroupProcess(X):
    return X


@sub_app.get("/groupProcess")
@Status.updateBA(
    "UPDATE tracking SET status=:status, startTime=:startTime WHERE model=:model and func=:func",
    [{"status": 1, "startTime":datetime.now(), "model": "yawOnl", "func": "groupProcess"}],
    "UPDATE tracking SET status=:status, endTime=:endTime WHERE model=:model and func=:func",
    [{"status": 2, "endTime":datetime.now(), "model": "yawOnl", "func": "groupProcess"}])
@Plcweb.returnName
def groupProcess():
    print('yawonl groupProcess 开始................')
    X = toCsv(Config.onlCleanProcess, Config.onlGroupProcess, onlGroupProcess)
    toAttrCsv(Config.onlGroupProcess, Config.onlGroupAttr)
    print('yawonl groupProcess 结束................')
    # show3dFig(Config.onlGroupProcess)
    return X

if __name__ == '__main__':
    groupProcess()