from yawSrc.tools.loadFile import  toPowerCompressAttrCsv
from toolsSrc.files.checkFiles import checkCsvFiles
import pandas as pd
import numpy as np

from common.config import yawConfig as Config
from toolsSrc.status.status import Status
from src.main.sub_server import sub_app
from toolsSrc.util.plcweb import Plcweb
'''
求x的平均功率
'''
def funMean(x):
    return np.mean(x[Config.ActPower])

'''
求x的最大功率
'''
def funMax(x):
    return max(x[Config.ActPower])

'''
对filename文件每个风速片的求 平均（最大）功率，返回 [ 风速， 功率， 图标] 列表
'''
def df_read_csv(filename, fun, legend):
    if checkCsvFiles([filename]) is False:
        return pd.DataFrame(columns=[Config.WindSpeed, Config.ActPower, Config.legendColName])

    X = pd.read_csv(filename)
    X[Config.WindSpeed ] = round(X[Config.WindSpeed], 1)  # 防止由于Config.scale_WindSpeed设置很小，得到曲线会抖动
    X = X.groupby(Config.WindSpeed, as_index=False).apply(lambda x: fun(x))
    X = pd.DataFrame(X)
    X[Config.legendColName] = legend
    X.columns = [Config.WindSpeed, Config.ActPower, Config.legendColName]
    return X

'''
from: 对hisSurfaceProcess.csv 和 onlSurfaceProcess.csv 
to: powerCompareProcess.csv 
功能：得到历史和实际两条 [风速，功率，图例] 曲线
'''
from datetime import datetime
@sub_app.get("/powerCompareProcess")
@Status.updateBA(
    "UPDATE tracking SET status=:status, startTime=:startTime WHERE model=:model and func=:func",
    [{"status": 1, "startTime":datetime.now(), "model": "yawOnl", "func": "powerCompareProcess"}],
    "UPDATE tracking SET status=:status, endTime=:endTime WHERE model=:model and func=:func",
    [{"status": 2, "endTime":datetime.now(), "model": "yawOnl", "func": "powerCompareProcess"}])
@Plcweb.returnName
def powerCompareProcess():
    print('yawOnl powerCompareProcess 开始................')
    
    # 如果 powerCompareProcess.csv 中存在Config.hisPowerLegend（数量>1), hisPower曲线从 powerCompareProcess.csv中,否则从hisSurfaceProcess.csv中读
    df = pd.read_csv(Config.powerCompareProcess)
    X = df[df[Config.legendColName] == Config.hisPowerLegend]
    if len(X) < 3:
        X = df_read_csv(Config.hisSurfaceProcess, funMean, Config.hisPowerLegend)

    X = pd.concat([X, df_read_csv(Config.onlSurfaceProcess, funMean, Config.onlPowerLegend)])
    # X = df_read_csv(Config.hisCleanProcess, funMean, Config.hisPowerLegend)
    # X = pd.concat([X, df_read_csv(Config.onlCleanProcess, funMean, Config.onlPowerLegend)])
    X = pd.DataFrame(X, columns=[Config.WindSpeed, Config.ActPower, Config.legendColName])
    X.to_csv(Config.powerCompareProcess, index=False)

    toPowerCompressAttrCsv(Config.powerCompareProcess, Config.powerCompareAttr)
    print('yawOnl powerCompareProcess 结束................')
    return X

if __name__ == '__main__':
    powerCompareProcess()