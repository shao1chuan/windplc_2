from yawSrc.tools.getPlotly import get2dFig, get2dFig
from yawSrc.tools.loadFile import toLineRegressCsv, toAttrCsv, toLineRegressAttrCsv

from common.config import yawConfig as Config



if __name__ == '__main__':

    fig = get2dFig(Config.onlLineRegress)
    fig.show()

