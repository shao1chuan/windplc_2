from yawSrc.tools.smoothDecorator import SmoothDecorator
from common.config import yawConfig as Config
from toolsSrc.decorator.toCsv import ToCsvDecorator
from yawSrc.tools.getPlotly import show3dFig
from yawSrc.tools.loadFile import toCsv,toAttrCsv
import pandas as pd
import time
from datetime import datetime
from toolsSrc.status.status import Status
from src.main.sub_server import sub_app
from toolsSrc.util.plcweb import Plcweb

@SmoothDecorator.smooth
@SmoothDecorator.sigma3filter
@SmoothDecorator.insert0
@ToCsvDecorator(Config.smoothPool)
def onlSmoothProcess(X):
    return X

@sub_app.get("/smoothProcess")
@Status.updateBA(
    "UPDATE tracking SET status=:status, startTime=:startTime WHERE model=:model and func=:func",
    [{"status": 1, "startTime":datetime.now(), "model": "yawOnl", "func": "smoothProcess"}],
    "UPDATE tracking SET status=:status, endTime=:endTime WHERE model=:model and func=:func",
    [{"status": 2, "endTime":datetime.now(), "model": "yawOnl", "func": "smoothProcess"}])
@Plcweb.returnName
def smoothProcess():
    print('yawonl smoothProcess 开始................')
    X = toCsv(Config.onlGroupProcess, Config.onlSmoothProcess, onlSmoothProcess)
    toAttrCsv(Config.onlSmoothProcess, Config.onlSmoothAttr)
    print('yawonl smoothProcess 结束................')
    # show3dFig(Config.onlSmoothProcess)
    return X

if __name__ == '__main__':
    smoothProcess()
