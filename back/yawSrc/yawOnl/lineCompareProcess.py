from yawSrc.tools.getPlotly import get2dFig, get2dFig, get2dFig_com
from yawSrc.tools.loadFile import toLineRegressCsv, toAttrCsv, toLineRegressAttrCsv, toLineCompareAttrCsv_Yaw
import pandas as pd
from common.config import yawConfig as Config
from toolsSrc.files.loadFiles import readLineCsvSetLegend
from toolsSrc.status.status import Status
from src.main.sub_server import sub_app
from toolsSrc.util.plcweb import Plcweb
'''
对比两条曲线
'''
from datetime import datetime
@sub_app.get("/lineCompareProcess")
@Status.updateBA(
    "UPDATE tracking SET status=:status, startTime=:startTime WHERE model=:model and func=:func",
    [{"status": 1, "startTime":datetime.now(), "model": "yawOnl", "func": "lineCompareProcess"}],
    "UPDATE tracking SET status=:status, endTime=:endTime WHERE model=:model and func=:func",
    [{"status": 2, "endTime":datetime.now(), "model": "yawOnl", "func": "lineCompareProcess"}])
@Plcweb.returnName
def lineCompareProcess():
    print('yawOnl lineCompareProcess 开始................')
    # 1. 两个曲线文件合并成一个文件，并用图例区分
    X = readLineCsvSetLegend(Config.hisLineRegress, '历史曲线')
    X = pd.concat([X, readLineCsvSetLegend(Config.onlLineRegress, '在线优化曲线')])
    X = pd.DataFrame(X, columns=[Config.WindDirection, Config.WindSpeed, Config.legendColName])
    X.to_csv(Config.lineCompareProcess, index=False)

    # 2. 生成属性文件
    toLineCompareAttrCsv_Yaw(Config.lineCompareProcess, Config.lineCompareAttr)
    print('yawOnl lineCompareProcess 结束................')
    return X

if __name__ == '__main__':
    lineCompareProcess()








