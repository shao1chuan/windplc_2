from yawSrc.tools.cleanDecorator import CleanDecorator
from yawSrc.tools.getPlotly import show3dFig

from common.config import yawConfig as Config

from yawSrc.tools.loadFile import toAttrCsv
from toolsSrc.files.loadFiles import loadfile


if __name__ == '__main__':

    show3dFig(Config.onlInitialData)