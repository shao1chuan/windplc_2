import pandas as pd
from yawSrc.tools.groupDecorator import GroupDecorator
from common.config import yawConfig as Config

from yawSrc.tools.getPlotly import show3dFig
from yawSrc.tools.loadFile import toCsv,toAttrCsv

if __name__ == '__main__':

    show3dFig(Config.onlGroupProcess)