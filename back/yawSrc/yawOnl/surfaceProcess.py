import pandas as pd
from toolsSrc.decorator.toCsv import ToCsvDecorator
from yawSrc.tools.surfaceDecorator import SurfaceDecorator  # 不要删
from yawSrc.tools.surfaceLineDecorator import SurfaceLineDecorator  # 不要删
from common.config import yawConfig as Config

from yawSrc.tools.getPlotly import show3dFig
from yawSrc.tools.loadFile import toCsv, toAttrCsv
import os
from toolsSrc.status.status import Status
from src.main.sub_server import sub_app
from toolsSrc.util.plcweb import Plcweb
from datetime import datetime
###############  曲线拟合 ################
@SurfaceLineDecorator.pointsFit
@SurfaceLineDecorator.insert0
############### end 曲线拟合 #############
# ################  曲面拟合 ###############
# @SurfaceDecorator.insertGround
# @SurfaceDecorator.fit
# @SurfaceDecorator.clean
# ################  end 曲面拟合 ###########
@ToCsvDecorator(Config.surfacePool)
def onlSurfaceProcess(points):
    return points


@sub_app.get("/surfaceProcess")
@Status.updateBA(
    "UPDATE tracking SET status=:status, startTime=:startTime WHERE model=:model and func=:func",
    [{"status": 1, "startTime":datetime.now(), "model": "yawOnl", "func": "surfaceProcess"}],
    "UPDATE tracking SET status=:status, endTime=:endTime WHERE model=:model and func=:func",
    [{"status": 2, "endTime":datetime.now(), "model": "yawOnl", "func": "surfaceProcess"}])
@Plcweb.returnName
def surfaceProcess():
    print('yawonl surfaceProcess 开始................')
    # toCsv(Config.onlFusionProcess, Config.onlSurfaceProcess, onlSurfaceProcess)
    X = toCsv(Config.onlSmoothProcess, Config.onlSurfaceProcess, onlSurfaceProcess)
    toAttrCsv(Config.onlSurfaceProcess, Config.onlSurfaceAttr)
    print('yawonl surfaceProcess 完毕................')
    return X

if __name__ == '__main__':
    surfaceProcess()