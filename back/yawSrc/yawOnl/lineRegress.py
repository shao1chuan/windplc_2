from yawSrc.tools.getPlotly import get2dFig, get2dFig
from yawSrc.tools.loadFile import toLineRegressCsv, toAttrCsv, toLineRegressAttrCsv, fitLineRegressCsv, toCsv
from common.config import yawConfig as Config
from toolsSrc.status.status import Status
from yawSrc.tools.lineRegressDecorator import lineRegressDecorator
from datetime import datetime
from src.main.sub_server import sub_app
from toolsSrc.util.plcweb import Plcweb

@lineRegressDecorator.toLineRegressCsv # 找到山脊线
@lineRegressDecorator.sigma3filter # 过滤曲线离散点
@lineRegressDecorator.fitLineRegressCsv # 对山脊线点进行曲线拟合
def onlLineRegressProcess(X):
    return X

@sub_app.get("/lineRegress")
@Status.updateBA(
    "UPDATE tracking SET status=:status, startTime=:startTime WHERE model=:model and func=:func",
    [{"status": 1, "startTime":datetime.now(), "model": "yawOnl", "func": "lineRegress"}],
    "UPDATE tracking SET status=:status, endTime=:endTime WHERE model=:model and func=:func",
    [{"status": 2, "endTime":datetime.now(), "model": "yawOnl", "func": "lineRegress"}])
@Plcweb.returnName
def lineRegress():
    print('yawOnl lineRegress 开始.................')

    X = toCsv(Config.onlSurfaceProcess, Config.onlLineRegress, onlLineRegressProcess)
    # toLineRegressCsv(Config.onlSurfaceProcess, Config.onlLineRegress)
    # fitLineRegressCsv(Config.onlLineRegress, Config.onlLineRegress)  # 曲线拟合
    toLineRegressAttrCsv(Config.onlLineRegress, Config.onlLineRegressAttr)
    print('yawOnl lineRegress 结束.................')
    return X


if __name__ == '__main__':
    lineRegress()
