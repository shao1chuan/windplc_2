from yawSrc.tools.getPlotly import show3dFig
from common.config import yawConfig as Config


if __name__ == '__main__':

    show3dFig(Config.onlCleanProcess)