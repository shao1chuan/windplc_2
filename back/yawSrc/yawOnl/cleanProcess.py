from yawSrc.tools.cleanDecorator import CleanDecorator
from yawSrc.tools.getPlotly import show3dFig

from common.config import yawConfig as Config
from datetime import datetime
from yawSrc.tools.loadFile import toCsv, toAttrCsv
from toolsSrc.status.status import Status
from toolsSrc.util.plcweb import Plcweb
from src.main.sub_server import sub_app

@CleanDecorator.insert0   # 如果initData得到空表，插入0
@CleanDecorator.drop_dataEffectFlag_False # 每次赛选都有可能得到空表，因此，后面都应该insert0
@CleanDecorator.insert0  # 当为空表时，插入一行 0
@CleanDecorator.drop_bigorsmall
@CleanDecorator.insert0
@CleanDecorator.drop_false
@CleanDecorator.insert0
@CleanDecorator.drop_discretePoint
@CleanDecorator.insert0
def onlCleanProcess(X):
    return X

@sub_app.get("/cleanProcess")
@Status.updateBA(
    "UPDATE tracking SET status=:status, startTime=:startTime WHERE model=:model and func=:func",
    [{"status": 1, "startTime":datetime.now(), "model": "yawOnl", "func": "cleanProcess"}],
    "UPDATE tracking SET status=:status, endTime=:endTime WHERE model=:model and func=:func",
    [{"status": 2, "endTime":datetime.now(), "model": "yawOnl", "func": "cleanProcess"}])
@Plcweb.returnName
def cleanProcess():
    print('yawonl cleanProcess 开始.............')
    X = toCsv(Config.onlInitialData, Config.onlCleanProcess, onlCleanProcess)
    toAttrCsv(Config.onlCleanProcess, Config.onlCleanAttr)
    print('yawonl cleanProcess 结束................')
    # show3dFig(Config.onlCleanProcess)
    return X

if __name__ == '__main__':
    cleanProcess()