from yawSrc.tools.getPlotly import get2dFig_com

from common.config import yawConfig as Config



if __name__ == '__main__':
    fig = get2dFig_com(Config.lineCompareProcess)
    fig.show()